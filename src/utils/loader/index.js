import React from "react";
import "./loader.css";

export default function(props) {
  return (
    <section className="quiz">
      <div className="container">
        <div className="row">
          <div className="col-md-12 block">
            <div id="loader" />
          </div>
        </div>
      </div>
    </section>
  );
}
