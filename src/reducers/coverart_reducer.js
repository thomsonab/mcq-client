import {
  COVERART_MOMENT_PDF,
  COVERART_RESET_ERROR,
  COVERART_SIZE_ERROR,
  COVERART_UPLOAD_ERROR,
  COVERART_FILETYPE_ERROR,
  COVERART_SET_SUBMISSION,
  COVERART_UNSET_SUBMISSION
} from "../actions/types";

export default function(state = { error: "" }, action) {
  switch (action.type) {
    case COVERART_MOMENT_PDF:
      return { ...state, coverart_pdf: action.payload, error: "" };
    case COVERART_RESET_ERROR:
      return { ...state, error: "" };
    case COVERART_SIZE_ERROR:
      return { ...state, error: "Pdf size should be less than 5Mb " };
    case COVERART_UPLOAD_ERROR:
      return { ...state, error: action.payload };
    case COVERART_FILETYPE_ERROR:
      return { ...state, error: "File should be a pdf" };
    case COVERART_SET_SUBMISSION:
      return { ...state, spin: true };
    case COVERART_UNSET_SUBMISSION:
      return { ...state, spin: false };
    default:
      return state;
  }
}
