import {
  FETCH_QUESTIONS,
  SUBMIT_ANSWERS,
  UPDATE_SCORE
} from "../actions/types";

export default function(state = {}, action) {
  switch (action.type) {
    case FETCH_QUESTIONS:
      return { ...state, questionlist: action.payload };
    case SUBMIT_ANSWERS:
      return { ...state, answers: action.payload };
    case UPDATE_SCORE:
      return { ...state, score: action.payload };
    default:
      return state;
  }
}
