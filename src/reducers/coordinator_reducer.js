import {
  COORDINATOR_SUBMISSIONS,
  UPDATE_GOODCOPMOMENT_SUBMISSION,
  TAB_SELECTED,
  UPDATE_COVERART_SUBMISSION,
  UPDATE_FILM_SUBMISSION
} from "../actions/types";

export default function(state = { tab: 1 }, action) {
  switch (action.type) {
    case COORDINATOR_SUBMISSIONS:
      return { ...state, ...action.payload, error: "" };
    case UPDATE_GOODCOPMOMENT_SUBMISSION:
      const index = action.payload.index;
      const studentdata = action.payload.studentdata;
      let goodcopmomentsdata = [
        ...state.goodcopmoments.slice(0, index),
        studentdata,
        ...state.goodcopmoments.slice(index + 1)
      ];
      return { ...state, goodcopmoments: goodcopmomentsdata };

    case UPDATE_COVERART_SUBMISSION:
      const coverartindex = action.payload.index;
      const coverartstudentdata = action.payload.studentdata;
      let coverartdata = [
        ...state.coverart.slice(0, coverartindex),
        coverartstudentdata,
        ...state.coverart.slice(coverartindex + 1)
      ];
      return { ...state, coverart: coverartdata };

    case UPDATE_FILM_SUBMISSION:
      const filmindex = action.payload.index;
      const filmstudentdata = action.payload.studentdata;
      const filmdata = [
        ...state.film.slice(0, filmindex),
        filmstudentdata,
        ...state.film.slice(filmindex + 1)
      ];
      return { ...state, film: filmdata };

    case TAB_SELECTED:
      return { ...state, tab: action.payload };
    default:
      return state;
  }
}
