import {
  INSTITUTE_PROFILE,
  INSTITUTE_PROFILE_ERROR,
  REMOVE_INSTITUTE_PROFILE_ERROR,
  INSTITUTIONLIST,
  PUSH_INSTITUTE_LIST,
  REMOVE_INSTITUTE_PROFILE,
  SET_REGISTER_INSTITUTE_PROCESSING,
  UNSET_REGISTER_INSTITUTE_PROCESSING
} from "../actions/types";

export default function(state = { institutelist: [], error: "" }, action) {
  switch (action.type) {
    case INSTITUTE_PROFILE:
      return { ...state, institute: action.payload, saved: true, error: "" };
    case INSTITUTE_PROFILE_ERROR:
      return { ...state, saved: false, error: action.payload };
    case REMOVE_INSTITUTE_PROFILE_ERROR:
      return { ...state, saved: true, error: "" };
    case INSTITUTIONLIST:
      return { ...state, institutelist: action.payload };
    case PUSH_INSTITUTE_LIST:
      return {
        ...state,
        institutelist: [...state.institutelist, action.payload]
      };
    case REMOVE_INSTITUTE_PROFILE:
      return { ...state, institute: undefined, error: "" };
    default:
      return state;
    case SET_REGISTER_INSTITUTE_PROCESSING:
      return { ...state, processing: true };
    case UNSET_REGISTER_INSTITUTE_PROCESSING:
      return { ...state, processing: false };
  }
}
