import {
  RESET_PASSWORD_ERROR,
  RESET_PASSWORD_SUCCESS,
  RESET_PASSWORD_SHOW_SPINNER,
  RESET_PASSWORD_HIDE_SPINNER
} from "../actions/types";

export default function(state = { spinner: false }, action) {
  switch (action.type) {
    case RESET_PASSWORD_ERROR:
      return { ...state, errorMessage: action.payload.message, success: false };
    case RESET_PASSWORD_SUCCESS:
      return { ...state, errorMessage: "", success: true };
    case RESET_PASSWORD_SHOW_SPINNER:
      return { ...state, spinner: true };
    case RESET_PASSWORD_HIDE_SPINNER:
      return { ...state, spinner: false };
    default:
      return state;
  }
}
