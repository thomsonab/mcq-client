import {
  USER,
  UNSET_USER_PROCESSING,
  SET_USER_PROCESSING,
  USER_UPDATE_ERROR,
  UNSET_USER_UPDATE_ERROR
} from "../actions/types";

export default function(state = {}, action) {
  switch (action.type) {
    case USER:
      return { ...state, user: action.payload };

    case SET_USER_PROCESSING:
      return { ...state, processing: true };

    case UNSET_USER_PROCESSING:
      return { ...state, processing: false };

    case USER_UPDATE_ERROR:
      return { ...state, error: action.payload };

    case UNSET_USER_UPDATE_ERROR:
      return { ...state, error: "" };
    default:
      return state;
  }
}
