import {
  AUTH_USER,
  UNAUTH_USER,
  AUTH_ERROR,
  REMOVE_AUTH_ERROR
} from "../actions/types";
export default function(state = {}, action) {
  switch (action.type) {
    case AUTH_USER:
      return { ...state, error: "", authenticated: true, ...action.payload };
    case UNAUTH_USER:
      // Ideally we are supposed to have separate authentication state details for,
      // student and coordinator. School === Institute, we have to come up with a common
      // ground. Until then :/
      return {
        ...state,
        authenticated: false,
        name: "",
        school: "",
        institute: "",
        type: ""
      };
    case AUTH_ERROR:
      return { ...state, error: action.payload, authenticated: false };
    case REMOVE_AUTH_ERROR:
      return { ...state, error: "" };

    default:
      return state;
  }
  return state;
}
