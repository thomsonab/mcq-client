import {
  FILM_SET_SUBMISSION,
  FILM_UNSET_SUBMISSION,
  FILM_RESET_ERROR,
  FILM_SUBMIT_ERROR
} from "../actions/types";

export default function(state = { error: "" }, action) {
  switch (action.type) {
    case FILM_SET_SUBMISSION:
      return { ...state, spin: true };
    case FILM_UNSET_SUBMISSION:
      return { ...state, spin: false };
    case FILM_RESET_ERROR:
      return { ...state, error: "" };
    case FILM_SUBMIT_ERROR:
      return { ...state, error: action.payload };
    default:
      return state;
  }
}
