import {
  REPORT_RESET_ERROR,
  REPORT_SIZE_ERROR,
  REPORT_UPLOAD_ERROR,
  REPORT_FILETYPE_ERROR,
  REPORT_SET_SUBMISSION,
  REPORT_UNSET_SUBMISSION,
  COORDINATOR_SUBMISSIONS
} from "../actions/types";

export default function(state = { error: "" }, action) {
  switch (action.type) {
    case REPORT_RESET_ERROR:
      return { ...state, error: "" };
    case REPORT_SIZE_ERROR:
      return { ...state, error: "Pdf size should be less than 10Mb" };
    case REPORT_UPLOAD_ERROR:
      return { ...state, error: action.payload };

    case REPORT_FILETYPE_ERROR:
      return { ...state, error: "File should be a pdf" };
    case REPORT_SET_SUBMISSION:
      return { ...state, spin: true };
    case REPORT_UNSET_SUBMISSION:
      return { ...state, spin: false };

    case COORDINATOR_SUBMISSIONS:
      return {
        ...state,
        coordinatorsubmissions: {
          ...action.payload
        }
      };
    default:
      return state;
  }
}
