import {
  ADMIN_INSTITUTIONS,
  UPDATE_ADMIN_INSTITUTIONS,
  SELECTED_GOODCOPMOMENTS,
  SELECTED_COVERARTS,
  SELECTED_FILMS,
  STUDENTLIST,
  SELECTED_REPORTS,
  UPDATE_SELECTED_GOODCOPMOMENT_SUBMISSION,
  UPDATE_SELECTED_COVERART_SUBMISSION,
  UPDATE_SELECTED_FILM_SUBMISSION,
  COMMITTEE_SELECTED_GOODCOPMOMENTS,
  COMMITTEE_SELECTED_COVERARTS,
  COMMITTEE_SELECTED_FILMS,
  UPDATE_COMMITTEE_SELECTED_GOODCOPMOMENT_SUBMISSION,
  UPDATE_COMMITTEE_SELECTED_COVERART_SUBMISSION,
  UPDATE_COMMITTEE_SELECTED_FILM_SUBMISSION
} from "../actions/types";

export default function(state = {}, action) {
  switch (action.type) {
    case ADMIN_INSTITUTIONS:
      return { ...state, institutions: action.payload };
    case UPDATE_ADMIN_INSTITUTIONS:
      const index = action.payload.index;
      const institutiondata = action.payload.institiondata;

      let institutionslice = [
        ...state.institutions.slice(0, index),
        institutiondata,
        ...state.institutions.slice(index + 1)
      ];
      return { ...state, institutions: institutionslice };

    case COMMITTEE_SELECTED_GOODCOPMOMENTS:
      return { ...state, ...action.payload, error: "" };

    case UPDATE_COMMITTEE_SELECTED_GOODCOPMOMENT_SUBMISSION:
      const committeeselectedgoodcopmomentindex = action.payload.index;
      const studentdata = action.payload.studentdata;
      let committeeselectedgoodcopmomentsdata = [
        ...state.committeeselectedgoodcopmoments.slice(
          0,
          committeeselectedgoodcopmomentindex
        ),
        studentdata,
        ...state.committeeselectedgoodcopmoments.slice(
          committeeselectedgoodcopmomentindex + 1
        )
      ];
      return {
        ...state,
        committeeselectedgoodcopmoments: committeeselectedgoodcopmomentsdata
      };

    case COMMITTEE_SELECTED_COVERARTS:
      return { ...state, ...action.payload, error: "" };

    case UPDATE_COMMITTEE_SELECTED_COVERART_SUBMISSION:
      const committeeselectedcoverartindex = action.payload.index;

      const committeeselectedcoverartstudentdata = action.payload.studentdata;

      let committeeselectedcoverartdata = [
        ...state.committeeselectedcoverarts.slice(
          0,
          committeeselectedcoverartindex
        ),
        committeeselectedcoverartstudentdata,
        ...state.committeeselectedcoverarts.slice(
          committeeselectedcoverartindex + 1
        )
      ];

      return {
        ...state,
        committeeselectedcoverarts: committeeselectedcoverartdata
      };

    case COMMITTEE_SELECTED_FILMS:
      return { ...state, ...action.payload, error: "" };

    case UPDATE_COMMITTEE_SELECTED_FILM_SUBMISSION:
      const committeeselectedfilmindex = action.payload.index;
      const committeeselectedfilmstudentdata = action.payload.studentdata;
      const committeeselectedfilmdata = [
        ...state.committeeselectedfilms.slice(0, committeeselectedfilmindex),
        committeeselectedfilmstudentdata,
        ...state.committeeselectedfilms.slice(committeeselectedfilmindex + 1)
      ];
      return { ...state, committeeselectedfilms: committeeselectedfilmdata };

    case SELECTED_GOODCOPMOMENTS:
      return { ...state, ...action.payload, error: "" };

    case UPDATE_SELECTED_GOODCOPMOMENT_SUBMISSION:
      const goodcopmomentindex = action.payload.index;
      const goodcopmomentstudentdata = action.payload.studentdata;
      let goodcopmomentsdata = [
        ...state.goodcopmoments.slice(0, goodcopmomentindex),
        goodcopmomentstudentdata,
        ...state.goodcopmoments.slice(goodcopmomentindex + 1)
      ];

      return { ...state, goodcopmoments: goodcopmomentsdata };

    case SELECTED_COVERARTS:
      return { ...state, ...action.payload, error: "" };

    case UPDATE_SELECTED_COVERART_SUBMISSION:
      const coverartindex = action.payload.index;

      const coverartstudentdata = action.payload.studentdata;

      let coverartdata = [
        ...state.coverarts.slice(0, coverartindex),
        coverartstudentdata,
        ...state.coverarts.slice(coverartindex + 1)
      ];
      return { ...state, coverarts: coverartdata };

    case SELECTED_FILMS:
      return { ...state, ...action.payload, error: "" };

    case UPDATE_SELECTED_FILM_SUBMISSION:
      const filmindex = action.payload.index;
      const filmstudentdata = action.payload.studentdata;
      const filmdata = [
        ...state.films.slice(0, filmindex),
        filmstudentdata,
        ...state.films.slice(filmindex + 1)
      ];
      return { ...state, films: filmdata };

    case STUDENTLIST:
      return { ...state, studentlist: action.payload, error: "" };

    case SELECTED_REPORTS:
      return { ...state, ...action.payload, error: "" };

    default:
      return state;
  }
}
