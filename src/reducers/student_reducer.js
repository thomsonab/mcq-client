import {
  GOOD_COP_MOMENT_PDF,
  SIZE_ERROR,
  UPLOAD_ERROR,
  RESET_ERROR,
  FILETYPE_ERROR,
  STUDENT_SUBMISSIONS,
  SET_STUDENT_SUBMISSION,
  UNSET_STUDENT_SUBMISSION
} from "../actions/types";

export default function(state = { error: "" }, action) {
  switch (action.type) {
    case GOOD_COP_MOMENT_PDF:
      return { ...state, goodcopmoment_pdf: action.payload, error: "" };
    case RESET_ERROR:
      return { ...state, error: "" };
    case SIZE_ERROR:
      return { ...state, error: "Pdf size should be less than 5Mb" };
    case UPLOAD_ERROR:
      return { ...state, error: action.payload };
    case FILETYPE_ERROR:
      return { ...state, error: "File should be a pdf" };
    case STUDENT_SUBMISSIONS:
      return { ...state, studentsubmissions: { ...action.payload } };
    case SET_STUDENT_SUBMISSION:
      return { ...state, spin: true };
    case UNSET_STUDENT_SUBMISSION:
      return { ...state, spin: false };
    default:
      return state;
  }
}
