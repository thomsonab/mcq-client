import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";
import authReducer from "./auth_reducer";
import questionsReducer from "./questions_reducer";
import instituteReducer from "./institute_reducer";
import studentReducer from "./student_reducer";
import coordinatorReducer from "./coordinator_reducer";
import resetpasswordReducer from "./reset_password";
import adminReducer from "./admin_reducer";
import userReducer from "./user_reducer";
import coverartReducer from "./coverart_reducer";
import filmReducer from "./film_reducer";
import reportReducer from "./report_reducer";
// router reducer is to make it available in the store, so that we can
// navigate as we want.
import { routerReducer } from "react-router-redux";

const rootReducer = combineReducers({
  form: formReducer,
  auth: authReducer,
  router: routerReducer,
  questions: questionsReducer,
  institute: instituteReducer,
  student: studentReducer,
  coverart: coverartReducer,
  coordinator: coordinatorReducer,
  resetpassword: resetpasswordReducer,
  admin: adminReducer,
  user: userReducer,
  film: filmReducer,
  report: reportReducer
});

export default rootReducer;
