import React from "react";
import ReactDOM from "react-dom";

import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import { Route, Switch } from "react-router-dom";
import ReduxThunk from "redux-thunk";

import createHistory from "history/createBrowserHistory";
import { ConnectedRouter, routerMiddleware, push } from "react-router-redux";

import "bootstrap/dist/css/bootstrap.css";
import "./index.css";

import App from "./App";
import Header from "./components/header";
import Footer from "./components/footer";
import AsyncComponent from "./utils/async-component";

//import Signin from "./components/signin";
// import Signout from "./components/signout";
// import Signup from "./components/signup";
// import Forgot from "./components/forgot";
// import ResetPassword from "./components/reset_password";
// import Questions from "./components/questions";
// import Scores from "./components/scores";
// import SignupWrapper from "./components/signupwrapper";
//import Dashboard from "./components/coordinator/dashboard";
//import StudentDashboard from "./components/student/student_dashboard";
//import AdminDashboard from "./components/admin/dashboard";

// import ActivitiesWrapper from "./components/activities/activitieswrapper";

//import AboutUs from "./components/aboutus";

import { AUTH_USER } from "./actions/types";

// Higher order authentication component
import RequireAuth from "./components/auth/require_auth";
import RequireStudentAuth from "./components/auth/require_student_auth";
import RequireCoordinatorAuth from "./components/auth/require_coordinator_auth";
import RequireAdminAuth from "./components/auth/require_admin_auth";
import RequireCommitteeAdminAuth from "./components/auth/require_committee_admin_auth";
import RequirePoliceAdminAuth from "./components/auth/require_police_admin_auth";

import reducers from "./reducers";
import registerServiceWorker from "./registerServiceWorker";

const AsyncForgot = AsyncComponent(() => import("./components/forgot"));
const AsyncResetPassword = AsyncComponent(() =>
  import("./components/reset_password")
);
const AsyncQuestions = AsyncComponent(() => import("./components/questions"));
const AsyncScores = AsyncComponent(() => import("./components/scores"));

const AsyncSignin = AsyncComponent(() => import("./components/signin"));
const AsyncSignup = AsyncComponent(() => import("./components/signupwrapper"));
const AsyncSignout = AsyncComponent(() => import("./components/signout"));

const AsyncDocs = AsyncComponent(() => import("./components/docs"));
const AsyncHelp = AsyncComponent(() => import("./components/help"));

const AsyncDashboard = AsyncComponent(() =>
  import("./components/coordinator/dashboard")
);
const AsyncStudentDashboard = AsyncComponent(() =>
  import("./components/student/student_dashboard")
);
const AsyncAdminDashboard = AsyncComponent(() =>
  import("./components/admin/dashboard")
);

const AsyncCommitteeAdminDashboard = AsyncComponent(() =>
  import("./components/admin/committeeadmindashboard")
);

const AsyncPoliceAdminDashboard = AsyncComponent(() =>
  import("./components/admin/policeadmindashboard")
);

const AsyncAboutUs = AsyncComponent(() => import("./components/aboutus/index"));
const AsyncActivitiesWrapper = AsyncComponent(() =>
  import("./components/activities/activitieswrapper")
);

const AsyncActivitiesNew = AsyncComponent(() =>
  import("./components/activities/activitiesnew")
);

// create history; we are choosing browser history
const history = createHistory();

// Build middleware for intercepting and dispatching navigation actions.
const middleware = routerMiddleware(history);

const createStoreWithMiddleware = applyMiddleware(middleware, ReduxThunk)(
  createStore
);

const store = createStoreWithMiddleware(reducers);

// If a user is already logged in let not
// ask him to login again
const token = localStorage.getItem("MCQ_token");

if (token) {
  // store has access to dispatch method.
  store.dispatch({ type: AUTH_USER });
}

// Redirect to appropriate page.
const usertype = localStorage.getItem("type");
const username = localStorage.getItem("Name");
if (usertype || username) {
  store.dispatch({
    type: AUTH_USER,
    payload: { type: usertype, name: username }
  });
}

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <div>
        <Header />
        <Switch>
          <Route path="/signin" component={AsyncSignin} />
          <Route path="/signout" component={AsyncSignout} />
          <Route path="/signup" component={AsyncSignup} />
          <Route path="/docs" component={AsyncDocs} />
          <Route path="/help" component={AsyncHelp} />
          <Route
            path="/dashboard"
            component={RequireCoordinatorAuth(AsyncDashboard)}
          />
          <Route
            path="/admindashboard"
            component={RequireAdminAuth(AsyncAdminDashboard)}
          />

          <Route
            path="/committeeadmindashboard"
            component={RequireCommitteeAdminAuth(AsyncCommitteeAdminDashboard)}
          />

          <Route
            path="/policeadmindashboard"
            component={RequirePoliceAdminAuth(AsyncPoliceAdminDashboard)}
          />

          <Route
            path="/studentdashboard/:tabselected"
            component={RequireStudentAuth(AsyncStudentDashboard)}
          />
          <Route
            path="/studentdashboard"
            component={RequireStudentAuth(AsyncStudentDashboard)}
          />
          <Route path="/aboutus" component={AsyncAboutUs} />
          <Route path="/activities/:id" component={AsyncActivitiesWrapper} />
          <Route path="/activities" component={AsyncActivitiesNew} />
          <Route path="/reset/:token" component={AsyncResetPassword} />
          <Route path="/forgotpassword" component={AsyncForgot} />
          <Route path="/questions" component={RequireAuth(AsyncQuestions)} />
          <Route path="/scores" component={RequireAuth(AsyncScores)} />
          <Route path="/" component={App} />
        </Switch>
        <Footer />
      </div>
    </ConnectedRouter>
  </Provider>,
  document.getElementById("root")
);
registerServiceWorker();
