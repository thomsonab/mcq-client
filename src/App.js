import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import logo from "./logo.svg";

import Body from "./components/body";

import StudentDashboard from "./components/student/student_dashboard";
import LeftPanel from "./components/common/leftpanel";
import RightPanel from "./components/common/rightpanel";

import "bootstrap/dist/css/bootstrap.min.css";
class App extends Component {
  render() {
    const info = "Please register to take the quiz";
    const link = {
      anchor: "/signup",
      label: "Register"
    };
    return (
      <div>
        <Body info={info} link={link} />
        {/* {this.props.authenticated && this.props.type === "student" ? (
          <StudentDashboard />
        ) : (
          <Body info={info} link={link} />
        )} */}
      </div>
    );
  }
}

function mapStatetoProps(state) {
  return {
    authenticated: state.auth.authenticated,
    type: state.auth.type
  };
}
export default connect(mapStatetoProps)(App);
