export const AUTH_USER = "AUTH_USER";
export const UNAUTH_USER = "UNAUTH_USER";
export const AUTH_ERROR = "AUTH_ERROR";
export const FETCH_QUESTIONS = "FETCH_QUESTIONS";
export const SUBMIT_ANSWERS = "SUBMIT_ANSWERS";
export const UPDATE_SCORE = "UPDATE_SCORE";
export const REMOVE_AUTH_ERROR = "REMOVE_AUTH_ERROR";
export const INSTITUTE_PROFILE = "INSTITUTE_PROFILE";
export const REMOVE_INSTITUTE_PROFILE = "REMOVE_INSTITUTE_PROFILE";
export const INSTITUTE_PROFILE_ERROR = "INSTITUTE_PROFILE_ERROR";
export const REMOVE_INSTITUTE_PROFILE_ERROR = "INSTITUTE_PROFILE_ERROR";

// This set the state which will be used by the spinners of the register
// institute page.
export const SET_REGISTER_INSTITUTE_PROCESSING =
  "SET_REGISTER_INSTITUTE_PROCESSING";
export const UNSET_REGISTER_INSTITUTE_PROCESSING =
  "UNSET_REGISTER_INSTITUTE_PROCESSING";

export const SIZE_ERROR = "SIZE_ERROR";
export const UPLOAD_ERROR = "UPLOAD_ERROR";
export const RESET_ERROR = "RESET_ERROR";
export const FILETYPE_ERROR = "FILETYPE_ERROR";
export const INSTITUTIONLIST = "INSTITUTIONLIST";
export const PUSH_INSTITUTE_LIST = "PUSH_INSTITUTE_LIST";

// Reset password
export const RESET_PASSWORD_ERROR = "RESET_PASSWORD_ERROR";
export const RESET_PASSWORD_SUCCESS = "RESET_PASSWORD_SUCCESS";
export const RESET_PASSWORD_SHOW_SPINNER = "RESET_PASSWORD_SHOW_SPINNER";
export const RESET_PASSWORD_HIDE_SPINNER = "RESET_PASSWORD_HIDE_SPINNER";

//Admin
export const ADMIN_INSTITUTIONS = "ADMIN_INSTITUTIONS";
export const UPDATE_ADMIN_INSTITUTIONS = "UPDATE_ADMIN_INSTITUTIONS";
export const SELECTED_GOODCOPMOMENTS = "SELECTED_GOODCOPMOMENTS";
export const SELECTED_COVERARTS = "SELECTED_COVERARTS";
export const SELECTED_FILMS = "SELECTED_FILMS";
export const STUDENTLIST = "STUDENTLIST";

// Committee Admin
export const UPDATE_SELECTED_GOODCOPMOMENT_SUBMISSION =
  "UPDATE_SELECTED_GOODCOPMOMENT_SUBMISSION";
export const UPDATE_SELECTED_COVERART_SUBMISSION =
  "UPDATE_SELECTED_COVERART_SUBMISSION";
export const UPDATE_SELECTED_FILM_SUBMISSION =
  "UPDATE_SELECTED_FILM_SUBMISSION";

// Police Admin
export const COMMITTEE_SELECTED_GOODCOPMOMENTS =
  "COMMITTEE_SELECTED_GOODCOPMOMENTS";
export const COMMITTEE_SELECTED_COVERARTS = "COMMITTEE_SELECTED_COVERARTS";
export const COMMITTEE_SELECTED_FILMS = "COMMITTEE_SELECTED_FILMS";
export const UPDATE_COMMITTEE_SELECTED_GOODCOPMOMENT_SUBMISSION =
  "UPDATE_COMMITTEE_SELECTED_GOODCOPMOMENT_SUBMISSION";
export const UPDATE_COMMITTEE_SELECTED_COVERART_SUBMISSION =
  "UPDATE_COMMITTEE_SELECTED_COVERART_SUBMISSION";
export const UPDATE_COMMITTEE_SELECTED_FILM_SUBMISSION =
  "UPDATE_COMMITTEE_SELECTED_FILM_SUBMISSION";

//UpdateUser
export const USER = "USER";
export const SET_USER_PROCESSING = "SET_USER_PROCESSING";
export const UNSET_USER_PROCESSING = "UNSET_USER_PROCESSING";
export const USER_UPDATE_ERROR = "USER_UPDATE_ERROR";
export const UNSET_USER_UPDATE_ERROR = "UNSET_USER_UPDATE_ERROR";

//Student
export const COVERART_MOMENT_PDF = "COVER_ART_MOMENT_PDF";

export const COVERART_RESET_ERROR = "COVERART_RESET_ERROR";
export const COVERART_SIZE_ERROR = "COVERART_SIZE_ERROR";
export const COVERART_UPLOAD_ERROR = "COVERART_UPLOAD_ERROR";
export const COVERART_FILETYPE_ERROR = "COVERART_FILETYPE_ERROR";
export const COVERART_SET_SUBMISSION = "COVERART_SET_SUBMISSION";
export const COVERART_UNSET_SUBMISSION = "COVERART_UNSET_SUBMISSION";

export const REPORT_SIZE_ERROR = "REPORT_SIZE_ERROR";
export const REPORT_UPLOAD_ERROR = "REPORT_UPLOAD_ERROR";
export const REPORT_FILETYPE_ERROR = "REPORT_FILETYPE_ERROR";
export const REPORT_SET_SUBMISSION = "REPORT_SET_SUBMISSION";
export const REPORT_UNSET_SUBMISSION = "REPORT_UNSET_SUBMISSION";
export const REPORT_RESET_ERROR = "REPORT_RESET_ERROR";
export const SELECTED_REPORTS = "SELECTED_REPORTS";
export const COORDINATOR_REPORTS = "COORDINATOR_REPORTS";

export const FILM_SET_SUBMISSION = "FILM_SET_SUBMISSION";
export const FILM_UNSET_SUBMISSION = "FILM_UNSET_SUBMISSION";
export const FILM_SUBMIT_ERROR = "FILM_SUBMIT_ERROR";
export const FILM_RESET_ERROR = "FILM_RESET_ERROR";

export const GOOD_COP_MOMENT_PDF = "GOOD_COP_MOMENT_PDF";
export const STUDENT_SUBMISSIONS = "STUDENT_SUBMISSIONS";
export const SET_STUDENT_SUBMISSION = "SET_STUDENT_SUBMISSON";
export const UNSET_STUDENT_SUBMISSION = "UNSET_STUDENT_SUBMISSION";

//Coordinator
export const COORDINATOR_SUBMISSIONS = "COORDINATOR_SUBMISSIONS";
export const UPDATE_GOODCOPMOMENT_SUBMISSION =
  "UPDATE_GOODCOPMOMENT_SUBMISSION";
export const UPDATE_COVERART_SUBMISSION = "UPDATE_COVERART_SUBMISSION";
export const UPDATE_FILM_SUBMISSION = "UPDATE_FILM_SUBMISSION";
export const TAB_SELECTED = "TAB_SELECTED";
