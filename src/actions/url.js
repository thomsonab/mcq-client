// Heroku dev
//export const ROOT_URL = "https://fast-scrubland-78705.herokuapp.com";

// Dev
//export const ROOT_URL = "http://localhost:3000";

//Staging
//export const ROOT_URL = "http://ocoh.aggu.in";
//export const ROOT_URL = "https://ocoh.azurewebsites.net";

//Production
//export const ROOT_URL = "http://ocoh.edmat.org";
//export const ROOT_URL = "https://edmat-ocoh.azurewebsites.net";

//Using Environment Variable
export const ROOT_URL = process.env.REACT_APP_ROOT_URL;
