import axios from "axios";

import {
  USER,
  SET_USER_PROCESSING,
  UNSET_USER_PROCESSING,
  USER_UPDATE_ERROR,
  UNSET_USER_UPDATE_ERROR
} from "../types";

import { ROOT_URL } from "../url";

export function fetchUserData() {
  return function(dispatch) {
    axios
      .get(`${ROOT_URL}/user`, {
        headers: {
          authorization: localStorage.getItem("MCQ_token")
        }
      })
      .then(response => {
        dispatch({
          type: USER,
          payload: response.data
        });
      })
      .catch(err => {
        console.log(err.response);
      });
  };
}

export function updateUser({
  name,
  institute,
  designation,
  facebookusername,
  email,
  phone
}) {
  return function(dispatch) {
    dispatch({ type: SET_USER_PROCESSING });
    dispatch({ type: UNSET_USER_UPDATE_ERROR });
    axios
      .put(
        `${ROOT_URL}/user`,
        {
          name: name,
          institute: institute,
          designation: designation,
          facebookusername: facebookusername,
          email: email,
          phone: phone
        },
        {
          headers: {
            authorization: localStorage.getItem("MCQ_token")
          }
        }
      )
      .then(response => {
        dispatch({ type: USER, payload: response.data.obj });
        dispatch({ type: UNSET_USER_PROCESSING });
      })
      .catch(err => {
        dispatch({
          type: USER_UPDATE_ERROR,
          payload: err.response.data.message
        });
        dispatch({ type: UNSET_USER_PROCESSING });
      });
  };
}
