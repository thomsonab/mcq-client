import axios from "axios";
import {
  AUTH_USER,
  AUTH_ERROR,
  UNAUTH_USER,
  FETCH_QUESTIONS,
  SUBMIT_ANSWERS,
  UPDATE_SCORE,
  REMOVE_AUTH_ERROR,
  INSTITUTE_PROFILE,
  REMOVE_INSTITUTE_PROFILE_ERROR,
  REMOVE_INSTITUTE_PROFILE,
  INSTITUTE_PROFILE_ERROR,
  INSTITUTIONLIST,
  PUSH_INSTITUTE_LIST,
  RESET_PASSWORD_ERROR,
  RESET_PASSWORD_SUCCESS,
  RESET_PASSWORD_SHOW_SPINNER,
  RESET_PASSWORD_HIDE_SPINNER,
  SET_REGISTER_INSTITUTE_PROCESSING,
  UNSET_REGISTER_INSTITUTE_PROCESSING,
  STUDENT_SUBMISSIONS
} from "./types";
// Assumption is that since store has access to history.
import { push } from "react-router-redux";
import { ROOT_URL } from "./url";

export function signoutUser() {
  localStorage.removeItem("MCQ_token");
  localStorage.removeItem("Name");
  localStorage.removeItem("School");
  localStorage.removeItem("Institute");
  localStorage.removeItem("type");
  return function(dispatch) {
    dispatch({
      type: UNAUTH_USER
    });
    dispatch({
      type: STUDENT_SUBMISSIONS,
      payload: {
        goodcopmoment: [],
        coverartdata: [],
        filmdata: []
      }
    });
  };
}

export function getInstituteList() {
  return function(dispatch) {
    axios
      .get(`${ROOT_URL}/institution/institutelist`, {
        headers: {
          authorization: localStorage.getItem("MCQ_token")
        }
      })
      .then(response => {
        let dispatchdata = response.data.map(institute => {
          return {
            name: institute
          };
        });
        dispatch({ type: INSTITUTIONLIST, payload: dispatchdata });
      })
      .catch(error => {
        console.log("Not available");
      });
  };
}

export function fetchScore() {
  return function(dispatch) {
    axios
      .get(`${ROOT_URL}/scores`, {
        headers: {
          authorization: localStorage.getItem("MCQ_token")
        }
      })
      .then(response => {
        dispatch({ type: UPDATE_SCORE, payload: response.data.score });
      })
      .catch(error => {
        dispatch(push("/questions"));
      });
  };
}

/* signupUser
 *  - This is for students. Added type as student here.
 * 
 */
export function signupUser({
  name,
  email,
  password,
  school,
  level,
  facebookusername,
  phone,
  registeredevents
}) {
  // Type info is not a part of the form reducer/action.
  const type = "student";
  let schoolvalue = school.name;
  return function(dispatch) {
    dispatch({ type: REMOVE_AUTH_ERROR });
    axios
      .post(`${ROOT_URL}/signup`, {
        name: name,
        email: email,
        password: password,
        level: level,
        facebookusername: facebookusername,
        school: schoolvalue,
        type: type,
        phone: phone,
        registeredevents: registeredevents
      })
      .then(response => {
        localStorage.setItem("MCQ_token", response.data.token);
        localStorage.setItem("Name", name);
        localStorage.setItem("School", schoolvalue);
        localStorage.setItem("type", type);
        dispatch({
          type: AUTH_USER,
          payload: { name: name, school: schoolvalue, type: type }
        });
        dispatch(push("/studentdashboard"));
      })
      .catch(err => {
        dispatch(authError(err.response.data.message));
      });
  };
}

/* signupCoordinator
 *  - This is for coordinators. Added type as coordinator here.
 * 
 */
export function signupCoordinator({
  name,
  email,
  password,
  institute,
  institute_custom,
  designation,
  facebookusername,
  phone
}) {
  // Type info is not a part of the form reducer/action.
  const type = "coordinator";
  let institutevalue = institute.name;
  return function(dispatch) {
    dispatch({ type: REMOVE_AUTH_ERROR });
    if (institutevalue === "Others") {
      institutevalue = institute_custom;
      dispatch({
        type: PUSH_INSTITUTE_LIST,
        payload: { name: institute_custom }
      });
    }
    axios
      .post(`${ROOT_URL}/signup`, {
        name: name,
        email: email,
        password: password,
        designation: designation,
        facebookusername: facebookusername,
        institute: institutevalue,
        institute_custom: institute_custom,
        type: type,
        phone: phone
      })
      .then(response => {
        localStorage.setItem("MCQ_token", response.data.token);
        localStorage.setItem("Name", name);
        localStorage.setItem("Institute", institutevalue);
        localStorage.setItem("type", type);
        dispatch({
          type: AUTH_USER,
          payload: { name: name, institute: institutevalue, type: type }
        });
        dispatch(push("/dashboard"));
      })
      .catch(err => {
        dispatch(authError(err.response.data.message));
      });
  };
}

export function resetPassword({ token, password, confirmpassword }) {
  return function(dispatch) {
    dispatch({
      type: RESET_PASSWORD_SHOW_SPINNER
    });
    axios
      .post(`${ROOT_URL}/resetpassword/${token}`, {
        password: password
      })
      .then(response => {
        dispatch({ type: RESET_PASSWORD_HIDE_SPINNER });
        dispatch({
          type: RESET_PASSWORD_SUCCESS
        });
      })
      .catch(err => {
        dispatch({ type: RESET_PASSWORD_HIDE_SPINNER });
        dispatch({
          type: RESET_PASSWORD_ERROR,
          payload: err.response.data
        });
      });
  };
}

export function signinUser({ email, password }) {
  //Submit email/password to server
  return function(dispatch) {
    dispatch({ type: REMOVE_AUTH_ERROR });
    axios
      .post(`${ROOT_URL}/signin`, {
        email: email,
        password: password
      })
      .then(response => {
        localStorage.setItem("MCQ_token", response.data.token);
        localStorage.setItem("Name", response.data.user.name);
        localStorage.setItem("School", response.data.user.school);
        localStorage.setItem("Institute", response.data.user.institute);
        localStorage.setItem("type", response.data.user.type);
        dispatch({
          type: AUTH_USER,
          payload: {
            name: response.data.user.name,
            school: response.data.user.school,
            institute: response.data.user.institute,
            type: response.data.user.type
          }
        });
        if (response.data.user.type === "student") {
          dispatch(push("/studentdashboard"));
        } else if (response.data.user.type === "coordinator") {
          dispatch(push("/dashboard"));
        } else if (response.data.user.type === "admin") {
          dispatch(push("/admindashboard"));
        } else if (response.data.user.type === "committeeadmin") {
          dispatch(push("/committeeadmindashboard"));
        } else if (response.data.user.type === "policeadmin") {
          dispatch(push("/policeadmindashboard"));
        }
      })
      .catch(() => {
        dispatch(authError("Bad Login Info"));
      });
  };
  // If true
  // Update state to indicate user is authenticated
  // - Save JWT
  // redirect to skeleton

  // If request is bad
  // - Show error to user.
}

export function fetchQuestions() {
  return function(dispatch) {
    axios
      .get(`${ROOT_URL}/questions`, {
        headers: {
          authorization: localStorage.getItem("MCQ_token")
        }
      })
      .then(response => {
        dispatch({
          type: FETCH_QUESTIONS,
          payload: response.data.questionlist
        });
      })
      .catch(err => {
        console.log(err.response);
      });
  };
}

export function submitAnswers(object) {
  const answerarray = [];

  for (const prop in object) {
    let answerobj = {
      question: {
        _id: prop
      },
      answer: object[prop]
    };
    answerarray.push(answerobj);
  }

  return function(dispatch) {
    axios
      .post(
        `${ROOT_URL}/answers`,
        { responses: answerarray },
        {
          headers: {
            authorization: localStorage.getItem("MCQ_token")
          }
        }
      )
      .then(response => {
        dispatch({
          type: SUBMIT_ANSWERS,
          payload: response.data.obj.responses
        });
        dispatch(push("/scores"));
      })
      .catch(error => {
        alert("You have already taken the questions");
        dispatch(push("/scores"));
      });
  };
}

export function updateInstitute({
  address,
  district,
  email,
  facebookusername,
  name,
  phone,
  pincode,
  type,
  hoddesignation,
  hodemail,
  hodname,
  hodfacebookusername,
  hodphone,
  registeredevents,
  _id
}) {
  return function(dispatch) {
    let preregisteredevents = {
      gcm: true,
      appreciation: true,
      quiz: true,
      pledge: true,
      promote: true
    };
    const postdata = {
      collegeOrInstitute: name.name,
      district: district,
      institutionemail: email,
      facebookusername: facebookusername,
      phone: phone,
      pincode: pincode,
      boardOrUniversity: type,
      address: address,
      hod: {
        name: hodname,
        email: hodemail,
        facebookusername: hodfacebookusername,
        phone: hodphone,
        designation: hoddesignation
      },
      registeredevents: { ...preregisteredevents, ...registeredevents }
    };

    dispatch({ type: REMOVE_INSTITUTE_PROFILE_ERROR });
    dispatch({ type: SET_REGISTER_INSTITUTE_PROCESSING });
    axios
      .put(`${ROOT_URL}/institution/${_id}`, postdata, {
        headers: {
          authorization: localStorage.getItem("MCQ_token")
        }
      })
      .then(response => {
        dispatch({ type: INSTITUTE_PROFILE, payload: response.data.obj });
        dispatch({ type: UNSET_REGISTER_INSTITUTE_PROCESSING });
      })
      .catch(err => {
        dispatch({
          type: INSTITUTE_PROFILE_ERROR,
          payload: err.response.data.message
        });
        dispatch({ type: UNSET_REGISTER_INSTITUTE_PROCESSING });
      });
  };
}

export function registerInstitute({
  address,
  district,
  email,
  facebookusername,
  name,
  phone,
  pincode,
  type,
  hoddesignation,
  hodemail,
  hodname,
  hodfacebookusername,
  hodphone,
  registeredevents
}) {
  return function(dispatch) {
    // Redux was not able to handle this scenario.
    let preregisteredevents = {
      gcm: true,
      appreciation: true,
      quiz: true,
      pledge: true,
      promote: true
    };

    // name field which contains the autocomplete data
    // has value of the form { name: 'Velammal college of engg'}

    const postdata = {
      collegeOrInstitute: name.name,
      district: district,
      institutionemail: email,
      facebookusername: facebookusername,
      phone: phone,
      pincode: pincode,
      boardOrUniversity: type,
      address: address,
      hod: {
        name: hodname,
        email: hodemail,
        facebookusername: hodfacebookusername,
        phone: hodphone,
        designation: hoddesignation
      },
      registeredevents: { ...preregisteredevents, ...registeredevents }
    };

    dispatch({ type: REMOVE_INSTITUTE_PROFILE_ERROR });
    dispatch({ type: SET_REGISTER_INSTITUTE_PROCESSING });
    axios
      .post(`${ROOT_URL}/institution`, postdata, {
        headers: {
          authorization: localStorage.getItem("MCQ_token")
        }
      })
      .then(response => {
        dispatch({ type: INSTITUTE_PROFILE, payload: response.data.obj });
        dispatch({ type: UNSET_REGISTER_INSTITUTE_PROCESSING });
      })
      .catch(err => {
        dispatch({
          type: INSTITUTE_PROFILE_ERROR,
          payload: err.response.data.message
        });
        dispatch({ type: UNSET_REGISTER_INSTITUTE_PROCESSING });
      });
  };
}

export function fetchInstitute() {
  return function(dispatch) {
    axios
      .get(`${ROOT_URL}/institution`, {
        headers: {
          authorization: localStorage.getItem("MCQ_token")
        }
      })
      .then(response => {
        dispatch({
          type: INSTITUTE_PROFILE,
          payload: response.data
        });
      })
      .catch(err => {
        console.log(err.response);
      });
  };
}

export function authError(error) {
  return {
    type: AUTH_ERROR,
    payload: error
  };
}

export function removeAuthError() {
  return function(dispatch) {
    dispatch({
      type: REMOVE_AUTH_ERROR
    });
  };
}

export function resetInstitute() {
  return function(dispatch) {
    dispatch({
      type: REMOVE_INSTITUTE_PROFILE
    });
  };
}
