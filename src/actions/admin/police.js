import axios from "axios";
import {
  COMMITTEE_SELECTED_GOODCOPMOMENTS,
  COMMITTEE_SELECTED_COVERARTS,
  COMMITTEE_SELECTED_FILMS,
  UPDATE_COMMITTEE_SELECTED_COVERART_SUBMISSION,
  UPDATE_COMMITTEE_SELECTED_FILM_SUBMISSION,
  UPDATE_COMMITTEE_SELECTED_GOODCOPMOMENT_SUBMISSION
} from "../types";
import { ROOT_URL } from "../url";

const dateFromObjectId = function(objectId) {
  return new Date(parseInt(objectId.substring(0, 8), 16) * 1000);
};

const formattedDate = function(objectId) {
  const date = dateFromObjectId(objectId);
  return `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`;
};

export function getCommitteeSelectedGoodcopmoments() {
  return function(dispatch) {
    axios
      .get(`${ROOT_URL}/policeadmin/goodcopmoments/committeeselected`, {
        headers: {
          authorization: localStorage.getItem("MCQ_token")
        }
      })
      .then(response => {
        let goodcopmomentdata = [];
        if (response.data) {
          goodcopmomentdata = response.data
            .reduce(function(reduced, current) {
              const goodcopmomentarray = current.goodcopmoment.map(function(
                row
              ) {
                row.user = current.user;
                return row;
              });
              return [...reduced, ...goodcopmomentarray];
            }, [])
            .map(function(row, index) {
              return {
                id: index + 1,
                _id: row._id,
                name: row.user.name,
                email: row.user.email,
                phone: row.user.phone,
                institute: row.user.institute,
                userid: row.user._id,
                dropboxid: row.dropboxid,
                selected: row.selected,
                committeeselected: row.committeeselected,
                relatedtouser: row.relatedtouser ? "Y" : "N",
                policeadminselected: row.policeadminselected,
                relationdescription: row.relationdescription
                  ? row.relationdescription
                  : "",
                submissiondate: dateFromObjectId(row._id)
              };
            });

          dispatch({
            type: COMMITTEE_SELECTED_GOODCOPMOMENTS,
            payload: {
              committeeselectedgoodcopmoments: goodcopmomentdata
            }
          });
        }
      })
      .catch(error => {
        console.log(error);
      });
  };
}

export function getCommitteeSelectedCoverarts() {
  return function(dispatch) {
    axios
      .get(`${ROOT_URL}/policeadmin/coverarts/committeeselected`, {
        headers: {
          authorization: localStorage.getItem("MCQ_token")
        }
      })
      .then(response => {
        let coverartdata = [];
        if (response.data) {
          coverartdata = response.data
            .reduce(function(reduced, current) {
              const coverartarray = current.coverart.map(function(row) {
                row.user = current.user;
                return row;
              });
              return [...reduced, ...coverartarray];
            }, [])
            .map(function(row, index) {
              return {
                id: index + 1,
                _id: row._id,
                name: row.user.name,
                email: row.user.email,
                phone: row.user.phone,
                institute: row.user.institute,
                userid: row.user._id,
                dropboxid: row.dropboxid,
                selected: row.selected,
                committeeselected: row.committeeselected,
                policeadminselected: row.policeadminselected,
                description: row.description ? row.description : "",
                submissiondate: dateFromObjectId(row._id)
              };
            });

          dispatch({
            type: COMMITTEE_SELECTED_COVERARTS,
            payload: {
              committeeselectedcoverarts: coverartdata
            }
          });
        }
      })
      .catch(error => {
        console.log(error);
      });
  };
}

export function getCommitteeSelectedFilms() {
  return function(dispatch) {
    axios
      .get(`${ROOT_URL}/policeadmin/filmmaking/committeeselected`, {
        headers: {
          authorization: localStorage.getItem("MCQ_token")
        }
      })
      .then(response => {
        let filmdata = [];
        if (response.data) {
          filmdata = response.data
            .reduce(function(reduced, current) {
              const filmarray = current.filmmaking.map(function(row) {
                row.user = current.user;
                return row;
              });
              return [...reduced, ...filmarray];
            }, [])
            .map(function(row, index) {
              return {
                id: index + 1,
                _id: row._id,
                name: row.user.name,
                email: row.user.email,
                phone: row.user.phone,
                institute: row.user.institute,
                userid: row.user._id,
                url: row.url,
                selected: row.selected,
                committeeselected: row.committeeselected,
                policeadminselected: row.policeadminselected,
                description: row.description ? row.description : "",
                submissiondate: dateFromObjectId(row._id)
              };
            });

          dispatch({
            type: COMMITTEE_SELECTED_FILMS,
            payload: {
              committeeselectedfilms: filmdata
            }
          });
        }
      })
      .catch(error => {
        console.log(error);
      });
  };
}

export function verifyGoodcopmoment({
  index,
  name,
  email,
  phone,
  id,
  userid,
  selected,
  institute
}) {
  return function(dispatch) {
    axios
      .patch(
        `${ROOT_URL}/policeadmin/goodcopmoments/${id}`,
        {
          userid: userid,
          selected: selected
        },
        {
          headers: {
            authorization: localStorage.getItem("MCQ_token")
          }
        }
      )
      .then(response => {
        let flatteneddata = {
          id: index + 1,
          _id: response.data._id,
          name: name,
          email: email,
          phone: phone,
          userid: userid,
          dropboxid: response.data.dropboxid,
          institute: institute,
          selected: response.data.selected,
          committeeselected: response.data.committeeselected,
          policeadminselected: response.data.policeadminselected,
          relatedtouser: response.data.relatedtouser ? "Y" : "N",
          relationdescription: response.data.relationdescription
            ? response.data.relationdescription
            : "",
          submissiondate: dateFromObjectId(response.data._id)
        };

        dispatch({
          type: UPDATE_COMMITTEE_SELECTED_GOODCOPMOMENT_SUBMISSION,
          payload: {
            studentdata: flatteneddata,
            index: index
          }
        });
      });
  };
}

export function verifyCoverart({
  index,
  name,
  email,
  phone,
  id,
  userid,
  institute,
  selected
}) {
  return function(dispatch) {
    axios
      .patch(
        `${ROOT_URL}/policeadmin/coverart/${id}`,
        {
          userid: userid,
          selected: selected
        },
        {
          headers: {
            authorization: localStorage.getItem("MCQ_token")
          }
        }
      )
      .then(response => {
        let flatteneddata = {
          id: index + 1,
          _id: response.data._id,
          name: name,
          email: email,
          phone: phone,
          userid: userid,
          institute: institute,
          dropboxid: response.data.dropboxid,
          selected: response.data.selected,
          committeeselected: response.data.committeeselected,
          policeadminselected: response.data.policeadminselected,
          description: response.data.description,
          submissiondate: dateFromObjectId(response.data._id)
        };
        dispatch({
          type: UPDATE_COMMITTEE_SELECTED_COVERART_SUBMISSION,
          payload: {
            studentdata: flatteneddata,
            index: index
          }
        });
      });
  };
}

export function verifyFilm({
  index,
  name,
  email,
  phone,
  id,
  userid,
  institute,
  selected
}) {
  return function(dispatch) {
    axios
      .patch(
        `${ROOT_URL}/policeadmin/filmmaking/${id}`,
        {
          userid: userid,
          selected: selected
        },
        {
          headers: {
            authorization: localStorage.getItem("MCQ_token")
          }
        }
      )
      .then(response => {
        let flatteneddata = {
          id: index + 1,
          _id: response.data._id,
          name: name,
          email: email,
          phone: phone,
          userid: userid,
          institute: institute,
          url: response.data.url,
          selected: response.data.selected,
          committeeselected: response.data.committeeselected,
          policeadminselected: response.data.policeadminselected,
          description: response.data.description,
          submissiondate: dateFromObjectId(response.data._id)
        };

        dispatch({
          type: UPDATE_COMMITTEE_SELECTED_FILM_SUBMISSION,
          payload: {
            studentdata: flatteneddata,
            index: index
          }
        });
      });
  };
}
