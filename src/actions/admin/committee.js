import axios from "axios";
import {
  SELECTED_GOODCOPMOMENTS,
  SELECTED_COVERARTS,
  SELECTED_FILMS,
  UPDATE_SELECTED_GOODCOPMOMENT_SUBMISSION,
  UPDATE_SELECTED_COVERART_SUBMISSION,
  UPDATE_SELECTED_FILM_SUBMISSION
} from "../types";
import { ROOT_URL } from "../url";

const dateFromObjectId = function(objectId) {
  return new Date(parseInt(objectId.substring(0, 8), 16) * 1000);
};

const formattedDate = function(objectId) {
  const date = dateFromObjectId(objectId);
  return `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`;
};

export function getSelectedGoodcopmoments() {
  return function(dispatch) {
    axios
      .get(`${ROOT_URL}/committeeadmin/goodcopmoments/selected`, {
        headers: {
          authorization: localStorage.getItem("MCQ_token")
        }
      })
      .then(response => {
        let goodcopmomentdata = [];
        if (response.data) {
          goodcopmomentdata = response.data
            .reduce(function(reduced, current) {
              const goodcopmomentarray = current.goodcopmoment.map(function(
                row
              ) {
                row.user = current.user;
                return row;
              });
              return [...reduced, ...goodcopmomentarray];
            }, [])
            .map(function(row, index) {
              return {
                id: index + 1,
                _id: row._id,
                name: row.user.name,
                email: row.user.email,
                phone: row.user.phone,
                institute: row.user.institute,
                userid: row.user._id,
                dropboxid: row.dropboxid,
                selected: row.selected,
                committeeselected: row.committeeselected,
                relatedtouser: row.relatedtouser ? "Y" : "N",
                relationdescription: row.relationdescription
                  ? row.relationdescription
                  : "",
                submissiondate: dateFromObjectId(row._id)
              };
            });

          dispatch({
            type: SELECTED_GOODCOPMOMENTS,
            payload: {
              goodcopmoments: goodcopmomentdata
            }
          });
        }
      })
      .catch(error => {
        console.log(error);
      });
  };
}

export function getSelectedCoverarts() {
  return function(dispatch) {
    axios
      .get(`${ROOT_URL}/committeeadmin/coverarts/selected`, {
        headers: {
          authorization: localStorage.getItem("MCQ_token")
        }
      })
      .then(response => {
        let coverartdata = [];
        if (response.data) {
          coverartdata = response.data
            .reduce(function(reduced, current) {
              const coverartarray = current.coverart.map(function(row) {
                row.user = current.user;
                return row;
              });
              return [...reduced, ...coverartarray];
            }, [])
            .map(function(row, index) {
              return {
                id: index + 1,
                _id: row._id,
                name: row.user.name,
                email: row.user.email,
                phone: row.user.phone,
                institute: row.user.institute,
                userid: row.user._id,
                dropboxid: row.dropboxid,
                selected: row.selected,
                committeeselected: row.committeeselected,
                description: row.description ? row.description : "",
                submissiondate: dateFromObjectId(row._id)
              };
            });

          dispatch({
            type: SELECTED_COVERARTS,
            payload: {
              coverarts: coverartdata
            }
          });
        }
      })
      .catch(error => {
        console.log(error);
      });
  };
}

export function getSelectedFilms() {
  return function(dispatch) {
    axios
      .get(`${ROOT_URL}/committeeadmin/filmmaking/selected`, {
        headers: {
          authorization: localStorage.getItem("MCQ_token")
        }
      })
      .then(response => {
        let filmdata = [];
        if (response.data) {
          filmdata = response.data
            .reduce(function(reduced, current) {
              const filmarray = current.filmmaking.map(function(row) {
                row.user = current.user;
                return row;
              });
              return [...reduced, ...filmarray];
            }, [])
            .map(function(row, index) {
              return {
                id: index + 1,
                _id: row._id,
                name: row.user.name,
                email: row.user.email,
                phone: row.user.phone,
                institute: row.user.institute,
                userid: row.user._id,
                url: row.url,
                selected: row.selected,
                committeeselected: row.committeeselected,
                description: row.description ? row.description : "",
                submissiondate: dateFromObjectId(row._id)
              };
            });

          dispatch({
            type: SELECTED_FILMS,
            payload: {
              films: filmdata
            }
          });
        }
      })
      .catch(error => {
        console.log(error);
      });
  };
}

export function verifyGoodcopmoment({
  index,
  name,
  email,
  phone,
  id,
  userid,
  selected,
  institute
}) {
  return function(dispatch) {
    axios
      .patch(
        `${ROOT_URL}/committeeadmin/goodcopmoments/${id}`,
        {
          userid: userid,
          selected: selected
        },
        {
          headers: {
            authorization: localStorage.getItem("MCQ_token")
          }
        }
      )
      .then(response => {
        let flatteneddata = {
          id: index + 1,
          _id: response.data._id,
          name: name,
          email: email,
          phone: phone,
          userid: userid,
          dropboxid: response.data.dropboxid,
          institute: institute,
          selected: response.data.selected,
          committeeselected: response.data.committeeselected,
          relatedtouser: response.data.relatedtouser ? "Y" : "N",
          relationdescription: response.data.relationdescription
            ? response.data.relationdescription
            : "",
          submissiondate: dateFromObjectId(response.data._id)
        };

        dispatch({
          type: UPDATE_SELECTED_GOODCOPMOMENT_SUBMISSION,
          payload: {
            studentdata: flatteneddata,
            index: index
          }
        });
      });
  };
}

export function verifyCoverart({
  index,
  name,
  email,
  phone,
  id,
  userid,
  institute,
  selected
}) {
  return function(dispatch) {
    axios
      .patch(
        `${ROOT_URL}/committeeadmin/coverart/${id}`,
        {
          userid: userid,
          selected: selected
        },
        {
          headers: {
            authorization: localStorage.getItem("MCQ_token")
          }
        }
      )
      .then(response => {
        let flatteneddata = {
          id: index + 1,
          _id: response.data._id,
          name: name,
          email: email,
          phone: phone,
          userid: userid,
          dropboxid: response.data.dropboxid,
          institute: institute,
          selected: response.data.selected,
          committeeselected: response.data.committeeselected,
          description: response.data.description,
          submissiondate: dateFromObjectId(response.data._id)
        };
        dispatch({
          type: UPDATE_SELECTED_COVERART_SUBMISSION,
          payload: {
            studentdata: flatteneddata,
            index: index
          }
        });
      });
  };
}

export function verifyFilm({
  index,
  name,
  email,
  phone,
  id,
  userid,
  institute,
  selected
}) {
  return function(dispatch) {
    axios
      .patch(
        `${ROOT_URL}/committeeadmin/filmmaking/${id}`,
        {
          userid: userid,
          selected: selected
        },
        {
          headers: {
            authorization: localStorage.getItem("MCQ_token")
          }
        }
      )
      .then(response => {
        let flatteneddata = {
          id: index + 1,
          _id: response.data._id,
          name: name,
          email: email,
          phone: phone,
          userid: userid,
          url: response.data.url,
          institute: institute,
          selected: response.data.selected,
          committeeselected: response.data.committeeselected,
          description: response.data.description,
          submissiondate: dateFromObjectId(response.data._id)
        };

        dispatch({
          type: UPDATE_SELECTED_FILM_SUBMISSION,
          payload: {
            studentdata: flatteneddata,
            index: index
          }
        });
      });
  };
}
