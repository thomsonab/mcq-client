import axios from "axios";
import {
  ADMIN_INSTITUTIONS,
  UPDATE_ADMIN_INSTITUTIONS,
  SELECTED_GOODCOPMOMENTS,
  SELECTED_COVERARTS,
  SELECTED_FILMS,
  STUDENTLIST,
  SELECTED_REPORTS
} from "../types";
import { ROOT_URL } from "../url";

const dateFromObjectId = function(objectId) {
  return new Date(parseInt(objectId.substring(0, 8), 16) * 1000);
};

const formattedDate = function(objectId) {
  const date = dateFromObjectId(objectId);
  return `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`;
};

export function updateValidateYN({ index, id, validatedyn }) {
  return function(dispatch) {
    axios
      .patch(
        `${ROOT_URL}/admin/institution/${id}`,
        {
          validatedyn: validatedyn
        },
        {
          headers: {
            authorization: localStorage.getItem("MCQ_token")
          }
        }
      )
      .then(response => {
        let flatteneddata = {
          _id: response.data._id,
          id: index + 1,
          institution: response.data.collegeOrInstitute,
          board: response.data.boardOrUniversity,
          email: response.data.institutionemail,
          phone: response.data.phone,
          created: dateFromObjectId(response.data._id),
          validatedyn: response.data.validatedyn,
          registeredevents: response.data.registeredevents,
          coordinator: response.data.coordinator,
          hod: response.data.hod
        };
        dispatch({
          type: UPDATE_ADMIN_INSTITUTIONS,
          payload: {
            institiondata: flatteneddata,
            index: index
          }
        });
      })
      .catch(error => {
        console.log("No institute");
      });
  };
}

export function fetchReports() {
  return function(dispatch) {
    axios
      .get(`${ROOT_URL}/admin/reports`, {
        headers: {
          authorization: localStorage.getItem("MCQ_token")
        }
      })
      .then(response => {
        let reportdata = [];
        if (response.data) {
          reportdata = response.data
            .reduce(function(reduced, current) {
              const reportarray = current.report.map(function(row) {
                row.user = current.user;
                return row;
              });
              return [...reduced, ...reportarray];
            }, [])
            .map(function(row, index) {
              return {
                id: index + 1,
                _id: row._id,
                name: row.user.name,
                email: row.user.email,
                phone: row.user.phone,
                institute: row.user.institute,
                userid: row.user._id,
                dropboxid: row.dropboxid,
                description: row.description ? row.description : "",
                submissiondate: dateFromObjectId(row._id)
              };
            });
          dispatch({
            type: SELECTED_REPORTS,
            payload: {
              selectedreports: reportdata
            }
          });
        }
      })
      .catch(err => {
        console.log(err);
      });
  };
}
export function fetchStudentList() {
  return function(dispatch) {
    axios
      .get(`${ROOT_URL}/admin/studentlist`, {
        headers: {
          authorization: localStorage.getItem("MCQ_token")
        }
      })
      .then(response => {
        let flatteneddata = response.data
          .filter(function(val) {
            if (!val.user) {
              return false;
            }
            return true;
          })

          .map(function(row, index) {
            return {
              _id: row._id,
              id: index + 1,
              coverart: row.coverart,
              filmmaking: row.filmmaking,
              goodcopmoment: row.goodcopmoment,
              coverartyn: row.registeredevents.coverart ? "Y" : "N",
              filmmakingyn: row.registeredevents.filmmaking ? "Y" : "N",
              goodcopmomentyn: row.registeredevents.gcm ? "Y" : "N",
              name: row.user.name,
              email: row.user.email,
              institute: row.user.institute,
              level: row.user.level,
              phone: row.user.phone
            };
          });
        dispatch({
          type: STUDENTLIST,
          payload: flatteneddata
        });
      })
      .catch(err => {
        console.log(err);
      });
  };
}

export function fetchInstitutions() {
  return function(dispatch) {
    axios
      .get(`${ROOT_URL}/admin/institution`, {
        headers: {
          authorization: localStorage.getItem("MCQ_token")
        }
      })
      .then(response => {
        let flatteneddata = response.data.map(function(row, index) {
          return {
            _id: row._id,
            id: index + 1,
            institution: row.collegeOrInstitute,
            address: `${row.address}, ${row.district}, ${row.pincode}`,
            board: row.boardOrUniversity,
            email: row.institutionemail,
            phone: row.phone || "",
            created: dateFromObjectId(row._id),
            validatedyn: row.validatedyn,
            registeredevents: row.registeredevents,
            coordinator: row.coordinator,
            hod: row.hod,
            /* The below data is specifically for the exports.
             * Ref: https://github.com/AllenFang/react-bootstrap-table/commit/e58f44744a64b377fea3a92e5532c11d453209ec
             */
            coordinatorName: row.coordinator.name,
            coordinatorEmail: row.coordinator.email,
            coordinatorPhone: row.coordinator.phone,
            coordinatorDesignation: row.coordinator.level,
            hoiName: row.hod.name,
            hoiEmail: row.hod.email,
            hoiPhone: row.hod.phone,
            hoiDesignation: row.hod.designation
          };
        });
        dispatch({ type: ADMIN_INSTITUTIONS, payload: flatteneddata });
      })
      .catch(error => {
        console.log("No institute");
      });
  };
}

export function getSelectedGoodcopmoments() {
  return function(dispatch) {
    axios
      .get(`${ROOT_URL}/admin/goodcopmoments/selected`, {
        headers: {
          authorization: localStorage.getItem("MCQ_token")
        }
      })
      .then(response => {
        let goodcopmomentdata = [];
        if (response.data) {
          goodcopmomentdata = response.data
            .reduce(function(reduced, current) {
              const goodcopmomentarray = current.goodcopmoment.map(function(
                row
              ) {
                row.user = current.user;
                return row;
              });
              return [...reduced, ...goodcopmomentarray];
            }, [])
            .map(function(row, index) {
              return {
                id: index + 1,
                _id: row._id,
                name: row.user.name,
                email: row.user.email,
                phone: row.user.phone,
                institute: row.user.institute,
                userid: row.user._id,
                dropboxid: row.dropboxid,
                selected: row.selected,
                committeeselected: row.committeeselected,
                policeadminselected: row.policeadminselected,
                relatedtouser: row.relatedtouser ? "Y" : "N",
                relationdescription: row.relationdescription
                  ? row.relationdescription
                  : "",
                submissiondate: dateFromObjectId(row._id)
              };
            });

          dispatch({
            type: SELECTED_GOODCOPMOMENTS,
            payload: {
              goodcopmoments: goodcopmomentdata
            }
          });
        }
      })
      .catch(error => {
        console.log(error);
      });
  };
}

export function getSelectedCoverarts() {
  return function(dispatch) {
    axios
      .get(`${ROOT_URL}/admin/coverarts/selected`, {
        headers: {
          authorization: localStorage.getItem("MCQ_token")
        }
      })
      .then(response => {
        let coverartdata = [];
        if (response.data) {
          coverartdata = response.data
            .reduce(function(reduced, current) {
              const coverartarray = current.coverart.map(function(row) {
                row.user = current.user;
                return row;
              });
              return [...reduced, ...coverartarray];
            }, [])
            .map(function(row, index) {
              return {
                id: index + 1,
                _id: row._id,
                name: row.user.name,
                email: row.user.email,
                phone: row.user.phone,
                institute: row.user.institute,
                userid: row.user._id,
                dropboxid: row.dropboxid,
                selected: row.selected,
                committeeselected: row.committeeselected,
                policeadminselected: row.policeadminselected,
                description: row.description ? row.description : "",
                submissiondate: dateFromObjectId(row._id)
              };
            });

          dispatch({
            type: SELECTED_COVERARTS,
            payload: {
              coverarts: coverartdata
            }
          });
        }
      })
      .catch(error => {
        console.log(error);
      });
  };
}

export function getSelectedFilms() {
  return function(dispatch) {
    axios
      .get(`${ROOT_URL}/admin/filmmaking/selected`, {
        headers: {
          authorization: localStorage.getItem("MCQ_token")
        }
      })
      .then(response => {
        let filmdata = [];
        if (response.data) {
          filmdata = response.data
            .reduce(function(reduced, current) {
              const filmarray = current.filmmaking.map(function(row) {
                row.user = current.user;
                return row;
              });
              return [...reduced, ...filmarray];
            }, [])
            .map(function(row, index) {
              return {
                id: index + 1,
                _id: row._id,
                name: row.user.name,
                email: row.user.email,
                phone: row.user.phone,
                institute: row.user.institute,
                userid: row.user._id,
                url: row.url,
                selected: row.selected,
                committeeselected: row.committeeselected,
                policeadminselected: row.policeadminselected,
                description: row.description ? row.description : "",
                submissiondate: dateFromObjectId(row._id)
              };
            });

          dispatch({
            type: SELECTED_FILMS,
            payload: {
              films: filmdata
            }
          });
        }
      })
      .catch(error => {
        console.log(error);
      });
  };
}
