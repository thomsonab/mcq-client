import axios from "axios";

import { STUDENT_SUBMISSIONS } from "../types";

import { ROOT_URL } from "../url";
import Dropbox from "dropbox";
import { dateFromObjectId } from "../../utils/date";

export function getSubmissions() {
  return function(dispatch) {
    axios
      .get(`${ROOT_URL}/student/submissions`, {
        headers: {
          authorization: localStorage.getItem("MCQ_token")
        }
      })
      .then(response => {
        let goodcopmomentdata = [];
        let coverartdata = [];
        let filmdata = [];

        if (
          response.data.goodcopmoment &&
          response.data.goodcopmoment.length > 0
        ) {
          goodcopmomentdata = response.data.goodcopmoment.map(function(
            row,
            index
          ) {
            return {
              id: index + 1,
              user: row.user,
              dropboxid: row.dropboxid,
              verified: row.verified,
              selected: row.selected,
              relatedtouser: row.relatedtouser ? "Y" : "N",
              relationdescription: row.relationdescription
                ? row.relationdescription
                : "",
              submissiondate: dateFromObjectId(row._id)
            };
          });
        }

        if (response.data.coverart && response.data.coverart.length > 0) {
          coverartdata = response.data.coverart.map(function(row, index) {
            return {
              id: index + 1,
              user: row.user,
              dropboxid: row.dropboxid,
              verified: row.verified,
              selected: row.selected,
              description: row.description,
              submissiondate: dateFromObjectId(row._id)
            };
          });
        }

        if (response.data.filmmaking && response.data.filmmaking.length > 0) {
          filmdata = response.data.filmmaking.map(function(row, index) {
            return {
              id: index + 1,
              user: row.user,
              url: row.url,
              verified: row.verified,
              selected: row.selected,
              description: row.description,
              submissiondate: dateFromObjectId(row._id)
            };
          });
        }

        dispatch({
          type: STUDENT_SUBMISSIONS,
          payload: {
            goodcopmoment: goodcopmomentdata,
            coverart: coverartdata,
            film: filmdata
          }
        });
      })
      .catch(error => {
        console.log(error);
      });
  };
}

export function downloadFile(id) {
  return function(dispatch) {
    const ACCESS_TOKEN = process.env.REACT_APP_DROPBOX_ACCESS_TOKEN;
    const dbx = new Dropbox({ accessToken: ACCESS_TOKEN });
    dbx
      .filesDownload({ path: id })
      .then(response => {
        console.log(response);
      })
      .catch(err => {
        console.log(err);
      });
  };
}
