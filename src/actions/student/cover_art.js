import axios from "axios";
import {
  COVERART_MOMENT_PDF,
  COVERART_RESET_ERROR,
  COVERART_SIZE_ERROR,
  COVERART_UPLOAD_ERROR,
  COVERART_FILETYPE_ERROR,
  COVERART_SET_SUBMISSION,
  COVERART_UNSET_SUBMISSION,
  STUDENT_SUBMISSIONS
} from "../types";

import { ROOT_URL } from "../url";
import { reset } from "redux-form";
import { dateFromObjectId } from "../../utils/date";

/* clearError
 *
 * Clears all the errors that
 * were present in the state.
 * 
 */
export function clearError() {
  return function(dispatch) {
    dispatch({ type: COVERART_RESET_ERROR });
  };
}

export function uploadCoverart({ file, description }) {
  const pdffile = file[0];
  if (!pdffile) {
    return function(dispatch) {
      dispatch({
        type: COVERART_UPLOAD_ERROR,
        payload: "Please select a file to upload"
      });
    };
  }
  if (pdffile.size > 5500000) {
    return function(dispatch) {
      dispatch({ type: COVERART_SIZE_ERROR });
    };
  }

  if (pdffile.type !== "application/pdf") {
    return function(dispatch) {
      dispatch({ type: COVERART_FILETYPE_ERROR });
    };
  }

  const formData = new FormData();
  formData.append("file", pdffile);
  formData.append("description", description);

  return function(dispatch) {
    dispatch({ type: COVERART_SET_SUBMISSION });
    axios
      .post(`${ROOT_URL}/student/uploadcoverart`, formData, {
        headers: {
          authorization: localStorage.getItem("MCQ_token")
        }
      })
      .then(response => {
        dispatch({ type: COVERART_UNSET_SUBMISSION });
        let goodcopmomentdata = [];
        let coverartdata = [];
        let filmdata = [];
        if (
          response.data.goodcopmoment &&
          response.data.goodcopmoment.length > 0
        ) {
          goodcopmomentdata = response.data.goodcopmoment.map(function(
            row,
            index
          ) {
            return {
              id: index + 1,
              user: row.user,
              dropboxid: row.dropboxid,
              verified: row.verified,
              selected: row.selected,
              relatedtouser: row.relatedtouser ? "Y" : "N",
              relationdescription: row.relationdescription
                ? row.relationdescription
                : "",
              submissiondate: dateFromObjectId(row._id)
            };
          });
        }

        if (response.data.coverart && response.data.coverart.length > 0) {
          coverartdata = response.data.coverart.map(function(row, index) {
            return {
              id: index + 1,
              user: row.user,
              dropboxid: row.dropboxid,
              selected: row.selected,
              verified: row.verified,
              description: row.description,
              submissiondate: dateFromObjectId(row._id)
            };
          });
        }

        if (response.data.filmmaking && response.data.filmmaking.length > 0) {
          filmdata = response.data.filmmaking.map(function(row, index) {
            return {
              id: index + 1,
              user: row.user,
              url: row.url,
              selected: row.selected,
              verified: row.verified,
              description: row.description,
              submissiondate: dateFromObjectId(row._id)
            };
          });
        }

        dispatch({
          type: STUDENT_SUBMISSIONS,
          payload: {
            goodcopmoment: goodcopmomentdata,
            coverart: coverartdata,
            film: filmdata
          }
        });
        alert("Cover Art Submitted");
        dispatch(reset("coverartform"));
      })
      .catch(error => {
        dispatch({ type: COVERART_UPLOAD_ERROR, payload: "Upload failed" });
        dispatch({ type: COVERART_UNSET_SUBMISSION });
      });
  };
}
