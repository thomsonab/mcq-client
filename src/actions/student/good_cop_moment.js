import axios from "axios";
import {
  GOOD_COP_MOMENT_PDF,
  SIZE_ERROR,
  UPLOAD_ERROR,
  RESET_ERROR,
  FILETYPE_ERROR,
  SET_STUDENT_SUBMISSION,
  UNSET_STUDENT_SUBMISSION,
  STUDENT_SUBMISSIONS
} from "../types";
import { ROOT_URL } from "../url";
import { reset } from "redux-form";
import { dateFromObjectId } from "../../utils/date";

/* clearError
 *
 * Clears all the errors that
 * were present in the state.
 * 
 */
export function clearError() {
  return function(dispatch) {
    dispatch({ type: RESET_ERROR });
  };
}

/* uploadGoodCopMoment
 *
 * Uploads the pdf submitted. 
 */
export function uploadGoodCopMoment({
  file,
  relationdescription,
  relatedtouser
}) {
  // file here refers to the file array.
  // we can have multiple pdf documents too.

  const pdffile = file[0];
  if (!pdffile) {
    return function(dispatch) {
      dispatch({
        type: UPLOAD_ERROR,
        payload: "Please select a file to upload"
      });
    };
  }
  if (pdffile.size > 5500000) {
    return function(dispatch) {
      dispatch({ type: SIZE_ERROR });
    };
  }

  if (pdffile.type !== "application/pdf") {
    return function(dispatch) {
      dispatch({ type: FILETYPE_ERROR });
    };
  }
  const formData = new FormData();
  formData.append("file", pdffile);
  formData.append("relatedtouser", relatedtouser);
  formData.append("relationdescription", relationdescription);

  // return function(dispatch) {};
  return function(dispatch) {
    dispatch({ type: SET_STUDENT_SUBMISSION });
    axios
      .post(`${ROOT_URL}/student/uploadgoodcopmoment`, formData, {
        headers: {
          authorization: localStorage.getItem("MCQ_token")
          // "content-type": "multipart/form-data"
        }
      })
      .then(response => {
        //dispatch({ type: GOOD_COP_MOMENT_PDF, payload: response.data.url });
        dispatch({ type: UNSET_STUDENT_SUBMISSION });

        let goodcopmomentdata = [];
        let coverartdata = [];
        let filmdata = [];

        if (
          response.data.goodcopmoment &&
          response.data.goodcopmoment.length > 0
        ) {
          goodcopmomentdata = response.data.goodcopmoment.map(function(
            row,
            index
          ) {
            return {
              id: index + 1,
              user: row.user,
              dropboxid: row.dropboxid,
              verified: row.verified,
              selected: row.selected,
              relatedtouser: row.relatedtouser ? "Y" : "N",
              relationdescription: row.relationdescription
                ? row.relationdescription
                : "",
              submissiondate: dateFromObjectId(row._id)
            };
          });
        }

        if (response.data.coverart && response.data.coverart.length > 0) {
          coverartdata = response.data.coverart.map(function(row, index) {
            return {
              id: index + 1,
              user: row.user,
              dropboxid: row.dropboxid,
              verified: row.verified,
              selected: row.selected,
              description: row.description,
              submissiondate: dateFromObjectId(row._id)
            };
          });
        }

        if (response.data.filmmaking && response.data.filmmaking.length > 0) {
          filmdata = response.data.filmmaking.map(function(row, index) {
            return {
              id: index + 1,
              user: row.user,
              url: row.url,
              verified: row.verified,
              selected: row.selected,
              description: row.description,
              submissiondate: dateFromObjectId(row._id)
            };
          });
        }

        dispatch({
          type: STUDENT_SUBMISSIONS,
          payload: {
            goodcopmoment: goodcopmomentdata,
            coverart: coverartdata,
            film: filmdata
          }
        });

        alert("Good Cop Moment Submitted");
        dispatch(reset("goodcopmomentform"));
      })
      .catch(error => {
        console.error("Upload failed");
        dispatch({ type: UPLOAD_ERROR, payload: "Upload failed" });
        dispatch({ type: UNSET_STUDENT_SUBMISSION });
      });
  };
}
