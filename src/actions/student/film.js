import axios from "axios";
import {
  FILM_RESET_ERROR,
  FILM_SET_SUBMISSION,
  FILM_UNSET_SUBMISSION,
  STUDENT_SUBMISSIONS,
  FILM_SUBMIT_ERROR
} from "../types";

import { ROOT_URL } from "../url";
import { reset } from "redux-form";
import { dateFromObjectId } from "../../utils/date";

/* clearError
 *
 * Clears all the errors that
 * were present in the state.
 * 
 */
export function clearError() {
  return function(dispatch) {
    dispatch({ type: FILM_RESET_ERROR });
  };
}

export function submitFilm({ description, url }) {
  return function(dispatch) {
    dispatch({ type: FILM_SET_SUBMISSION });
    axios
      .post(
        `${ROOT_URL}/student/filmmaking`,
        {
          description: description,
          url: url
        },
        {
          headers: {
            authorization: localStorage.getItem("MCQ_token")
          }
        }
      )
      .then(response => {
        dispatch({ type: FILM_UNSET_SUBMISSION });
        let goodcopmomentdata = [];
        let coverartdata = [];
        let filmdata = [];
        if (
          response.data.goodcopmoment &&
          response.data.goodcopmoment.length > 0
        ) {
          goodcopmomentdata = response.data.goodcopmoment.map(function(
            row,
            index
          ) {
            return {
              id: index + 1,
              user: row.user,
              dropboxid: row.dropboxid,
              verified: row.verified,
              selected: row.selected,
              relatedtouser: row.relatedtouser ? "Y" : "N",
              relationdescription: row.relationdescription
                ? row.relationdescription
                : "",
              submissiondate: dateFromObjectId(row._id)
            };
          });
        }

        if (response.data.coverart && response.data.coverart.length > 0) {
          coverartdata = response.data.coverart.map(function(row, index) {
            return {
              id: index + 1,
              user: row.user,
              dropboxid: row.dropboxid,
              selected: row.selected,
              verified: row.verified,
              description: row.description,
              submissiondate: dateFromObjectId(row._id)
            };
          });
        }

        if (response.data.filmmaking && response.data.filmmaking.length > 0) {
          filmdata = response.data.filmmaking.map(function(row, index) {
            return {
              id: index + 1,
              user: row.user,
              url: row.url,
              verified: row.verified,
              selected: row.selected,
              description: row.description,
              submissiondate: dateFromObjectId(row._id)
            };
          });
        }

        dispatch({
          type: STUDENT_SUBMISSIONS,
          payload: {
            goodcopmoment: goodcopmomentdata,
            coverart: coverartdata,
            film: filmdata
          }
        });
        alert("Short film entry Submitted");
        dispatch(reset("filmform"));
      })
      .catch(error => {
        console.log(error);
        dispatch({ type: FILM_SUBMIT_ERROR, payload: "Submission failed" });
        dispatch({ type: FILM_UNSET_SUBMISSION });
      });
  };
}
