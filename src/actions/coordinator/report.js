import axios from "axios";
import {
  REPORT_MOMENT_PDF,
  REPORT_RESET_ERROR,
  REPORT_SIZE_ERROR,
  REPORT_UPLOAD_ERROR,
  REPORT_FILETYPE_ERROR,
  REPORT_SET_SUBMISSION,
  REPORT_UNSET_SUBMISSION,
  COORDINATOR_SUBMISSIONS
} from "../types";
import { ROOT_URL } from "../url";
import { reset } from "redux-form";
import { dateFromObjectId } from "../../utils/date";

import Dropbox from "dropbox";

/* clearError
 *
 * Clears all the errors that
 * were present in the state.
 * 
 */
export function clearError() {
  return function(dispatch) {
    dispatch({ type: REPORT_RESET_ERROR });
  };
}

export function getCoordinatorReportSubmissions() {
  return function(dispatch) {
    axios
      .get(`${ROOT_URL}/coordinator/reportsubmissions`, {
        headers: {
          authorization: localStorage.getItem("MCQ_token")
        }
      })
      .then(response => {
        let reportdata = [];

        if (response.data.report && response.data.report.length > 0) {
          reportdata = response.data.report.map(function(row, index) {
            return {
              id: index + 1,
              user: row.user,
              dropboxid: row.dropboxid,
              verified: row.verified,

              description: row.description ? row.description : "",
              submissiondate: dateFromObjectId(row._id)
            };
          });
        }

        dispatch({
          type: COORDINATOR_SUBMISSIONS,
          payload: {
            report: reportdata
          }
        });
      })
      .catch(error => {
        console.log(error);
      });
  };
}

export function downloadFile(id) {
  return function(dispatch) {
    const ACCESS_TOKEN = process.env.REACT_APP_DROPBOX_ACCESS_TOKEN;
    const dbx = new Dropbox({ accessToken: ACCESS_TOKEN });
    dbx
      .filesDownload({ path: id })
      .then(response => {
        console.log(response);
      })
      .catch(err => {
        console.log(err);
      });
  };
}

export function uploadReportdoc({ file, description }) {
  const pdffile = file[0];
  if (!pdffile) {
    return function(dispatch) {
      dispatch({
        type: REPORT_UPLOAD_ERROR,
        payload: "Please select a file to upload"
      });
    };
  }
  if (pdffile.size > 10500000) {
    return function(dispatch) {
      dispatch({ type: REPORT_SIZE_ERROR });
    };
  }

  if (pdffile.type !== "application/pdf") {
    return function(dispatch) {
      dispatch({ type: REPORT_FILETYPE_ERROR });
    };
  }

  const formData = new FormData();
  formData.append("file", pdffile);
  formData.append("description", description);

  return function(dispatch) {
    dispatch({ type: REPORT_SET_SUBMISSION });
    axios
      .post(`${ROOT_URL}/coordinator/uploadreport`, formData, {
        headers: {
          authorization: localStorage.getItem("MCQ_token")
        }
      })
      .then(response => {
        dispatch({ type: REPORT_UNSET_SUBMISSION });
        alert("Report Submitted");

        let reportdata = [];
        if (response.data.report && response.data.report.length > 0) {
          reportdata = response.data.report.map(function(row, index) {
            return {
              id: index + 1,
              user: row.user,
              dropboxid: row.dropboxid,
              verified: row.verified,

              description: row.description ? row.description : "",
              submissiondate: dateFromObjectId(row._id)
            };
          });
        }
        dispatch({
          type: COORDINATOR_SUBMISSIONS,
          payload: {
            report: reportdata
          }
        });
        dispatch(reset("reportform"));
      })
      .catch(error => {
        dispatch({ type: REPORT_UPLOAD_ERROR, payload: "Upload failed" });
        dispatch({ type: REPORT_UNSET_SUBMISSION });
      });
  };
}
