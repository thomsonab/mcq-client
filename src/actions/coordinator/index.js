import axios from "axios";

import {
  COORDINATOR_SUBMISSIONS,
  UPDATE_GOODCOPMOMENT_SUBMISSION,
  UPDATE_COVERART_SUBMISSION,
  TAB_SELECTED,
  UPDATE_FILM_SUBMISSION
} from "../types";

import { ROOT_URL } from "../url";
import Dropbox from "dropbox";
import { dateFromObjectId } from "../../utils/date";

export function verifyFilm({
  index,
  name,
  email,
  phone,
  id,
  userid,
  selected
}) {
  return function(dispatch) {
    axios
      .patch(
        `${ROOT_URL}/coordinator/filmmaking/${id}`,
        {
          userid: userid,
          selected: selected
        },
        {
          headers: {
            authorization: localStorage.getItem("MCQ_token")
          }
        }
      )
      .then(response => {
        let flatteneddata = {
          id: index + 1,
          _id: response.data._id,
          name: name,
          email: email,
          phone: phone,
          userid: userid,
          url: response.data.url,
          selected: response.data.selected,
          description: response.data.description,
          submissiondate: dateFromObjectId(response.data._id)
        };

        dispatch({
          type: UPDATE_FILM_SUBMISSION,
          payload: {
            studentdata: flatteneddata,
            index: index
          }
        });
      });
  };
}

export function verifyCoverart({
  index,
  name,
  email,
  phone,
  id,
  userid,
  selected
}) {
  return function(dispatch) {
    axios
      .patch(
        `${ROOT_URL}/coordinator/coverart/${id}`,
        {
          userid: userid,
          selected: selected
        },
        {
          headers: {
            authorization: localStorage.getItem("MCQ_token")
          }
        }
      )
      .then(response => {
        let flatteneddata = {
          id: index + 1,
          _id: response.data._id,
          name: name,
          email: email,
          phone: phone,
          userid: userid,
          dropboxid: response.data.dropboxid,
          selected: response.data.selected,
          description: response.data.description,
          submissiondate: dateFromObjectId(response.data._id)
        };
        dispatch({
          type: UPDATE_COVERART_SUBMISSION,
          payload: {
            studentdata: flatteneddata,
            index: index
          }
        });
      });
  };
}

export function verifyGoodcopmoment({
  index,
  name,
  email,
  phone,
  id,
  userid,
  selected
}) {
  return function(dispatch) {
    axios
      .patch(
        `${ROOT_URL}/coordinator/goodcopmoments/${id}`,
        {
          userid: userid,
          selected: selected
        },
        {
          headers: {
            authorization: localStorage.getItem("MCQ_token")
          }
        }
      )
      .then(response => {
        let flatteneddata = {
          id: index + 1,
          _id: response.data._id,
          name: name,
          email: email,
          phone: phone,
          userid: userid,
          dropboxid: response.data.dropboxid,
          selected: response.data.selected,
          relatedtouser: response.data.relatedtouser ? "Y" : "N",
          relationdescription: response.data.relationdescription
            ? response.data.relationdescription
            : "",
          submissiondate: dateFromObjectId(response.data._id)
        };

        dispatch({
          type: UPDATE_GOODCOPMOMENT_SUBMISSION,
          payload: {
            studentdata: flatteneddata,
            index: index
          }
        });
      });
  };
}
export function getGoodcopmoments() {
  return function(dispatch) {
    axios
      .get(`${ROOT_URL}/coordinator/goodcopmoments`, {
        headers: {
          authorization: localStorage.getItem("MCQ_token")
        }
      })
      .then(response => {
        let goodcopmomentdata = [];
        let coverartdata = [];
        let filmdata = [];
        if (response.data) {
          goodcopmomentdata = response.data
            .reduce(function(reduced, current) {
              const goodcopmomentarray = current.goodcopmoment.map(function(
                row
              ) {
                row.user = current.user;
                return row;
              });

              return [...reduced, ...goodcopmomentarray];
            }, [])
            .map(function(row, index) {
              return {
                id: index + 1,
                _id: row._id,
                name: row.user.name,
                email: row.user.email,
                phone: row.user.phone,
                userid: row.user._id,
                dropboxid: row.dropboxid,
                selected: row.selected,
                relatedtouser: row.relatedtouser ? "Y" : "N",
                relationdescription: row.relationdescription
                  ? row.relationdescription
                  : "",
                submissiondate: dateFromObjectId(row._id)
              };
            });

          coverartdata = response.data
            .reduce(function(reduced, current) {
              const coverartarray = current.coverart.map(function(row) {
                row.user = current.user;
                return row;
              });
              return [...reduced, ...coverartarray];
            }, [])
            .map(function(row, index) {
              return {
                id: index + 1,
                _id: row._id,
                name: row.user.name,
                email: row.user.email,
                phone: row.user.phone,
                userid: row.user._id,
                dropboxid: row.dropboxid,
                selected: row.selected,
                description: row.description,
                submissiondate: dateFromObjectId(row._id)
              };
            });

          filmdata = response.data
            .reduce(function(reduced, current) {
              const filmarray = current.filmmaking.map(function(row) {
                row.user = current.user;
                return row;
              });
              return [...reduced, ...filmarray];
            }, [])
            .map(function(row, index) {
              return {
                id: index + 1,
                _id: row._id,
                name: row.user.name,
                email: row.user.email,
                phone: row.user.phone,
                userid: row.user._id,
                url: row.url,
                selected: row.selected,
                description: row.description,
                submissiondate: dateFromObjectId(row._id)
              };
            });
        }
        dispatch({
          type: COORDINATOR_SUBMISSIONS,
          payload: {
            goodcopmoments: goodcopmomentdata,
            coverart: coverartdata,
            film: filmdata
          }
        });

        // if (goodcopmomentdata.length > 0) {
        //   dispatch({
        //     type: TAB_SELECTED,
        //     payload: 3 // I hate magic numbers. Here 3rd tab corresponds to good cop moment.
        //     // There should be a better way to do this.
        //   });
        // }
      })
      .catch(error => {
        console.log(error);
      });
  };
}

export function downloadFile(id) {
  return function(dispatch) {
    const ACCESS_TOKEN = process.env.REACT_APP_DROPBOX_ACCESS_TOKEN;
    const dbx = new Dropbox({ accessToken: ACCESS_TOKEN });
    dbx
      .filesDownload({ path: id })
      .then(response => {
        console.log(response);
      })
      .catch(err => {
        console.log(err);
      });
  };
}

export function switchTabTo(key) {
  return function(dispatch) {
    dispatch({
      type: TAB_SELECTED,
      payload: key
    });
  };
}

export function resetCoordinator() {
  return function(dispatch) {
    dispatch({
      type: COORDINATOR_SUBMISSIONS,
      payload: {
        goodcopmoments: [],
        coverartdata: [],
        filmdata: []
      }
    });
  };
}
