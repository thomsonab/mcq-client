import React from "react";

export default function(props) {
  return (
    <div className="container">
      <h2>
        {" "}
        <a href="http://aggu.media/em/forms/pfw">Click here</a> to register your
        institution for Police Friendship Week Celebration{" "}
      </h2>
      <h2>
        {" "}
        <a href="http://aggu.media/em/forms/ssp">Click here</a> to register your
        institution for Safety Patrol Team Training{" "}
      </h2>
      <br />
      <h2>
        {" "}
        <a href="http://aggu.media/em/forms/query">Click here</a> for general
        queries{" "}
      </h2>
    </div>
  );
}
