import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import * as actions from "../actions";

import { ProgressBar } from "react-bootstrap";
import quizhead from "../static/images/quizhead.png";
import LeftPanel from "./common/leftpanel";
import RightPanel from "./common/rightpanel";
import QuizHead from "./common/quizhead";

class Questions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      progress: false
    };
  }

  componentWillMount() {
    this.props.fetchQuestions();
  }

  handleFormSubmit(formValues) {
    this.setState({
      progress: true
    });
    this.props.submitAnswers(formValues);
  }

  renderHTML(fields) {
    return <div />;
  }

  render() {
    const { handleSubmit } = this.props;
    const questionarray = this.props.questionlist;
    if (!questionarray) {
      return (
        <section className="quiz">
          <div className="container">
            <div className="row">
              <LeftPanel>
                <Link to="/studentdashboard">
                  <button className="btn btn-sidemenu">Dashboard</button>
                </Link>
              </LeftPanel>
              <RightPanel>
                <QuizHead />
              </RightPanel>
            </div>
          </div>
        </section>
      );
    }
    const fieldsarray = questionarray.map(function(element, index) {
      const choices = element.choices;
      const choicesHTML = choices.map(function(choice, i) {
        return (
          <label className="radio-inline" key={i + 1}>
            <Field
              name={element._id}
              component="input"
              type="radio"
              value={choice}
            />{" "}
            {choice}
          </label>
        );
      });

      return (
        <div className="form-group">
          <div key={index + 1}>
            <label className="control-label col-md-12">{element.name}</label>
            <div className="col-md-12">{choicesHTML}</div>
          </div>
          <br />
          <br />
          <br />
        </div>
      );
    });

    return (
      <section className="quiz">
        <div className="container">
          <div className="row">
            <LeftPanel>
              <Link to="/studentdashboard">
                <button className="btn btn-sidemenu">Dashboard</button>
              </Link>
            </LeftPanel>
            <RightPanel>
              <QuizHead />
              <div className="formo">
                <div className="panel panel-default">
                  {this.state.progress ? (
                    <ProgressBar active={true} striped now={100} />
                  ) : (
                    <div />
                  )}
                  <div className="panel-body">
                    <form
                      onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}
                    >
                      {fieldsarray}
                      <button action="submit" className="btn btn-primary">
                        Submit
                      </button>
                    </form>
                  </div>
                </div>
              </div>
            </RightPanel>
          </div>
        </div>
      </section>
    );
  }
}

function mapStateToProps(state) {
  return {
    questionlist: state.questions.questionlist
  };
}
export default reduxForm({
  form: "questionsForm"
})(connect(mapStateToProps, actions)(Questions));
