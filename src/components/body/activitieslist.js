import React from "react";
import { Link } from "react-router-dom";

export default function(props) {
  return (
    <section className="linkers" id="linkers">
      <div className="container">
        <div className="row">
          <div className="col-md-4 " style={{ padding: "0px" }}>
            <div className="cute-box text-right">
              <h2>
                Share your<br /> Good Cop Moment
              </h2>
              <small>Get a chance to meet your heroes</small>
              <br />
              <br />
              <Link to="/activities/1" className="btn btn-danger">
                Know More
              </Link>
            </div>
          </div>
          <div className="col-md-4 " style={{ padding: "0px" }}>
            <div className="cute-box">
              <h2>Design a cover art for ‘Book of Sagas’</h2>
              <small>
                Best design will feature the cover of ‘Book of Sagas’- to be
                released at OCOH Forum
              </small>
              <br />
              <br />
              <Link to="/activities/3" className="btn btn-danger">
                Know More
              </Link>
            </div>
          </div>
          <div className="col-md-4 " style={{ padding: "0px" }}>
            <div className="cute-box">
              <h2>Film Making on the theme ‘OCOH’</h2>
              <small>
                Best films will be aired at OCOH Forum in front of Distinguished
                Dignitaries &amp; Elite crowd
              </small>
              <br />
              <br />
              <Link to="/activities/2" className="btn btn-danger">
                Know More
              </Link>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-4 text-right" style={{ padding: "0px" }}>
            <div className="cute-box">
              <h2>Are you an Institution ?</h2>Do you have Ideas / Suggestions{" "}
              <br />to make our cops the best in className?<br />
              <br />
              <small>
                Best Ideas / Suggestion institute will get an opportunity at
                OCOH Forum to express their Idea
              </small>
            </div>
          </div>
          <div className="col-md-8 " style={{ padding: "0px" }}>
            <div className="cute-box">
              <h2>
                Cop Experience | Student Safety Awareness | Road Safety
                Awareness
              </h2>
              <small>
                Do you want your institue to be the exemplary in showing the
                well deserved appreciation to our law enforcement friends and
                have your students actively participate in knowing them better?
              </small>
              <br />
              <br />
              <Link to="/signup" className="btn btn-danger">
                Register Now
              </Link>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
