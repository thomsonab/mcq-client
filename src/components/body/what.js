import React from "react";
import what_jpg from "../../static/images/what/16.png";

export default function(props) {
  return (
    <section id="what" className="forma">
      <div className="container">
        <div className="row">
          <div className="col-md-8 text-right">
            <div className="section-heading text-right">
              <h2 className="mb-0">What is OCOH?</h2>
              <p className="lead my-0">Our Cops Our Heroes</p>
            </div>
            <blockquote className="blockquote blockquote-reverse full_b">
              <p className="mb-0">
                The idea is to showcase simple or complex; but effective work
                our cops do, to make our life safe and comfortable. Our Police
                officers spend a lot of time doing ordinary things under extra
                ordinary situations. Whether clearing the traffic, assisting a
                child or an elderly person cross the road, push starting a
                vehicle stuck in traffic or tackling raging mafia and
                anti-social threats in our own community. They provide a service
                of a para important that keeps us safe and comfortable, which is
                often ignored by a few and far mishaps.
              </p>
            </blockquote>
            <p className="">
              <span className="small_t">
                <span className="bold">
                  Our Cops Our Hero’s is a CSR initiative of Education Matters
                </span>{" "}
                in association with
                <br /> Chennai Sahodaya School Complex, Greater Chennai Police
                and Chennai City Home Guards
                <br />{" "}
              </span>
              <span className="big_t">
                that aims to cultivate a youth community<br /> that commends
                police officers who do a great job<br />
              </span>
              <br />{" "}
            </p>
          </div>
          <div className="col-md-4 ">
            <div>
              <img src={what_jpg} alt="..." />
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
