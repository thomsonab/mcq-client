import React, { Component } from "react";
import { Button, Modal } from "react-bootstrap";
import { Link } from "react-router-dom";
import { LinkContainer } from "react-router-bootstrap";

import jumbo_img from "../../static/images/jumbo.png";

class JumboImage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false
    };
  }
  onClickJumboImage() {
    this.setState({ showModal: true });
  }

  close() {
    this.setState({ showModal: false });
  }

  render() {
    return (
      <div className="row">
        {" "}
        <Link to="/studentdashboard">
          <img
            src={jumbo_img}
            style={{ width: "100%", cursor: "pointer" }}

            // onClick={this.onClickJumboImage.bind(this)}
          />
        </Link>
        {/* <Modal show={this.state.showModal} onHide={this.close.bind(this)}>
          <Modal.Header closeButton>
            {" "}
            <h4>Our Cops Our Heroes</h4>{" "}
          </Modal.Header>
          <Modal.Body>
            <div className="container">
              Not yet open for entries. Please check back later.
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.close.bind(this)}>Close</Button>
          </Modal.Footer>
        </Modal> */}
      </div>
    );
  }
}

export default JumboImage;
