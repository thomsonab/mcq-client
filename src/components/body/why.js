import React from "react";

// Static files
import copsilhoutte from "../../static/images/why/15.png";
import whytitle from "../../static/images/why/why_title.jpg";

export default function(props) {
  return (
    <section id="why" className="forma">
      <div className="container">
        <div className="row">
          <div className="col-md-5">
            <img src={copsilhoutte} alt="" />
          </div>
          <div className="col-md-6 text-right align-middle">
            <br />
            <br />
            <br />
            <span className="bold">Let's be honest.</span>
            <br />
            <br /> Not all police officers are good, just as there are not so
            good people in any profession. But should the isolated actions of a
            few speak for the overwhelming majority?
            <br /> We don't think so. Most of our police officers are
            good-willed, hardworking people who deserve our gratitude, respect,
            and recognition.
            <br />
            <br />{" "}
            <span className="bolder">
              Our initiative “OUR COP OUR HEROES” isn't about politics, race, or
              religion. All of us have been positively impacted by law
              enforcement, and now it is the time to show them our appreciation.
            </span>
            <br />
            <br /> Our initiative encourages students to appreciate the positive
            impact of our police department on the community, by indulging them
            in various activities such as taking them for a visit to the nearby
            police station, RSP Volunteering, Safety Awareness Campaigns, etc.
            <br />
            <br />
          </div>
          <div className="col-md-1 desk-only">
            <img src={whytitle} alt="" />
          </div>
        </div>
      </div>
    </section>
  );
}
