import React from "react";
import ClickSlider from "../clickslider/clickslidewrapper";

export default function(props) {
  return (
    <section id="how" className="install how-to-process">
      <div className="container">
        <div className="section-heading text-center">
          <h2 className="mb-0">How it works</h2>
          <p className="lead my-0">
            Our Cops Our Heroes - Share a Good Cop Moment
          </p>
        </div>
        <ClickSlider />
      </div>
    </section>
  );
}
