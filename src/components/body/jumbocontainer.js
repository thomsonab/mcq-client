import React from "react";
import { Link } from "react-router-dom";

import JumboImage from "./jumboimage";

import quizhead from "../../static/images/quizhead.png";

import emimage1 from "../../static/images/emimages/1.png";
import emimage2 from "../../static/images/emimages/2.png";
import emimage3 from "../../static/images/emimages/3.png";
import emimage4 from "../../static/images/emimages/4.png";

export default function(props) {
  return (
    <section className="jumbo">
      <div className="container">
        <JumboImage />
        <div className="row patron">
          <div className="col-md-3 col-xs-6">
            <a href="http://edmat.org">
              <img src={emimage1} alt="" />
            </a>
          </div>
          <div className="col-md-3 col-xs-6">
            <a href="http://www.chennaisahodaya.org">
              <img src={emimage2} alt="" />
            </a>
          </div>
          <div className="col-md-3 col-xs-6">
            <a href="http://www.tnpolice.gov.in">
              <img src={emimage3} alt="" />
            </a>
          </div>
          <div className="col-md-3 col-xs-6">
            <a href="http://">
              <img src={emimage4} alt="" />
            </a>
          </div>
        </div>
      </div>
    </section>
  );
}
