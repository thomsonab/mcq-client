import React from "react";
import who_jpg from "../../static/images/who/who1.jpg";

export default function(props) {
  return (
    <section id="who" className="forma">
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <img src={who_jpg} alt="" />
          </div>
        </div>
      </div>
    </section>
  );
}
