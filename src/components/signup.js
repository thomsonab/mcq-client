import React, { Component } from "react";
import { Field, FormSection, reduxForm } from "redux-form";
import { Link } from "react-router-dom";
import * as actions from "../actions";
import { connect } from "react-redux";

import CheckBoxesForm from "./student/checkboxesform";
import VirtualizedSelect from "react-virtualized-select";
import "react-select/dist/react-select.css";
import "react-virtualized/styles.css";
import "react-virtualized-select/styles.css";

class Signup extends Component {
  constructor(props) {
    super(props);
    this.renderAutocompleteField = this.renderAutocompleteField.bind(this);
  }
  componentWillMount() {
    if (this.props.institutelist.length === 0) {
      this.props.getInstituteList();
    }
    this.props.removeAuthError();
  }

  handleFormSubmit(formProps) {
    this.props._showProgress();

    this.props.signupUser(formProps);
  }

  renderAutocompleteField(field) {
    const { meta: { touched, error } } = field;
    const classNameforField = `form-group ${
      touched && error ? "has-error" : ""
    }`;
    return (
      <div className={classNameforField}>
        <label className="control-label col-md-4" htmlfor={field.name}>
          {field.label}
          {field.info ? (
            <span>
              <br />
              <small id="emailHelp" className="form-text text-muted">
                {field.info}
              </small>
            </span>
          ) : (
            <br />
          )}
        </label>
        <div className="col-md-8">
          <VirtualizedSelect
            options={this.props.institutelist}
            {...field.input}
            placeholder={field.placeholder}
            onBlur={() => {
              field.input.onBlur(field.input.value);
            }}
            labelKey="name"
            valueKey="name"
          />
          {touched && error && <div className="error">{error}</div>}
        </div>
      </div>
    );
  }

  renderLinkField() {
    if (this.props.form.values) {
      if (this.props.form.values.school) {
        if (this.props.form.values.school.name === "Others") {
          return (
            <div className="form-group">
              <label className="control-label col-md-4" />
              <div className="col-md-8">
                If you are not able to find your institute name please{" "}
                <a
                  href="https://goo.gl/forms/Fv4Hzh2tE5Gps5TL2"
                  style={{ color: "red" }}
                >
                  click here
                </a>{" "}
                to add your institute name
              </div>
            </div>

            // <Field
            //   name="institute_custom"
            //   component={this.renderFormField}
            //   type="text"
            //   placeholder="Enter your affiliated institution name"
            // />
          );
        }
      }
      return <div />;
    }
  }

  renderFormField(field) {
    const { meta: { touched, error } } = field;

    const classNameforField = `form-group ${
      touched && error ? "has-error" : ""
    }`;
    return (
      <div className={classNameforField}>
        <label className="control-label col-md-4" htmlfor={field.name}>
          {field.label}
          {field.info ? (
            <span>
              <br />
              <small id="emailHelp" className="form-text text-muted">
                {field.info}
              </small>
            </span>
          ) : (
            <br />
          )}
        </label>
        {field.label === "Your facebook username" ? (
          <div className="col-md-2" style={{ "padding-top": 5 }}>
            www.facebook.com/
          </div>
        ) : (
          <div>{field.name}</div>
        )}
        <div
          className={
            field.label === "Your facebook username" ? "col-md-6" : "col-md-8"
          }
        >
          <input
            id={field.name}
            type={field.type}
            {...field.input}
            placeholder={field.placeholder}
            className="form-control"
          />
          {touched && error && <div className="error">{error}</div>}
        </div>
      </div>
    );
  }

  renderSelectField(field) {
    const { meta: { touched, error } } = field;

    // const classNameforField = `form-group ${
    //   touched && error ? "has-error" : ""
    // }`;
    return (
      <div className="form-group">
        <label className="control-label col-md-4" htmlfor={field.name}>
          {field.label}
          <br />
          <small className="form-text text-muted">{field.info}</small>
        </label>
        <div className="col-md-3">
          <select className="form-control" id="class" {...field.input}>
            <option selected="selected" />
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
            <option>6</option>
            <option>7</option>
            <option>8</option>
            <option>9</option>
            <option>10</option>
            <option>+1</option>
            <option>+2</option>
            <option>Diploma</option>
            <option>UG</option>
            <option>PG</option>
          </select>
          {touched && error && <div className="error">{error}</div>}
        </div>
      </div>
    );
  }

  renderAlert() {
    if (this.props.errorMessage) {
      this.props._hideProgress();
      return (
        <div className="alert alert-danger">
          <strong> Oops!</strong> {this.props.errorMessage}
        </div>
      );
    }
  }

  render() {
    const { handleSubmit, pristine, submitting } = this.props;

    return (
      <form
        className="form-horizontal"
        onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}
      >
        <Field
          name="name"
          component={this.renderFormField}
          label="Name"
          type="text"
          placeholder="Enter your full name for certificate"
          info="Please enter correct name for certificate"
        />
        <Field
          name="level"
          component={this.renderSelectField}
          label="Class/Level"
          type="select"
          info="Your current education qualification"
        />
        {/* <Field
          name="school"
          component={this.renderFormField}
          label="Institution/School"
          type="text"
          placeholder="Enter your institution/school name you are affiliated with"
          info="Enter institution/school name"
        /> */}

        <Field
          name="school"
          component={this.renderAutocompleteField}
          label="Institution/School"
          type="autocomplete"
          placeholder="Select institution/school you are affiliated with"
          info="Please select the institution/school you are affiliated with"
        />

        {this.renderLinkField()}

        <Field
          name="facebookusername"
          component={this.renderFormField}
          label="Your facebook username"
          type="text"
          placeholder="Please enter facebookusername"
          info="We wont share the info with anyone"
        />
        {/* <Field
                        name="area"
                        component={this.renderFormField}
                        type="text"
                        label="Area"
                        placeholder="Enter the place you stay"
                      /> */}

        <Field
          name="email"
          component={this.renderFormField}
          label="Email"
          type="email"
          placeholder="Enter your email id"
          info="Please enter correct email for verification"
        />

        <Field
          name="phone"
          component={this.renderFormField}
          label="Phone"
          type="text"
          placeholder="Enter your phone number"
          info="Please enter phone number for further communication"
        />

        <Field
          name="password"
          component={this.renderFormField}
          label="Password"
          type="password"
          placeholder="Enter password"
        />

        <Field
          name="confirmpassword"
          component={this.renderFormField}
          label="Confirm Password"
          type="password"
          placeholder="Enter the same password entered above"
        />
        <FormSection name="registeredevents">
          <CheckBoxesForm />
        </FormSection>
        {this.renderAlert()}
        <div className="col-md-4" />
        <button
          action="submit"
          className="btn btn-primary"
          disabled={submitting}
        >
          Sign up
        </button>
        <Link className="btn btn-danger" to="/">
          Cancel
        </Link>
      </form>
    );
  }
}

function validate(values) {
  const errors = {};
  if (!values.name) {
    errors.name = "Please Enter name";
  }
  if (!values.level) {
    errors.level = "Please Enter Class/Level info";
  }
  if (!values.school) {
    errors.school = "Please Enter school name";
  }

  if (values.school && values.school.name === "Others") {
    errors.school =
      "Others is not permitted. Please follow the instructions below";
  }

  if (!values.email) {
    errors.email = "Please Enter a valid email address";
  }

  if (!values.phone) {
    errors.phone = "Please Enter phone number of the institution";
  }

  if (values.phone && isNaN(Number(values.phone))) {
    errors.phone = "Phone number should be a number";
  }
  if (!values.password) {
    errors.password = "Please enter a password";
  }
  if (values.confirmpassword !== values.password) {
    errors.confirmpassword = "The password entered doesnt match";
  }

  return errors;
}

function mapStateToProps(state) {
  return {
    errorMessage: state.auth.error,
    institutelist: state.institute.institutelist,
    form: state.form.signup
  };
}

export default reduxForm({
  form: "signup",
  validate: validate
})(connect(mapStateToProps, actions)(Signup));
