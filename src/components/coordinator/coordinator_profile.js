import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
import { Link } from "react-router-dom";
import * as actions from "../../actions/user";
import { connect } from "react-redux";
import "../admin/static/validatebutton.css";

class CoordinatorProfile extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    const user = this.props.user;
    if (user) {
      this.props.initialize({
        name: user.name,
        institute: user.institute,
        designation: user.level,
        facebookusername: user.facebookusername,
        email: user.email,
        phone: user.phone
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.user !== nextProps.user) {
      const user = nextProps.user;
      this.props.initialize({
        name: user.name,
        institute: user.institute,
        designation: user.level,
        facebookusername: user.facebookusername,
        email: user.email,
        phone: user.phone
      });
    }
  }
  handleFormSubmit(formProps) {
    this.props.updateUser(formProps);
  }

  renderAlert() {
    if (this.props.errorMessage) {
      return (
        <div className="alert alert-danger">
          <strong> Oops!</strong> {this.props.errorMessage}
        </div>
      );
    }
  }

  renderFormField(field) {
    const { meta: { touched, error } } = field;

    const classNameforField = `form-group ${
      touched && error ? "has-error" : ""
    }`;
    return (
      <div className={classNameforField}>
        <label className="control-label col-md-4" htmlFor={field.name}>
          {field.label}
          {field.info ? (
            <span>
              <br />
              <small id="emailHelp" className="form-text text-muted">
                {field.info}
              </small>
            </span>
          ) : (
            <br />
          )}
        </label>
        {field.label.match(/facebook/g) ? (
          <div className="col-md-2" style={{ paddingTop: 5 }}>
            www.facebook.com/
          </div>
        ) : (
          <div>{field.name}</div>
        )}
        <div
          className={field.label.match(/facebook/g) ? "col-md-6" : "col-md-8"}
        >
          {field.type === "textarea" ? (
            <textarea
              id={field.name}
              type={field.type}
              {...field.input}
              placeholder={field.placeholder}
              className="form-control"
            />
          ) : (
            <input
              id={field.name}
              type={field.type}
              {...field.input}
              disabled={field.disabled ? true : false}
              placeholder={field.placeholder}
              className="form-control"
            />
          )}

          {touched && error && <div className="error">{error}</div>}
        </div>
      </div>
    );
  }

  render() {
    const { handleSubmit, pristine, submitting } = this.props;

    return (
      <div>
        <span className="text-center">
          <h1>Coordinator Profile</h1>
        </span>
        <form
          className="form-horizontal"
          onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}
        >
          <Field
            name="name"
            component={this.renderFormField}
            label="Name"
            type="text"
            placeholder="Enter your full name"
            info="Please enter your full name"
          />

          {/* <Field
            name="institute"
            component={this.renderFormField}
            label="Institution/School"
            type="text"
            placeholder="Enter your affiliated institution name"
            info="Please enter affiliated institution name"
          /> */}

          <Field
            name="institute"
            component={this.renderFormField}
            label="Institution/School"
            type="text"
            disabled={true}
            placeholder="Select your affiliated institution name"
            info="If your institution doesnt show up, select Others"
          />

          <Field
            name="designation"
            component={this.renderFormField}
            label="Designation"
            type="text"
            placeholder="Enter your designation in the institution"
            info="Please enter your designation"
          />
          <Field
            name="facebookusername"
            component={this.renderFormField}
            label="Your facebook username"
            type="text"
            placeholder="Please enter facebookusername"
            info="We wont share the info with anyone"
          />

          <Field
            name="email"
            component={this.renderFormField}
            label="Email"
            type="email"
            placeholder="Enter your email id"
            info="Please enter correct email for verification"
          />

          <Field
            name="phone"
            component={this.renderFormField}
            label="Phone"
            type="text"
            placeholder="Enter your phone number"
            info="Please enter phone number for further communications"
          />

          {this.renderAlert()}
          <div className="col-md-4" />

          <button
            action="submit"
            className="btn btn-primary"
            disabled={submitting}
          >
            {this.props.spin ? (
              <span>
                <span className="glyphicon glyphicon-refresh spinning" />Processing...
              </span>
            ) : (
              "Update Info"
            )}
          </button>
        </form>
      </div>
    );
  }
}

function validate(values) {
  const errors = {};
  if (!values.name) {
    errors.name = "Please Enter name";
  }
  if (!values.designation) {
    errors.designation = "Please Enter your designation";
  }
  if (!values.institute) {
    errors.institute = "Please Select an institution name";
  }

  if (!values.email) {
    errors.email = "Please Enter a valid email address";
  }

  if (!values.phone) {
    errors.phone = "Please Enter your phone number";
  }

  if (values.phone && isNaN(Number(values.phone))) {
    errors.phone = "Phone number should be a number";
  }

  return errors;
}

function mapStateToProps(state) {
  return {
    spin: state.user.processing,
    errorMessage: state.user.error
  };
}

export default reduxForm({
  form: "coordinatorupdate",
  validate: validate
})(connect(mapStateToProps, actions)(CoordinatorProfile));
