import React, { Component } from "react";
import { connect } from "react-redux";
import { Tab, Tabs } from "react-bootstrap";
import RegisterInstitute from "./register_institute";
import CoordinatorProfile from "./coordinator_profile";

import LeftPanel from "../common/leftpanel";
import RightPanel from "../common/rightpanel";
import QuizHead from "../common/quizhead";
import NonValidatedInstitute from "./non_validated_institute";
import GoodCopMoments from "./goodcopmoments";
import Coverart from "./coverart";
import Film from "./film";
import Report from "./report";
import CoordinatorSubmissions from "./coordinator_submissions";

import * as actions from "../../actions";
import * as useractions from "../../actions/user";
import * as coordinatoractions from "../../actions/coordinator/index";

class Dashboard extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    this.props.fetchUserData();
    this.props.getGoodcopmoments();
    return this.props.fetchInstitute();
  }

  handleSelect(key) {
    this.props.switchTabTo(key);
  }

  displayInstituteForm() {
    if (this.props.institute && this.props.institute.message) {
      return <NonValidatedInstitute />;
    } else if (
      this.props.institute &&
      this.props.institute.validatedyn === "N"
    ) {
      return <NonValidatedInstitute />;
    } else {
      return <RegisterInstitute />;
    }
  }

  render() {
    return (
      <div className="container">
        {/* <LeftPanel /> */}
        {/* <RightPanel> */}
        <QuizHead />
        <Tabs
          // activeKey={this.state.key}
          activeKey={this.props.tabselected}
          onSelect={this.handleSelect.bind(this)}
          id="dashboard-tab"
        >
          <Tab eventKey={1} title="Register Institute">
            {this.displayInstituteForm()}
          </Tab>
          <Tab
            eventKey={2}
            title="Coordinator Profile"
            disabled={
              this.props.institute
                ? this.props.institute.validatedyn === "N" ? true : false
                : true
            }
          >
            <CoordinatorProfile user={this.props.user} />
          </Tab>

          {/* <Tab
            eventKey={3}
            title="Student Entry Profile"
            disabled={
              this.props.institute
                ? this.props.institute.validatedyn === "N" ? true : false
                : true
            }
          >
            <div>Work in progress...</div>
          </Tab> */}

          <Tab
            eventKey={3}
            title="Good Cop Moment Submissions"
            disabled={
              this.props.institute
                ? this.props.institute.validatedyn === "N" ? true : false
                : true
            }
          >
            <div>
              {" "}
              <GoodCopMoments goodcopmoments={this.props.goodcopmoments} />{" "}
            </div>
          </Tab>

          <Tab
            eventKey={4}
            title="Cover Art Submissions"
            disabled={
              this.props.institute
                ? this.props.institute.validatedyn === "N" ? true : false
                : true
            }
          >
            <div>
              {" "}
              <Coverart coverart={this.props.coverart} />{" "}
            </div>
          </Tab>
          <Tab
            eventKey={5}
            title="Film Submissions"
            disabled={
              this.props.institute
                ? this.props.institute.validatedyn === "N" ? true : false
                : true
            }
          >
            <div>
              {" "}
              <Film film={this.props.film} />{" "}
            </div>
          </Tab>

          <Tab
            eventKey={6}
            title="Reports"
            disabled={
              this.props.institute
                ? this.props.institute.validatedyn === "N" ? true : false
                : true
            }
          >
            <Report />
          </Tab>
          <Tab
            eventKey={7}
            title="Report Submissions"
            disabled={
              this.props.institute
                ? this.props.institute.validatedyn === "N" ? true : false
                : true
            }
          >
            <CoordinatorSubmissions />
          </Tab>
        </Tabs>
        {/* </RightPanel> */}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    institute: state.institute.institute,
    user: state.user.user,
    goodcopmoments: state.coordinator.goodcopmoments,
    film: state.coordinator.film,
    coverart: state.coordinator.coverart,
    tabselected: state.coordinator.tab
  };
}

export default connect(mapStateToProps, {
  ...actions,
  ...useractions,
  ...coordinatoractions
})(Dashboard);
