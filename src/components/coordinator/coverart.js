import React, { Component } from "react";
import { connect } from "react-redux";

import * as coordinatoractions from "../../actions/coordinator/index";

import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import "../../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import { formattedDate } from "../../utils/date";
import DownloadButton from "../student/downloadbutton";
import VerifyButton from "./verifybutton";
import "../css/goodcopmoment.css";

function displayDownloadButton(cell, row) {
  return <DownloadButton dropboxid={row.dropboxid} />;
}

function displayVerifyButton(cell, row) {
  return (
    <VerifyButton
      section="coverart"
      data={{
        index: row.id - 1,
        id: row._id,
        selected: row.selected,
        userid: row.userid,
        name: row.name,
        email: row.email,
        phone: row.phone
      }}
    />
  );
}

class Coverart extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <h3>Cover Art Submission </h3>
        <div>
          {this.props.coverart ? (
            <BootstrapTable data={this.props.coverart} search={true} pagination>
              <TableHeaderColumn
                dataField="id"
                width="40"
                isKey
                searchable={false}
              >
                ID
              </TableHeaderColumn>
              <TableHeaderColumn dataField="name" width="100" dataSort={true}>
                Name
              </TableHeaderColumn>
              <TableHeaderColumn
                dataField="email"
                width="150"
                dataSort={true}
                tdStyle={{ whiteSpace: "normal" }}
              >
                Email
              </TableHeaderColumn>
              <TableHeaderColumn dataField="phone" width="100" dataSort={true}>
                Phone
              </TableHeaderColumn>
              <TableHeaderColumn
                dataField="submissiondate"
                width="80"
                dataSort={true}
                dataFormat={formattedDate}
              >
                Submission Date
              </TableHeaderColumn>

              <TableHeaderColumn
                dataField="description"
                width="200"
                dataSort={true}
                tdStyle={{ whiteSpace: "normal" }}
              >
                Relation Description
              </TableHeaderColumn>

              <TableHeaderColumn
                dataField="dropboxid"
                width="100"
                dataFormat={displayDownloadButton}
              >
                Pdf
              </TableHeaderColumn>
              <TableHeaderColumn
                dataField="selected"
                dataFormat={displayVerifyButton}
                export={false}
                width="150"
              >
                Verify
              </TableHeaderColumn>
            </BootstrapTable>
          ) : (
            <div> Loading </div>
          )}
        </div>
      </div>
    );
  }
}

export default connect(null, coordinatoractions)(Coverart);
