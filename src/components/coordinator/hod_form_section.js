import React, { Component } from "react";
import { Field } from "redux-form";

class HodFormSection extends Component {
  renderFormField(field) {
    const { meta: { touched, error } } = field;

    const classNameforField = `form-group ${
      touched && error ? "has-error" : ""
    }`;
    return (
      <div className={classNameforField}>
        <label className="control-label col-md-4" htmlFor={field.name}>
          {field.label}
          {field.info ? (
            <span>
              <br />
              <small id="emailHelp" class="form-text text-muted">
                {field.info}
              </small>
            </span>
          ) : (
            <br />
          )}
        </label>
        {field.label === "Your facebook username" ? (
          <div className="col-md-2" style={{ "padding-top": 5 }}>
            www.facebook.com/
          </div>
        ) : (
          <div>{field.name}</div>
        )}
        <div
          className={
            field.label === "Your facebook username" ? "col-md-6" : "col-md-8"
          }
        >
          <input
            id={field.name}
            type={field.type}
            {...field.input}
            placeholder={field.placeholder}
            className="form-control"
          />
          {touched && error && <div className="error">{error}</div>}
        </div>
      </div>
    );
  }

  render() {
    return (
      <div>
        <Field
          name="name"
          component={this.renderFormField}
          label="Head of Institution"
          type="text"
          placeholder="Enter the full name"
          info=""
        />

        <Field
          name="designation"
          component={this.renderFormField}
          label="Head of Institution Designation"
          type="text"
          placeholder="Enter the full name"
          info=""
        />
        <Field
          name="phone"
          component={this.renderFormField}
          label="Head of Institution Phone Number"
          type="text"
          placeholder="Enter phone number"
          info="For communication"
        />

        <Field
          name="facebookusername"
          component={this.renderFormField}
          label="Head of institution facebook username"
          type="text"
          placeholder="Enter the full name"
          info="We wont share the info with anyone"
        />

        <Field
          name="email"
          component={this.renderFormField}
          label="Head of Institution"
          type="email"
          placeholder="Enter email"
          info="For all official communication / Updates"
        />
      </div>
    );
  }
}

// function validate(values) {
//     const errors = {};
//         if (!values.designation) {
//       errors.designation = "Please Enter your designation";
//     }
//     if (!values.institute) {
//       errors.institute = "Please Enter institution name";
//     }

//     if (!values.email) {
//       errors.email = "Please Enter a valid email address";
//     }

//     if (!values.name) {
//       errors.name = "Please Enter name of the head of the institution";
//     }
//     return errors;
// }
export default HodFormSection;
