import React, { Component } from "react";
import { connect } from "react-redux";

import * as coordinatoractions from "../../actions/coordinator/index";

import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import "../../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import { formattedDate } from "../../utils/date";
import DownloadButton from "../student/downloadbutton";
import VerifyButton from "./verifybutton";
import "../css/goodcopmoment.css";

function displayDownloadButton(cell, row) {
  return <DownloadButton dropboxid={row.dropboxid} />;
}

function displayVerifyButton(cell, row) {
  return (
    <VerifyButton
      section="goodcopmoment"
      data={{
        index: row.id - 1,
        id: row._id,
        selected: row.selected,
        userid: row.userid,
        name: row.name,
        email: row.email,
        phone: row.phone
      }}
    />
  );
}

class Goodcopmoments extends Component {
  constructor(props) {
    super(props);
  }

  // componentWillMount() {
  //   return this.props.getGoodcopmoments();
  // }

  render() {
    return (
      <div>
        <h3>Good Cop Moment Submissions </h3>
        <div>
          {this.props.goodcopmoments ? (
            <BootstrapTable
              data={this.props.goodcopmoments}
              search={true}
              pagination
            >
              <TableHeaderColumn
                dataField="id"
                width="40"
                isKey
                searchable={false}
              >
                ID
              </TableHeaderColumn>
              <TableHeaderColumn dataField="name" width="100" dataSort={true}>
                Name
              </TableHeaderColumn>
              <TableHeaderColumn
                dataField="email"
                width="120"
                dataSort={true}
                tdStyle={{ whiteSpace: "normal" }}
              >
                Email
              </TableHeaderColumn>
              <TableHeaderColumn dataField="phone" width="100" dataSort={true}>
                Phone
              </TableHeaderColumn>
              <TableHeaderColumn
                dataField="submissiondate"
                width="80"
                dataSort={true}
                dataFormat={formattedDate}
              >
                Submission Date
              </TableHeaderColumn>
              <TableHeaderColumn
                dataField="relatedtouser"
                width="70"
                dataSort={true}
              >
                Related To User
              </TableHeaderColumn>
              <TableHeaderColumn
                dataField="relationdescription"
                width="200"
                dataSort={true}
                tdStyle={{ whiteSpace: "normal" }}
              >
                Relation Description
              </TableHeaderColumn>

              <TableHeaderColumn
                dataField="dropboxid"
                width="100"
                dataFormat={displayDownloadButton}
              >
                Pdf
              </TableHeaderColumn>
              <TableHeaderColumn
                dataField="selected"
                dataFormat={displayVerifyButton}
                export={false}
                width="150"
              >
                Verify
              </TableHeaderColumn>
            </BootstrapTable>
          ) : (
            <div> Loading </div>
          )}
        </div>
      </div>
    );
  }
}

// function mapStateToProps(state) {
//   return {
//     goodcopmoments: state.coordinator.goodcopmoments
//   };
// }

export default connect(null, coordinatoractions)(Goodcopmoments);
