import React, { Component } from "react";
import { Field, FormSection, reduxForm } from "redux-form";
import { Link } from "react-router-dom";
import * as actions from "../../actions";
import { connect } from "react-redux";
import CheckBoxesForm from "./checkboxesform";
import VirtualizedSelect from "react-virtualized-select";
import "../admin/static/validatebutton.css";
import "react-select/dist/react-select.css";
import "react-virtualized/styles.css";
import "react-virtualized-select/styles.css";
// import HodFormSection from "./hod_form_section";

class RegisterInstitute extends Component {
  constructor(props) {
    super(props);
    this.renderAutocompleteField = this.renderAutocompleteField.bind(this);
  }

  componentWillMount() {
    if (this.props.institutelist.length === 0) {
      this.props.getInstituteList();
    }

    /*
     * The initialization of form that happens when the component is going to mount.
     * At this time the auth will have the institute data. So we initialize the form with that.
     * 
     */
    if (!this.props.institute) {
      this.props.initialize({
        name: { name: this.props.auth.institute }
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    /* This is quite a *dangerous* subroutine,
     * Make sure that if you are doing initializations here (which are sure to be avoided),
     * you do it only when receiving new props and make sure that those don't interrupt the 
     * normal user interface lifecycle.
     */

    if (
      this.props.institutelist &&
      this.props.institute !== nextProps.institute
    ) {
      const institutioninfo = nextProps.institute;
      this.props.initialize({
        // Why is this of this form. The virtualized select,
        // updates the form in the below format when changes happen.
        // So lets not mess with it during initialization.
        name: { name: institutioninfo.collegeOrInstitute },
        type: institutioninfo.boardOrUniversity,
        phone: institutioninfo.phone,
        address: institutioninfo.address,
        district: institutioninfo.district,
        pincode: institutioninfo.pincode,
        facebookusername: institutioninfo.facebookusername,
        email: institutioninfo.institutionemail,
        hodname: institutioninfo.hod.name,
        hoddesignation: institutioninfo.hod.designation,
        hodphone: institutioninfo.hod.phone,
        hodfacebookusername: institutioninfo.hod.facebookusername,
        hodemail: institutioninfo.hod.email,
        registeredevents: institutioninfo.registeredevents
      });
    }
  }

  renderSelectField(field) {
    const { meta: { touched, error } } = field;

    // const classNameforField = `form-group ${
    //   touched && error ? "has-error" : ""
    // }`;
    return (
      <div className="form-group">
        <label className="control-label col-md-4" htmlFor={field.name}>
          {field.label}
          <br />
          <small className="form-text text-muted">{field.info}</small>
        </label>
        <div className="col-md-3">
          <select className="form-control" id="class" {...field.input}>
            <option selected="selected" />
            <option>CBSE School</option>
            <option>ICSC School</option>
            <option>Matriculation School</option>
            <option>State Board</option>
            <option>Engineering College</option>
            <option>Art &amp; Science College</option>
            <option>Others (Nursing, Pharmachy college etc)</option>
            <option>Deemed University</option>
            <option>Poly technique</option>
          </select>
          {touched && error && <div className="error">{error}</div>}
        </div>
      </div>
    );
  }

  renderFormField(field) {
    const { meta: { touched, error } } = field;

    const classNameforField = `form-group ${
      touched && error ? "has-error" : ""
    }`;
    return (
      <div className={classNameforField}>
        <label className="control-label col-md-4" htmlFor={field.name}>
          {field.label}
          {field.info ? (
            <span>
              <br />
              <small id="emailHelp" className="form-text text-muted">
                {field.info}
              </small>
            </span>
          ) : (
            <br />
          )}
        </label>
        {field.label.match(/facebook/g) ? (
          <div className="col-md-2" style={{ paddingTop: 5 }}>
            www.facebook.com/
          </div>
        ) : (
          <div>{field.name}</div>
        )}
        <div
          className={field.label.match(/facebook/g) ? "col-md-6" : "col-md-8"}
        >
          {field.type === "textarea" ? (
            <textarea
              id={field.name}
              type={field.type}
              {...field.input}
              placeholder={field.placeholder}
              className="form-control"
            />
          ) : (
            <input
              id={field.name}
              type={field.type}
              {...field.input}
              placeholder={field.placeholder}
              className="form-control"
            />
          )}

          {touched && error && <div className="error">{error}</div>}
        </div>
      </div>
    );
  }

  renderAutocompleteField(field) {
    const { meta: { touched, error } } = field;
    const classNameforField = `form-group ${
      touched && error ? "has-error" : ""
    }`;
    return (
      <div className={classNameforField}>
        <label className="control-label col-md-4" htmlFor={field.name}>
          {field.label}
          {field.info ? (
            <span>
              <br />
              <small id="emailHelp" className="form-text text-muted">
                {field.info}
              </small>
            </span>
          ) : (
            <br />
          )}
        </label>
        <div className="col-md-8">
          <VirtualizedSelect
            options={this.props.institutelist}
            {...field.input}
            placeholder={field.placeholder}
            onBlur={() => {
              field.input.onBlur(field.input.value);
            }}
            autoFocus
            labelKey="name"
            valueKey="name"
          />
          {touched && error && <div className="error">{error}</div>}
        </div>
      </div>
    );
  }

  handleFormSubmit(formProps) {
    this.props.registerInstitute(formProps);
  }

  handleFormUpdate(formProps) {
    formProps._id = this.props.institute._id;
    this.props.updateInstitute(formProps);
  }

  renderAlert() {
    if (this.props.errorMessage) {
      return (
        <div className="alert alert-danger">
          <strong> Oops!</strong> {this.props.errorMessage}
        </div>
      );
    }
  }

  render() {
    // console.log(this.props.institutelist);
    // console.log(this.props.institute);
    const { handleSubmit, pristine, submitting } = this.props;

    return (
      <div>
        <span className="text-center">
          <h1>Institute Registration Form</h1>
        </span>
        <form
          className="form-horizontal"
          onSubmit={
            handleSubmit(
              !this.props.institute
                ? this.handleFormSubmit.bind(this)
                : this.handleFormUpdate.bind(this)
            )
            // this.props.institute
            //   ? handleSubmit(this.handleFormSubmit.bind(this))
            //   : handleSubmit(this.handleFormUpdate.bind(this))
          }
        >
          {/* <Field
            name="name"
            component={this.renderFormField}
            label="Institution/School Name"
            type="text"
            placeholder="Enter School Name"
            info="Select Institution/School Name"
          /> */}

          <Field
            name="name"
            component={this.renderAutocompleteField}
            label="Institution/School"
            type="autocomplete"
            placeholder="Select your affiliated institution name"
            info="If you had added a custom institute name, search for it and select"
          />

          <Field
            name="type"
            component={this.renderSelectField}
            label="Institution/School Board"
            type="select"
            info="Select Institution/ School Board"
          />

          <Field
            name="phone"
            component={this.renderFormField}
            label="Institution / School Phone Number"
            type="text"
            placeholder="Enter Institution / School Phone Number"
            info="Please enter correct phone number for validation"
          />

          <Field
            name="address"
            component={this.renderFormField}
            label="Institution/School Address"
            type="textarea"
            placeholder="Door No., Street Name, City/Town/Village"
            info="For all official communication/Updates"
          />

          <Field
            name="district"
            component={this.renderFormField}
            label="District"
            type="text"
            placeholder="Enter District"
            info=""
          />

          <Field
            name="pincode"
            component={this.renderFormField}
            label="Pincode"
            type="text"
            placeholder="Enter Pincode"
            info=""
          />

          <Field
            name="facebookusername"
            component={this.renderFormField}
            label="Institution / School facebook username"
            type="text"
            placeholder="username"
            info="We wont share the info with anyone"
          />

          <Field
            name="email"
            component={this.renderFormField}
            label="Email Address"
            type="email"
            placeholder="Enter institution email id"
            info="For all official communication / Updates"
          />

          <hr />
          {/* 
          <FormSection name="hod">
            <HodFormSection />
          </FormSection> */}

          <Field
            name="hodname"
            component={this.renderFormField}
            label="Head of Institution"
            type="text"
            placeholder="Enter the full name"
            info=""
          />

          <Field
            name="hoddesignation"
            component={this.renderFormField}
            label="Head of Institution Designation"
            type="text"
            placeholder="Enter Designation"
            info=""
          />
          <Field
            name="hodphone"
            component={this.renderFormField}
            label="Head of Institution Phone Number"
            type="text"
            placeholder="Enter phone number"
            info="For communication"
          />

          <Field
            name="hodfacebookusername"
            component={this.renderFormField}
            label="Head of institution facebook username"
            type="text"
            placeholder="Enter Facebookusername"
            info="We wont share the info with anyone"
          />

          <Field
            name="hodemail"
            component={this.renderFormField}
            label="Head of Institution"
            type="email"
            placeholder="Enter email"
            info="For all official communication / Updates"
          />

          <FormSection name="registeredevents">
            <CheckBoxesForm />
          </FormSection>
          {this.renderAlert()}
          <div className="col-md-4" />
          {this.props.institute ? (
            <button
              action="submit"
              className="btn btn-primary"
              disabled={submitting}
            >
              {this.props.spin ? (
                <span>
                  <span className="glyphicon glyphicon-refresh spinning" />Processing...
                </span>
              ) : (
                "Update Institute"
              )}
            </button>
          ) : (
            <button
              action="submit"
              className="btn btn-primary"
              disabled={submitting}
            >
              {this.props.spin ? (
                <span>
                  <span className="glyphicon glyphicon-refresh spinning" />Processing...
                </span>
              ) : (
                "Register Institute"
              )}
            </button>
          )}
        </form>
      </div>
    );
  }
}

function validate(values) {
  // const { hod } = values;
  const errors = {};
  // errors.hod = {};
  if (!values.name) {
    errors.name = "Please Enter name";
  }

  if (!values.type) {
    errors.type = "Please select board/university info";
  }

  if (!values.phone) {
    errors.phone = "Please Enter phone number of the institution";
  }

  if (values.phone && isNaN(Number(values.phone))) {
    errors.phone = "Phone number should be a number";
  }

  if (!values.address) {
    errors.address = "Please Enter address of the institution";
  }

  if (!values.district) {
    errors.district = "Please enter the district";
  }

  if (!values.pincode) {
    errors.pincode = "Please enter pincode";
  }
  if (values.pincode && isNaN(Number(values.pincode))) {
    errors.pincode = "Pincode should be a number";
  }

  if (!values.email) {
    errors.email = "Please enter email";
  }

  if (!values.hoddesignation) {
    errors.hoddesignation = "Please Enter your designation";
  }
  if (!values.hodphone) {
    errors.hodphone = "Please Enter phone number";
  }

  if (values.hodphone && isNaN(Number(values.hodphone))) {
    errors.hodphone = "Phone number must be a number";
  }

  if (!values.hodemail) {
    errors.hodemail = "Please Enter a valid email address";
  }

  if (!values.hodname) {
    errors.hodname = "Please Enter name of the head of the institution";
  }

  return errors;
}

function mapStateToProps(state) {
  return {
    institutelist: state.institute.institutelist,
    errorMessage: state.institute.error,
    institute: state.institute.institute,
    auth: state.auth,
    spin: state.institute.processing
  };
}

export default reduxForm({
  form: "institutionform",
  validate: validate
})(connect(mapStateToProps, actions)(RegisterInstitute));
