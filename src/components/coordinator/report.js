import React, { Component } from "react";
import { Field, FormSection, reduxForm } from "redux-form";
import {
  renderFormField,
  renderYNField,
  renderFileInput
} from "../student/utils/util";

import { connect } from "react-redux";

import * as reportactions from "../../actions/coordinator/report";

import GuidelinesReport from "./utils/guidelines_report";

class ReportForm extends Component {
  constructor(props) {
    super(props);
  }

  handleFormSubmit(formProps) {
    this.props.uploadReportdoc(formProps);
  }

  renderAlert() {
    if (this.props.report && !this.props.report.error) {
      return <div />;
    } else {
      return (
        <div className="alert alert-danger">
          <strong> Oops! </strong> {this.props.report.error}
        </div>
      );
    }
  }

  render() {
    const { handleSubmit, pristine, submitting } = this.props;

    return (
      <div>
        <br />
        <GuidelinesReport />
        <form
          className="form-horizontal"
          onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}
        >
          <Field
            name="description"
            component={renderFormField}
            label="Report Title"
            type="textarea"
            info="Please give info about the report"
          />

          <Field
            name="file"
            type="file"
            label="Upload Report"
            component={renderFileInput}
            info="Please make sure that it is the pdf file version of the Report template and is less than 10Mb"
          />
          {this.renderAlert()}
          <div className="col-md-4" />
          <button
            className="btn btn-primary"
            action="submit"
            disabled={submitting}
          >
            {this.props.spin ? (
              <span>
                <span className="glyphicon glyphicon-refresh spinning" />Processing...
              </span>
            ) : (
              "Submit Report"
            )}
          </button>
        </form>
      </div>
    );
  }
}

function validate(values) {
  const errors = {};

  if (!values.file) {
    errors.file = "Please select a pdf file";
  }

  return errors;
}

function mapStateToProps(state) {
  return {
    report: state.report,
    spin: state.report.spin
  };
}

export default reduxForm({
  form: "reportform",
  validate: validate
})(connect(mapStateToProps, { ...reportactions })(ReportForm));
