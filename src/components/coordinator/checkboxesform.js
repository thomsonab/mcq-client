import React, { Component } from "react";
import { Field } from "redux-form";

class CheckBoxesForm extends Component {
  render() {
    return (
      <div>
        <div className="col-md-4" />
        <div>
          <Field
            name="gcm"
            id="gcm"
            component="input"
            type="checkbox"
            checked={true}
            disabled={true}
          />
          <label htmlFor="gcm">
            Encourage students to share good experiences they have with our
            police officer
          </label>
        </div>

        <div className="col-md-4" />
        <div>
          <Field
            name="appreciation"
            id="appreciation"
            component="input"
            type="checkbox"
            checked={true}
            disabled={true}
          />
          <label htmlFor="appreciation">
            Encourage students to show appreciation for our police officers in
            social media
          </label>
        </div>

        <div className="col-md-4" />
        <div>
          <Field
            name="quiz"
            id="quiz"
            component="input"
            type="checkbox"
            checked={true}
            disabled={true}
          />
          <label htmlFor="quiz">
            Encourage Students to take the on line quiz on Greater Chennai
            Police
          </label>
        </div>

        <div className="col-md-4" />
        <div>
          <Field
            name="pledge"
            id="pledge"
            component="input"
            type="checkbox"
            checked={true}
            disabled={true}
          />
          <label htmlFor="pledge">
            Organise for students to take the OUR COPS OUR HEROES pledge
          </label>
        </div>

        <div className="col-md-4" />
        <div>
          <Field
            name="promote"
            id="promote"
            component="input"
            type="checkbox"
            checked={true}
            disabled={true}
          />
          <label htmlFor="promote">
            Display the OCOH Banner & Promo materials
          </label>
        </div>

        <div className="col-md-4" />
        <div>
          <Field
            name="filmmaking"
            id="filmmaking"
            component="input"
            type="checkbox"
            disabled={false}
          />
          <label htmlFor="filmmaking">
            Actively engage Student participation in OCOH themed film making
          </label>
        </div>

        <div className="col-md-4" />
        <div>
          <Field
            name="coverart"
            id="coverart"
            component="input"
            type="checkbox"
            disabled={false}
          />
          <label htmlFor="coverart">
            Actively engage Student participation in designing coverart for Book
            of Sagas
          </label>
        </div>

        <div className="col-md-4" />
        <div>
          <Field
            name="copexperience"
            id="copexperience"
            component="input"
            type="checkbox"
            disabled={false}
          />
          <label htmlFor="copexperience">
            Actively engage Student participation in COPEXPERRIENCE - Police
            Station Visit
          </label>
        </div>

        <div className="col-md-4" />
        <div>
          <Field
            name="rsp"
            id="rsp"
            component="input"
            type="checkbox"
            disabled={false}
          />
          <label htmlFor="rsp">
            Actively engage Student participation in Student Safety Cadet for
            traffic volunteering
          </label>
        </div>

        <div className="col-md-4" />
        <div>
          <Field
            name="copspeech"
            id="copspeech"
            component="input"
            type="checkbox"
            disabled={false}
          />
          <label htmlFor="copspeech">
            Invite a Police officer to your institution to speak on Self safety
            and security
          </label>
        </div>
      </div>
    );
  }
}

export default CheckBoxesForm;
