import React from "react";

export default function(props) {
  return (
    <div className="container">
      <div className="row">
        <div className="col-md-10">
          <h2>Thanks for registering your institute. </h2>
          <p>
            To access institute dashboard please verify your credentials by
            getting the approval signature and seal of your Head of Institution.<br />
            <a
              href="http://www.edmat.org/ocoh/assets/ocoh_verification_letter.doc"
              className="btn btn-danger"
            >
              {" "}
              Click Here{" "}
            </a>{" "}
            to download the verification letter. <br /> Please email the
            verified letter scan to ocoh@edmat.org.
          </p>
        </div>
      </div>
    </div>
  );
}
