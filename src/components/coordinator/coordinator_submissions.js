import React, { Component } from "react";
import { connect } from "react-redux";

import * as coordinatoractions from "../../actions/coordinator/report";

import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import "../../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import { formattedDate } from "../../utils/date";
import DownloadButton from "../student/downloadbutton";

//getCoordinatorReportSubmissions

function displayDownloadButton(cell, row) {
  return <DownloadButton dropboxid={row.dropboxid} />;
}

class CoordinatorSubmissions extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    return this.props.getCoordinatorReportSubmissions();
  }

  render() {
    return (
      <div>
        <h3>Submitted Reports </h3>
        <div>
          {this.props.coordinatorsubmissions ? (
            <BootstrapTable
              data={this.props.coordinatorsubmissions.report}
              search={true}
              pagination
            >
              <TableHeaderColumn
                dataField="id"
                width="40"
                isKey
                searchable={false}
              >
                ID
              </TableHeaderColumn>
              <TableHeaderColumn
                dataField="submissiondate"
                width="100"
                dataSort={true}
                dataFormat={formattedDate}
              >
                Submission Date
              </TableHeaderColumn>

              <TableHeaderColumn
                dataField="description"
                width="200"
                dataSort={true}
                tdStyle={{ whiteSpace: "normal" }}
              >
                Report Title
              </TableHeaderColumn>

              <TableHeaderColumn
                dataField="dropboxid"
                width="100"
                dataFormat={displayDownloadButton}
              >
                Pdf
              </TableHeaderColumn>
            </BootstrapTable>
          ) : (
            <div> Loading </div>
          )}
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    coordinatorsubmissions: state.report.coordinatorsubmissions
  };
}

export default connect(mapStateToProps, coordinatoractions)(
  CoordinatorSubmissions
);
