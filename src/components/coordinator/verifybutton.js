import React, { Component } from "react";
import { connect } from "react-redux";
import { Button } from "react-bootstrap";
import * as actions from "../../actions/coordinator";

import "../admin/static/validatebutton.css";

class VerifyButton extends Component {
  constructor(props) {
    super(props);
    this.selected = this.selected.bind(this);
    this.rejected = this.rejected.bind(this);
    this.state = {
      spin_selected: false,
      spin_rejected: false
    };
  }

  selected() {
    // We will have to remove it as soon as people get used to it.
    this.setState({
      spin_selected: true
    });
    if (this.props.section === "goodcopmoment") {
      this.props.verifyGoodcopmoment({
        index: this.props.data.index,
        id: this.props.data.id,
        selected: "Y",
        userid: this.props.data.userid,
        name: this.props.data.name,
        email: this.props.data.email,
        phone: this.props.data.phone
      });
    }

    if (this.props.section === "coverart") {
      this.props.verifyCoverart({
        index: this.props.data.index,
        id: this.props.data.id,
        selected: "Y",
        userid: this.props.data.userid,
        name: this.props.data.name,
        email: this.props.data.email,
        phone: this.props.data.phone
      });
    }

    if (this.props.section === "film") {
      this.props.verifyFilm({
        index: this.props.data.index,
        id: this.props.data.id,
        selected: "Y",
        userid: this.props.data.userid,
        name: this.props.data.name,
        email: this.props.data.email,
        phone: this.props.data.phone
      });
    }
  }

  rejected() {
    this.setState({
      spin_rejected: true
    });
    if (this.props.section === "goodcopmoment") {
      this.props.verifyGoodcopmoment({
        index: this.props.data.index,
        id: this.props.data.id,
        selected: "N",
        userid: this.props.data.userid,
        name: this.props.data.name,
        email: this.props.data.email,
        phone: this.props.data.phone
      });
    }

    if (this.props.section === "coverart") {
      this.props.verifyCoverart({
        index: this.props.data.index,
        id: this.props.data.id,
        selected: "N",
        userid: this.props.data.userid,
        name: this.props.data.name,
        email: this.props.data.email,
        phone: this.props.data.phone
      });
    }

    if (this.props.section === "film") {
      this.props.verifyFilm({
        index: this.props.data.index,
        id: this.props.data.id,
        selected: "N",
        userid: this.props.data.userid,
        name: this.props.data.name,
        email: this.props.data.email,
        phone: this.props.data.phone
      });
    }
  }

  render() {
    if (this.props.data.selected === "Y") {
      return <div>Selected</div>;
    } else if (this.props.data.selected === "N") {
      return <div> Rejected </div>;
    } else {
      return (
        <div>
          <Button bsStyle="primary" bsSize="small" onClick={this.selected}>
            {this.state.spin_selected ? (
              <span>
                <span className="glyphicon glyphicon-refresh spinning" />Processing...
              </span>
            ) : (
              "Approve"
            )}
          </Button>{" "}
          <Button bsStyle="primary" bsSize="small" onClick={this.rejected}>
            {this.state.spin_rejected ? (
              <span>
                <span className="glyphicon glyphicon-refresh spinning" />Processing...
              </span>
            ) : (
              "Disapprove"
            )}
          </Button>
        </div>
      );
    }
  }
}

export default connect(null, actions)(VerifyButton);
