import React from "react";

export default function(props) {
  return (
    <div>
      <div className="alert alert-success" role="alert">
        <b>Important!</b>&ensp;&ensp; Use this template to write your Report
        &ensp;<a
          className="btn btn-success"
          target="_blank"
          href="https://aggumedia-my.sharepoint.com/:w:/g/personal/dev_aggu_media/EW_rBVosiSlKlvZz4uvOFvAB4cTTBU00Q7ymiSuM4VlwAw?e=lrc3iI"
        >
          Download - Report Template
        </a>
      </div>{" "}
    </div>
  );
}
