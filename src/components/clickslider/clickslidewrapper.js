import React, { Component } from "react";
import ListItemLeft from "./listitemleft";
import ListWrapper from "./listwrapper";
import ListItemRight from "./listitemright";
import ListWrapperRight from "./listwrapperright";
import "./clickslider.css";

import appscreen1 from "../../static/images/how/app-screen-1.png";
import appscreen2 from "../../static/images/how/app-screen-2.png";
import appscreen3 from "../../static/images/how/app-screen-3.png";
import appscreen4 from "../../static/images/how/app-screen-4.png";
import appscreen5 from "../../static/images/how/app-screen-5.png";

class ClickSlideWrapper extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listarray: ClickSlideWrapper.InitializeState()
    };
    this.onListItemClick = this.onListItemClick.bind(this);
  }

  onListItemClick(itemnumber) {
    this.setState(prevstate => {
      let newstate = prevstate.listarray.slice();
      newstate = newstate.map(function(val, index) {
        if (index === itemnumber) {
          val.selected = true;
        } else {
          val.selected = false;
        }

        return val;
      });

      return {
        listarray: newstate
      };
    });
  }

  generateList() {
    const list = this.state.listarray.map((li, index) => {
      return (
        <ListItemLeft
          key={index + 1}
          selected={li.selected}
          onClick={this.onListItemClick}
          itemnumber={index}
        >
          <h3>{li.content}</h3>
          <p>{li.body}</p>
        </ListItemLeft>
      );
    });
    return list;
  }

  generateImages() {
    const list = this.state.listarray.map((li, index) => {
      return (
        <ListItemRight
          key={index + 1}
          selected={li.selected}
          itemnumber={index}
        >
          <img src={li.description} alt="" />
        </ListItemRight>
      );
    });
    return list;
  }
  render() {
    return (
      <div className="container-container-wrapper">
        <ListWrapper>{this.generateList()}</ListWrapper>
        <ListWrapperRight>{this.generateImages()}</ListWrapperRight>
      </div>
    );
  }

  static InitializeState() {
    return [
      {
        content: "Share your Good Cop Moment",
        body:
          "Students write us their moment of good experience / inspiration / interaction, with our police / their heroes, in their life or in their community.",
        description: appscreen1,
        selected: true
      },
      {
        content: "We Publish & Share on Social Media",
        body:
          "We publish their approved moments on social media to be shared across all platforms.",
        description: appscreen2,
        selected: false
      },
      {
        content: "Activities",
        body:
          "They can also participate in, Individual Activities & Institutional Activities",
        description: appscreen3,
        selected: false
      },
      {
        content: "Book of Sagas",
        body:
          "Collection of all OCOH Good Cop Moments will be published as ‘Book of Sagas’ in the OCOH grand finale",
        description: appscreen4,
        selected: false
      },
      {
        content: "OCOH Forum - Final",
        body:
          "Selected few will have face-to-face session with their heroes and cherish their moment in the grand finale of OCOH forum",
        description: appscreen5,
        selected: false
      }
    ];
  }
}

export default ClickSlideWrapper;
