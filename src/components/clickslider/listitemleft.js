import React, { Component } from "react";

class ListItem extends Component {
  constructor(props) {
    super(props);
    this.onDivClick = this.onDivClick.bind(this);
  }

  onDivClick(e) {
    this.props.onClick(this.props.itemnumber);
  }
  render() {
    return (
      <div
        className={
          this.props.selected
            ? "container-induvidual-click"
            : "container-induvidual"
        }
        onClick={this.onDivClick}
      >
        <div className="circle">
          <div className="circlecontent">{this.props.itemnumber + 1}</div>
        </div>
        <div className="component">{this.props.children}</div>
      </div>
    );
  }
}

export default ListItem;
