import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { LinkContainer } from "react-router-bootstrap";
import PropTypes from "prop-types";
import * as actions from "../actions";

//Bootstrap components
import { Nav, Navbar, NavItem, NavDropdown, MenuItem } from "react-bootstrap";

import "../static/css/header.css";
import logo from "../static/images/logo.png";

class Header extends Component {
  constructor(props) {
    super(props);
  }

  renderLinks() {
    if (this.props.authenticated && this.props.type === "student") {
      return (
        <Nav pullRight>
          <NavItem eventKey={3}>
            <small>Hi, {this.props.name}</small>
            {"  "}
          </NavItem>
          <LinkContainer to="/help">
            <NavItem eventKey={4}>Help</NavItem>
          </LinkContainer>
          <LinkContainer to="/studentdashboard">
            <NavItem eventKey={1}>Dashboard</NavItem>
          </LinkContainer>
          <LinkContainer to="/signout">
            <NavItem eventKey={2}>SignOut</NavItem>
          </LinkContainer>
        </Nav>
      );
    } else if (this.props.authenticated && this.props.type === "coordinator") {
      return (
        <Nav pullRight>
          <NavItem eventKey={3}>
            <small>Hi, {this.props.name}</small>
            {"  "}
          </NavItem>
          <LinkContainer to="/help">
            <NavItem eventKey={4}>Help</NavItem>
          </LinkContainer>
          <LinkContainer to="/dashboard">
            <NavItem eventKey={1}>Dashboard</NavItem>
          </LinkContainer>
          <LinkContainer to="/signout">
            <NavItem eventKey={2}>SignOut</NavItem>
          </LinkContainer>
        </Nav>
      );
    } else if (
      this.props.authenticated &&
      this.props.type === "committeeadmin"
    ) {
      return (
        <Nav pullRight>
          <NavItem eventKey={3}>
            <small>Hi, {this.props.name}</small>
            {"  "}
          </NavItem>
          <LinkContainer to="/committeeadmindashboard">
            <NavItem eventKey={1}>Dashboard</NavItem>
          </LinkContainer>
          <LinkContainer to="/signout">
            <NavItem eventKey={2}>SignOut</NavItem>
          </LinkContainer>
        </Nav>
      );
    } else if (this.props.authenticated && this.props.type === "policeadmin") {
      return (
        <Nav pullRight>
          <NavItem eventKey={3}>
            <small>Hi, {this.props.name}</small>
            {"  "}
          </NavItem>
          <LinkContainer to="/policeadmindashboard">
            <NavItem eventKey={1}>Dashboard</NavItem>
          </LinkContainer>
          <LinkContainer to="/signout">
            <NavItem eventKey={2}>SignOut</NavItem>
          </LinkContainer>
        </Nav>
      );
    } else if (this.props.authenticated && this.props.type === "admin") {
      return (
        <Nav pullRight>
          <NavItem eventKey={3}>
            <small>Hi, {this.props.name}</small>
            {"  "}
          </NavItem>
          <LinkContainer to="/help">
            <NavItem eventKey={4}>Help</NavItem>
          </LinkContainer>
          <LinkContainer to="/admindashboard">
            <NavItem eventKey={1}>Dashboard</NavItem>
          </LinkContainer>
          <LinkContainer to="/signout">
            <NavItem eventKey={2}>SignOut</NavItem>
          </LinkContainer>
        </Nav>
      );
    } else {
      return (
        <Nav pullRight>
          <LinkContainer to="/help">
            <NavItem eventKey={4}>Help</NavItem>
          </LinkContainer>
          <LinkContainer to="/signin">
            <NavItem eventKey={1}>SignIn</NavItem>
          </LinkContainer>
          <LinkContainer to="/signup">
            <NavItem eventKey={2} className="displayred">
              Register Now
            </NavItem>
          </LinkContainer>
        </Nav>
      );
    }
  }
  // renderLinks() {
  //   if (this.props.authenticated) {
  //     return (
  //       <li className="nav-items">
  //         <Link to="/signout" className="nav-link">
  //           Sign out
  //         </Link>
  //       </li>
  //     );
  //   } else {
  //     return [
  //       <li className="nav-items" key="sign-in">
  //         <Link to="/signin" className="nav-link">
  //           Sign in
  //         </Link>
  //       </li>,
  //       <li className="nav-items" key="sign-up">
  //         <Link to="/signup" className="nav-link">
  //           Sign up
  //         </Link>
  //       </li>
  //     ];
  //   }
  // }

  render() {
    return (
      <Navbar collapseOnSelect fluid fixedTop>
        <Navbar.Header>
          <Navbar.Brand>
            <LinkContainer to="/" className="navbar-brand">
              {/* <a className="navbar-brand"> */}

              <img src={logo} alt="ocoh logo" />

              {/* </a> */}
            </LinkContainer>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>

        <Navbar.Collapse>
          <Nav>
            <NavDropdown eventKey={3} title="OCOH" id="basic-nav-dropdown">
              <MenuItem eventKey={3.1} href="#why">
                Why OCOH?
              </MenuItem>
              <MenuItem eventKey={3.2} href="#what">
                What is OCOH?
              </MenuItem>

              <MenuItem divider />
              <MenuItem eventKey={3.3} href="#how">
                How it Works?
              </MenuItem>
            </NavDropdown>

            {/*
                NavItem doesnt seem to work. There is a bug in the react-bootstrap code.
                https://github.com/react-bootstrap/react-bootstrap/issues/2365
                @chrisbianca's solution seems to work.

                I am leaving the commented out code for future developers to fix it,
                if the repo had fixed it.
            */}
            {/* <LinkContainer to="#who">
              <NavItem eventKey={4}>About us</NavItem>
            </LinkContainer> */}

            {/* <li role="presentation">
              <a href="#who">About us</a>
            </li> */}

            <LinkContainer to="/aboutus">
              <NavItem eventKey={4}>About us</NavItem>
            </LinkContainer>

            <LinkContainer to="/activities">
              <NavItem eventKey={5}>Activities</NavItem>
            </LinkContainer>
            {/* <li role="presentation">
              <Link to="/activities">Activities</Link>
            </li> */}
          </Nav>
          {this.renderLinks()}
          {/* <Nav pullRight>
            <NavItem eventKey={1} href="/help.html">
              Help
            </NavItem>
            <NavItem eventKey={2} href="/register.html">
              Register Now <span className="sr-only">(current)</span>
            </NavItem>
          </Nav> */}
        </Navbar.Collapse>
      </Navbar>
    );
  }
}

function mapStateToProps(state) {
  return {
    authenticated: state.auth.authenticated,
    type: state.auth.type,
    name: state.auth.name
  };
}

export default connect(mapStateToProps, actions)(Header);
