import React, { Component } from "react";
import { Link } from "react-router-dom";
import Signup from "./signup";
import SignupCoordinator from "./signup_coordinator";

import LeftPanel from "./common/leftpanel";
import RightPanel from "./common/rightpanel";
import QuizHead from "./common/quizhead";

import {
  ProgressBar,
  ToggleButton,
  ToggleButtonGroup,
  ButtonToolbar
} from "react-bootstrap";

class SignupWrapper extends Component {
  constructor(props) {
    super(props);
    this.state = {
      progress: false,
      formtype: ""
    };
    this._showProgress = this._showProgress.bind(this);
    this._hideProgress = this._hideProgress.bind(this);
  }

  /* Spinner indicatior
   *
   */
  _showProgress() {
    this.setState({
      progress: true
    });
  }

  /* Spinner closer
   *
   */
  _hideProgress() {
    this.setState({
      progress: false
    });
  }

  /* Coupled with the button toggle,
   * which indicates which form to render.
   */
  onChangeFormType = formtype => {
    this.setState({
      formtype
    });
  };

  switchForms() {
    if (this.state.formtype === "student") {
      return (
        <Signup
          _showProgress={this._showProgress}
          _hideProgress={this._hideProgress}
        />
      );
    } else if (this.state.formtype === "coordinator") {
      return (
        <SignupCoordinator
          _showProgress={this._showProgress}
          _hideProgress={this._hideProgress}
        />
      );
    } else {
      return (
        // <div>
        //   <div
        //     style={{
        //       textAlign: "center"
        //     }}
        //   >
        //     <span class="text-center">
        //       <h3>OCOH Registration</h3>
        //       <h5>Please select your appropriate role to proceed</h5>
        //     </span>
        //   </div>
        // </div>
        <div />
      );
    }
  }

  render() {
    return (
      <section className="quiz">
        <div className="container">
          <div className="row">
            {/* <div className="col-md-3 side-over">
              <h1>Take a Quiz on Chennai Police</h1>
              <span className="bold">
                Show them how much you appreciate them<br />
                <br />
                <br />
              </span>
              If you have already taken the quiz<br />
              and want to see your score and certificate.<br />
              <br />
              <button className="btn btn-danger">
                <Link to="/signin">Login Here</Link>
              </button>
              <br />
              <br />
            </div> */}

            <LeftPanel />

            <RightPanel>
              <QuizHead />
              <div className="formo">
                <div className="panel panel-default">
                  {this.state.progress ? (
                    <ProgressBar active={true} striped now={100} />
                  ) : (
                    <div />
                  )}
                  <div className="panel-body">
                    <span className="text-center">
                      <h3>OCOH Registration</h3>
                      <h5>Please select your appropriate role to proceed</h5>
                    </span>
                    <div className="selectForm">
                      <label className="control-label col-md-4" />
                      <ButtonToolbar>
                        <ToggleButtonGroup
                          type="radio"
                          name="selectFormType"
                          value={this.state.formtype}
                          onChange={this.onChangeFormType}
                        >
                          <ToggleButton value={"coordinator"}>
                            Institute Co-ordinator
                          </ToggleButton>
                          <ToggleButton value={"student"}>Student</ToggleButton>
                        </ToggleButtonGroup>
                      </ButtonToolbar>
                    </div>
                    <br />
                    <br />
                    {this.switchForms()}
                  </div>
                </div>
                <div>
                  <a href="https://www.useloom.com/share/8340099e403846aab27fce4fd3b925c1">
                    Click here
                  </a>{" "}
                  to view the screen tutorial for Institute Coordinator
                  Registration
                  <br />
                  <a href="https://www.useloom.com/share/5e0b531085e643568b7056f5008f8059">
                    Click here
                  </a>{" "}
                  to view the screen tutorial for Student Registration
                </div>
              </div>
            </RightPanel>
          </div>
        </div>
      </section>
    );
  }
}

export default SignupWrapper;
