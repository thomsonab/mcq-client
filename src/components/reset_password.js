import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
import { ProgressBar, PageHeader } from "react-bootstrap";
import { Link } from "react-router-dom";

import { connect } from "react-redux";
import * as actions from "../actions";

import RightPanel from "./common/rightpanel";
import LeftPanel from "./common/leftpanel";
import QuizHead from "./common/quizhead";
import "../static/css/form.css";
import "../static/css/signin.css";

class ResetPassword extends Component {
  constructor(props) {
    super(props);
  }

  handleFormSubmit({ password, confirmpassword }) {
    this.setState({
      progress: true
    });
    this.props.resetPassword({
      confirmpassword: confirmpassword,
      password: password,
      token: this.props.match.params.token
    });
  }

  renderAlert() {
    if (this.props.errorMessage) {
      return (
        <div className="alert alert-danger">
          <strong> Oops!</strong> {this.props.errorMessage}
        </div>
      );
    }
  }

  renderFormField(field) {
    const { meta: { touched, error } } = field;
    return (
      <div>
        <fieldset className="form-group">
          <label>{field.label}</label>
          <input
            type={field.type}
            {...field.input}
            placeholder={field.placeholder}
            className="form-control"
          />
        </fieldset>
        {touched && error && <div className="error">{error}</div>}
      </div>
    );
  }

  renderForm() {
    const { handleSubmit, pristine, submitting } = this.props;
    if (!this.props.success) {
      return (
        <form
          className="form-group"
          onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}
        >
          <Field
            name="password"
            component={this.renderFormField}
            label="Password"
            type="password"
            placeholder="Enter password"
          />

          <Field
            name="confirmpassword"
            component={this.renderFormField}
            label="Confirm Password"
            type="password"
            placeholder="Enter the same password entered above"
          />

          {this.renderAlert()}
          <button
            action="submit"
            className="btn btn-primary"
            disabled={submitting}
          >
            Reset Password
          </button>
        </form>
      );
    } else {
      return (
        <div>
          Your Password has been reset successfully.
          <br />
          <br />
          <Link to="/signin" className="btn btn-danger">
            Click to SignIn
          </Link>
        </div>
      );
    }
  }

  render() {
    return (
      <section className="quiz">
        <div className="container">
          <div className="row">
            <LeftPanel />
            <RightPanel>
              <QuizHead />
              <div className="formo col-md-8">
                <div className="panel panel-default">
                  {this.props.progress && !this.props.errorMessage ? (
                    <ProgressBar active={true} striped now={100} />
                  ) : (
                    <div />
                  )}
                  <div className="panel-body">{this.renderForm()}</div>
                </div>
              </div>
            </RightPanel>
          </div>
        </div>
      </section>
    );
  }
}

function validate(values) {
  const errors = {};
  if (!values.password) {
    errors.password = "Please Enter a password";
  }
  if (values.confirmpassword !== values.password) {
    errors.confirmpassword = "The password entered doesnt match";
  }
  return errors;
}

function mapStateToProps(state) {
  return {
    errorMessage: state.resetpassword.errorMessage,
    success: state.resetpassword.success,
    progress: state.resetpassword.spinner
  };
}

export default reduxForm({
  form: "resetpassword",
  validate: validate
})(connect(mapStateToProps, actions)(ResetPassword));
