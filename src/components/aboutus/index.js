import React from "react";
import "./aboutus.css";

import emlogo from "../../static/images/aboutus/1.jpg";
import cssclogo from "../../static/images/aboutus/2.jpg";
import gcplogo from "../../static/images/aboutus/3.jpg";
import cchg from "../../static/images/aboutus/4.jpg";
import aggumedialogo from "../../static/images/aboutus/5.jpg";
import vfs from "../../static/images/aboutus/6.jpg";
import mog from "../../static/images/aboutus/mog.jpg";

export default function(props) {
  return (
    <section className="register">
      <div className="container">
        <h2 className="pan-title">Who We are?</h2>
        <div className="row">
          <div className="col-md-3">
            <br />
            <img
              src={emlogo}
              alt="education_matters_logo"
              className="text-right"
            />
          </div>
          <div className="col-md-9 text-justify">
            <h3 className="pan-title">Education Matters</h3>
            <p>
              {" "}
              Education Matters, estd. In 2003, is an award winning
              International Education Consultancy with more than 30 man years of
              experience behind the team We are a socially responsible company
              and we believe that our police officers deserve, appreciation and
              recognition from our community, so we launch our passion “Our Cops
              Our Heroes”
            </p>
            <a href="http://edmat.org" className="btn btn-default">
              Know More
            </a>
          </div>
        </div>

        <hr className="mt-5 bg-6" />
        <div className="row">
          <div className="col-md-3">
            <br />
            <img
              src={cssclogo}
              alt="Chennai Sahodaya School Complex logo"
              className="text-right"
            />
          </div>
          <div className="col-md-9 text-justify">
            <h3 className="pan-title">Chennai Sahodaya School Complex</h3>
            <p>
              {" "}
              An interactive forum and liaison between schools and the CBSE
              Board to establish new benchmarks of quality We promote academic
              collaboration through teacher exchanges, common science fairs,
              joint seminars, workshops, exchange of library facilities, among
              member schools
            </p>
            <a
              href="http://www.chennaisahodaya.org/"
              className="btn btn-default"
            >
              Know More
            </a>
          </div>
        </div>
        <br />
        <hr className="mt-5 bg-6" />
        <br />
        <div className="row">
          <div className="col-md-3 align-r">
            <h5>Strategic Partner</h5>
            <br />
            <img
              src={gcplogo}
              alt="Greater Chennai Police Logo"
              className="text-right"
            />
            <h3 className="pan-title">Greater Chennai Police</h3>
            <a href="http://www.tnpolice.gov.in/" className="btn btn-default">
              Know More
            </a>
          </div>
          <div className="col-md-3 align-r">
            <h5>Associate Partner</h5>
            <br />
            <img
              src={cchg}
              alt="Chennai City Home Guard logo"
              className="text-right"
            />
            <h3 className="pan-title">Chennai City Home Guard</h3>
            <a href="#" className="btn btn-default">
              Know More
            </a>
          </div>
          <div className="col-md-3 align-r">
            <h5>Creative Partner</h5>
            <br />
            <img
              src={aggumedialogo}
              alt="Aggu media logo"
              className="text-right"
            />
            <h3 className="pan-title">Aggu Media</h3>
            <a href="http://www.aggu.media/" className="btn btn-default">
              Know More
            </a>
          </div>
          <div className="col-md-3 align-r">
            <h5>Sponsor</h5>
            <br />
            <img src={vfs} alt="Vancouver Film School" className="text-right" />
            <h3 className="pan-title">VFS</h3>
            <a href="http://www.vfs.edu/" className="btn btn-default">
              Know More
            </a>
          </div>
        </div>
        <br />
        <hr className="mt-5 bg-6" />
        <br />
        <h2 className="pan-title">Message of Good Wishes</h2>
        <a href="https://goo.gl/HjFX8j" target="_blank">
          <img src={mog} alt="message of good wishes" className="img-full" />
        </a>
        <br />
        <hr className="mt-5 bg-6" />
        <br />
        <div className="row text-center">
          <div className="col-md-6">
            <h2 className="pan-title">Implementation Team</h2>
            <span className="bold">Mr. S. Namasivayam, </span>
            <br />
            Senior Principal ,Maharishi Vidya Mandir Senior Sec .School <br />
            <br />
            <span className="bold">Dr. J Ajeeth Prasath Jain, </span>
            <br />
            Senior Principal, Bhavan's Rajaji Vidyashram<br />
            <br />
            <span className="bold">Sardar Manjit Singh Nayar, </span>
            <br />
            General Secretary & Correspondent, Guru Nanak College<br />
            <br />
            <span className="bold">Mr.Sudhir Kumar Lodha, </span>
            <br />
            Member State Minority Commission, Government of Tamilnadu<br />
            <br />
            <span className="bold">Mr.P.Aravindhan, IPS, </span>
            <br />
            <br />
            <br />
            <span className="bold">Vijay Kumar Nair, </span>
            <br />
            Director, Education Matters <br />
            <br />
          </div>
          <div className="col-md-6">
            <h2 className="pan-title">Advisory Team</h2>
            <br />
            <span className="bold">Rev. Dr. M. Arociasamy Xavier.S.J, </span>
            <br />
            Principal, Loyola College<br />
            <br />
            <span className="bold">Dr. (Mrs). Thangam Meganathan, </span>
            <br />
            Chairperson, Rajalakshmi Engineering College<br />
            <br />
            <span className="bold">Dr. Annie Jacob,</span>
            <br />
            Director, KCG College of Technology<br />
            <br />
            <span className="bold">Dr. Lalitha Balakrishnan,</span>
            <br />
            Principal ,M.O.P.Vaishnav College for Women<br />
            <br />
            <span className="bold">Thiru. R. Sudhakar, IPS, </span>
            <br />
            JC - North, Greater Chennai Police <br />
            <br />
            <span className="bold">Mr. V. Bala Krishnan, IPS,</span>
            <br />
            Deputy Commissioner of Police<br />
            <br />
            <span className="bold">Dr. Sr. Jasintha Quadras, </span>
            <br />
            Principal, Stella Maris College<br />
            <br />
            <br />
          </div>
        </div>
      </div>
    </section>
  );
}
