import React from "react";

import "../static/css/footer.css";

export default function() {
  return (
    <footer className="site-footer section b-t">
      <div className="container pb-3">
        <div className="row text-left">
          <div className="col-md-5 mr-auto">
            <h4 className="color-5">
              What is <span className="bold">OCOH</span>?
            </h4>
            <p>
              Our Cops Our Hero’s is a CSR initiative of Education Matters in
              association with Chennai Sahodaya School Complex, Greater Chennai
              Police &amp; Chennai City Home Guards{" "}
              <span className="bold">
                that aims to cultivate a youth community that appreciates
                police, who do a great job
              </span>.
            </p>
          </div>
          <div className="col-md-2">
            <h4 className="color-5">
              <span className="bold">Follow</span> Us
            </h4>
            <nav className="nav flex-column mt-3">
              <a className="nav-link pl-0" href="http://fb.me/OurCopsOurHeroes">
                <i className="fa-facebook" /> Facebook
              </a>{" "}
              <a className="nav-link pl-0" href="http://twitter.com/ocoh_in">
                <i className="fa-twitter" /> Twitter
              </a>{" "}
            </nav>
          </div>
          <div className="col-md-4">
            <h4 className="color-5">
              Get<span className="bold"> in touch</span>
            </h4>
            <p>
              For any query
              <br />
              <span className="bold">
                Call us at +91 9841840004 or <br />Write
              </span>{" "}
              to us at ocoh@edmat.org
            </p>
          </div>
        </div>
        <hr className="mt-5 bg-6" />
        <div className="row footer-bottom small align-items-center">
          <div className="col-md-4">
            <p className="mt-2 mb-0">
              © 2017 Education Matters. All Rights Reserved
            </p>
          </div>

          <div className="col-md-4 col-md-auto">
            <nav className="nav justify-content-center justify-content-md-end">
              <a className="nav-link" href="#">
                Privacy Policy |
              </a>{" "}
              <a className="nav-link" href="#">
                Terms of Service |
              </a>{" "}
              <a className="nav-link" href="#">
                Sitemap
              </a>
            </nav>
          </div>
        </div>
      </div>
    </footer>
  );
}
