import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
import { Link } from "react-router-dom";
import { ProgressBar } from "react-bootstrap";

import { connect } from "react-redux";
import * as actions from "../actions";

import RightPanel from "./common/rightpanel";

import "../static/css/form.css";
import "../static/css/signin.css";

import QuizHead from "./common/quizhead";

class Signin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      progress: false
    };
  }

  componentWillMount() {
    this.props.removeAuthError();
  }
  handleFormSubmit({ email, password }) {
    this.setState({
      progress: true
    });
    this.props.signinUser({ email: email, password: password });
  }

  renderAlert() {
    if (this.props.errorMessage) {
      return (
        <div className="alert alert-danger">
          <strong> Oops!</strong> {this.props.errorMessage}
        </div>
      );
    }
  }

  renderFormField(field) {
    return (
      <div>
        <fieldset className="form-group">
          <label>{field.label}</label>
          <input
            type={field.type}
            {...field.input}
            placeholder={field.placeholder}
            className="form-control"
          />
        </fieldset>
      </div>
    );
  }

  render() {
    const { handleSubmit } = this.props;
    return (
      <section className="quiz">
        <div className="container">
          <div className="row">
            <div className="col-md-3 side-over">
              <h1>Not yet Registered?</h1>
              <span className="bold">
                Register as a student or as an Institute Co-ordinator<br />
                <br />
              </span>
              <button className="btn btn-danger">
                <Link to="/signup">Register</Link>
              </button>

              <br />
              <br />
            </div>
            <RightPanel>
              <QuizHead />
              <div className="formo col-md-8">
                <div className="panel panel-default">
                  {this.state.progress && !this.props.errorMessage ? (
                    <ProgressBar active={true} striped now={100} />
                  ) : (
                    <div />
                  )}
                  <div className="panel-body">
                    <form
                      onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}
                    >
                      <Field
                        name="email"
                        component={this.renderFormField}
                        label="Email"
                        type="text"
                        placeholder="Enter email"
                      />
                      <Field
                        name="password"
                        component={this.renderFormField}
                        label="Password"
                        type="password"
                        placeholder="Enter password"
                      />
                      {this.renderAlert()}
                      <button action="submit" className="btn btn-primary">
                        Sign in
                      </button>{" "}
                      <Link className="btn btn-danger" to="/">
                        Cancel
                      </Link>{" "}
                      <Link to="/forgotpassword">Forgot Password</Link>
                    </form>
                  </div>
                </div>
                <div>
                  <a href="https://www.useloom.com/share/8340099e403846aab27fce4fd3b925c1">
                    Click here
                  </a>{" "}
                  to view the screen tutorial for Institute Coordinator
                  Registration
                  <br />
                  <a href="https://www.useloom.com/share/5e0b531085e643568b7056f5008f8059">
                    Click here
                  </a>{" "}
                  to view the screen tutorial for Student Registration
                </div>
              </div>
            </RightPanel>
          </div>
        </div>
      </section>
    );
  }
}

function mapStateToProps(state) {
  return {
    errorMessage: state.auth.error
  };
}

export default reduxForm({
  form: "signin"
})(connect(mapStateToProps, actions)(Signin));
