import React, { Component } from "react";
import { connect } from "react-redux";
import jsPDF from "jspdf";
import * as actions from "../actions";

import quizhead from "../static/images/quizhead.png";
import base64image from "./student/image";

class Scores extends Component {
  constructor(props) {
    super(props);
    this.downloadPdf = this.downloadPdf.bind(this);
    this.generatePdf = this.generatePdf.bind(this);
  }
  componentWillMount() {
    this.props.fetchScore();
  }

  generatePdf() {
    let doc = new jsPDF({
      orientation: "landscape",
      format: "a4"
    });

    doc.addImage(base64image, "JPEG", 1, 1, 290, 210);
    doc.setFont("helvetica");
    doc.setFontSize(20);
    doc.setFontType("bold");
    if (this.props.score <= 6) {
      doc.text(48, 130, "Certificate of Appreciation");
    } else {
      doc.text(48, 130, "Certificate of Merit");
    }

    doc.setFontSize(13);
    doc.setFontType("normal");
    doc.text(
      160,
      75,
      `This to certify that ${this.props.name ||
        localStorage.getItem("Name")} of `
    );
    doc.text(
      160,
      85,
      `${this.props.school || localStorage.getItem("School")} has`
    );

    if (this.props.score > 6) {
      doc.text(160, 95, "completed the OCOH quiz on Greater Chennai Police");

      doc.text(160, 105, `with a commendable score of ${this.props.score}/10`);

      doc.text(
        160,
        115,
        "and has shown a greater appreciation towards our police"
      );

      doc.text(
        160,
        125,
        "officers, who do a great job towards student safety."
      );
    } else {
      doc.text(160, 95, "taken the OCOH quiz on Greater Chennai Police");

      doc.text(160, 105, `and has shown a greater appreciation towards`);
      doc.text(160, 115, "our police officers, who do a great job towards");

      doc.text(160, 125, "student safety.");
    }
    doc.save();
  }
  downloadPdf(e) {
    e.preventDefault();
    let doc = new jsPDF();
    const margins = {
      top: 80,
      bottom: 60,
      left: 40,
      width: 522
    };

    const source = document.getElementById("section-to-print");

    const specialElementHandlers = {
      // element with id of "bypass" - jQuery style selector
      "#bypassme": function(element, renderer) {
        // true = "handled elsewhere, bypass text extraction"
        return true;
      }
    };
    doc.fromHTML(
      source,
      margins.left,
      margins.top,
      {
        width: margins.width,
        elementHandlers: specialElementHandlers
      },
      function(dispose) {
        // dispose: object with X, Y of the last line add to the PDF
        //          this allow the insertion of new lines after html
        doc.save("Test.pdf");
      },
      margins
    );
  }
  render() {
    const div1style = {
      width: 800,
      height: 600,
      padding: 20,
      "text-align": "center",
      border: "10px solid #787878"
    };

    const div2style = {
      width: 750,
      height: 550,
      padding: 20,
      "text-align": "center",
      border: "5px solid #787878"
    };

    const spanstyle = {
      "font-size": 50,
      "font-weight": "bold"
    };

    let testing = new jsPDF({
      orientation: "landscape",
      format: "a4"
    });

    let certificate = (
      <section class="quiz">
        <div class="container">
          <div class="row">
            <div class="col-md-3 side-over">
              <h1>Take a Quiz on Chennai Police</h1>{" "}
              <span class="bold">
                Show them how much you appreciate them<br />
                <br />
                <br />
              </span>{" "}
              Thanks for taking the Quiz.
              <br />
              <br />
              <button
                class="btn btn-danger"
                onClick={this.generatePdf}
                disabled={this.props.score ? false : true}
              >
                Download Certificate
              </button>
              <br />
              <br />{" "}
            </div>
            <div class="col-md-9 main-over">
              {" "}
              <img src={quizhead} alt="" />
              <div class="formo">
                <h1>Thanks for taking the Chennai Police Quiz!</h1>
                Download your certificate and Share with your friends to have
                them show their appreciation too.
              </div>
            </div>
          </div>
        </div>
      </section>
    );

    if (this.props.score) {
      return certificate;
    } else {
      return <div />;
    }
  }
}

function mapStateToProps(state) {
  return {
    score: state.questions.score,
    name: state.auth.name,
    school: state.auth.school
  };
}
export default connect(mapStateToProps, actions)(Scores);
