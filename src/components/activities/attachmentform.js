import React, { Component } from "react";
import { connect } from "react-redux";
import {
  clearError,
  uploadGoodCopMoment
} from "../../actions/student/good_cop_moment";

class AttachmentForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      file: null
    };
    this.onChange = this.onChange.bind(this);
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }

  onChange(e) {
    this.props.clearError();
    this.setState({ file: e.target.files[0] });
  }

  onFormSubmit(e) {
    e.preventDefault();

    this.props.uploadGoodCopMoment({ file: this.state.file });
  }
  render() {
    return (
      <div className="container">
        <form onSubmit={this.onFormSubmit}>
          <fieldset className="form-group">
            <label>Upload Good Cop Moment</label>
            <input type="file" onChange={this.onChange} />
            {this.props.student && !this.props.student.error ? (
              ""
            ) : (
              <div className="error">{this.props.student.error}</div>
            )}
          </fieldset>
          <button
            type="submit"
            disabled={
              !this.state.file ||
              (this.props.student && this.props.student.error)
            }
          >
            {" "}
            Upload{" "}
          </button>
        </form>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    student: state.student
  };
}

export default connect(mapStateToProps, { clearError, uploadGoodCopMoment })(
  AttachmentForm
);
