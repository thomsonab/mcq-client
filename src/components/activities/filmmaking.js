import React from "react";
import { Link } from "react-router-dom";

export default function(props) {
  return (
    <div>
      <br />
      <br />
      <h4 className="mb-4 font-md color-1">Film making on OCOH Theme</h4>
      <p>
        The activity is to make a film on the theme of 'Our Cops Our Heroes'.
        The selected films will be aired at the OCOH Forum Grand Finale.
      </p>

      <div className="col-md-12 row">
        <div className="col-md-6">
          <h2>
            Rules &amp; Regulations for Short Film Making for College Students
          </h2>
          <ul>
            <li>The participant should have a valid college identity card.</li>
            <li>
              Student must register his/her participation in the film making
              competition online on www.edmat.org on or before 30 January 2018
            </li>
            <li>
              The duration of the submission of should be within 10-15 minutes,
              including the title cards and credits.
            </li>
            <li>
              Any film material used in your film must be original, or
              permission from the source must be obtained. This responsibility
              rests entirely with the filmmaker. Failure to obtain clearance
              could result in your film being withdrawn from the competition.{" "}
            </li>
            <li>
              The film must have subtitles in English, irrespective of the
              language spoken in the film.
            </li>
            <li>
              The film should not offend any caste, religion, or creed. The film
              should not demean any police officials or the department.{" "}
            </li>
            <li>
              The screening committee will carry out the first level of
              selection and best 10 film will be presented to the Jury Panel{" "}
            </li>
            <li>
              The selected participants will be informed a week before the Our
              Cops Our Heroes Forum.
            </li>
            <li>
              Participant can use any recording device for film making. But the
              video quality must be a minmum of 720p.
            </li>
            <li>
              The private link of the vedipo hosted in streaming platform like
              youtube, vimeo must be submitted online on www.edmat.org, through
              student dash board
            </li>
            <li>
              The screening committee will determine the date and time of
              screening the submissions. The organizer cannot be held
              accountable for changes in the schedule of the film lineup.
            </li>
            <li>
              Selected entries must email still photos/ posters and a 10-15 word
              synopsis for use in the screening. Please ensure that the final
              output with the highest quality possible is sent for submission.
            </li>
            <li>
              Please do not send DVD’s or copies of your film direct to us
              unless requested to do so.{" "}
            </li>
            <li>
              The organizer reserves the right to refuse to accept and/or screen
              any submission deemed to be inappropriate.{" "}
            </li>
            <li>
              Your entry is subjected to your institutional approval, and
              necessary details will be emailed on submission of the film on
              www.edmat.org
            </li>
            <li>
              All non technical and technical queries relating to your
              submission should be sent to fm@ocoh.in
            </li>
            <li>
              Submission are open for the categories of short film, documentary
              and animation
            </li>
            <li>
              The submission of a film to OUR COPS OUR HEROES will be considered
              to be an acknowledgement of the festival rules.
            </li>

            <br />
            <br />
          </ul>
        </div>
        <div className="col-md-6">
          <h2>
            Rules and Regulations for Public Service Announcement (PSA) for
            School Students
          </h2>
          <ul>
            <li>The participant should have a valid school identity card.</li>
            <li>
              Student must register his/her participation in the film making
              competition online on www.edmat.org on or before 30 January 2018
            </li>
            <li>
              The duration of the submission of should be within 10-15 minutes,
              including the title cards and credits.
            </li>
            <li>
              Any film material used in your film must be original, or
              permission from the source must be obtained. This responsibility
              rests entirely with the filmmaker. Failure to obtain clearance
              could result in your film being withdrawn from the competition.{" "}
            </li>
            <li>
              The film must have subtitles in English, irrespective of the
              language spoken in the film.
            </li>
            <li>
              The film should not offend any caste, religion, or creed. The film
              should not demean any police officials or the department.{" "}
            </li>
            <li>
              The screening committee will carry out the first level of
              selection and best 10 film will be presented to the Jury Panel{" "}
            </li>
            <li>
              The selected participants will be informed a week before the Our
              Cops Our Heroes Forum.
            </li>
            <li>
              The private link of the vedipo hosted in streaming platform like
              youtube, vimeo must be submitted online on www.edmat.org, through
              student dash board
            </li>
            <li>
              The screening committee will determine the date and time of
              screening the submissions. The organizer cannot be held
              accountable for changes in the schedule of the film lineup.
            </li>
            <li>
              Selected entries must email still photos/ posters and a 10-15 word
              synopsis for use in the screening. Please ensure that the final
              output with the highest quality possible is sent for submission.
            </li>
            <li>
              Please do not send DVD’s or copies of your film direct to us
              unless requested to do so.{" "}
            </li>
            <li>
              The organizer reserves the right to refuse to accept and/or screen
              any submission deemed to be inappropriate.{" "}
            </li>
            <li>
              Your entry is subjected to your institutional approval, and
              necessary details will be emailed on submission of the film on
              www.edmat.org
            </li>
            <li>
              All non technical and technical queries relating to your
              submission should be sent to fm@ocoh.in
            </li>
            <li>
              Submission are open for the categories of short film, documentary
              and animation
            </li>
            <li>
              The submission of a film to OUR COPS OUR HEROES will be considered
              to be an acknowledgement of the festival rules.
            </li>
          </ul>
        </div>
      </div>
      {/* <a
        href="https://goo.gl/forms/4ImqDajeSVOUI9Nq2"
        className="btn btn-small btn-danger"
      >
        Submit Your Entry
      </a> */}
      <Link to="/studentdashboard" className="btn btn-small btn-danger">
        Submit You Entry
      </Link>
      <br />
      <br />
    </div>
  );
}
