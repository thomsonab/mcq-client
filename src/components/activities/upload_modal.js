import React, { Component } from "react";
import { Button, Modal } from "react-bootstrap";
import AttachmentForm from "./attachmentform";

class UploadPdf extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false
    };
  }

  close() {
    this.setState({ showModal: false });
  }

  open() {
    this.setState({ showModal: true });
  }

  render() {
    return (
      <div>
        <Button bsStyle="primary" bsSize="large" onClick={this.open.bind(this)}>
          Upload Good Cop Moment
        </Button>
        <Modal show={this.state.showModal} onHide={this.close.bind(this)}>
          <Modal.Header closeButton>
            <Modal.Title>Good Cop Moment</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <AttachmentForm />
          </Modal.Body>

          <Modal.Footer>
            <Button onClick={this.close.bind(this)}>Close</Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default UploadPdf;
