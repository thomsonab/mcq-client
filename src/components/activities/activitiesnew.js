import React from "react";

import { Link } from "react-router-dom";

export default function(props) {
  return (
    <section className="register">
      <div className="container">
        <div className="row">
          <div className="col-md-4 text-right">
            <h2>Good Cop Moment</h2>

            <Link to="/activities/1" className="btn btn-danger">
              Rules, Guidelines, How tos &amp; Contact
            </Link>
          </div>
          <div className="col-md-8">
            <h4 className="mb-4 color-5 font-md">
              <br />Share your Good Cop Moment
            </h4>
            <p>
              Our initiative “OUR COP OUR HEROES” isn't about politics, race, or
              religion. All of us have been positively impacted by law
              enforcement, and now is the time to show them our appreciation. Do
              you have a moment of good experience / inspiration / interaction,
              with our police? Share it with us
            </p>
            <Link to="/studentdashboard/1" className="btn btn-primary">
              Signup &amp; Submit your Entry
            </Link>
          </div>
        </div>{" "}
        <hr className="mt-5 bg-6" />
        <div className="row">
          <div className="col-md-4 text-right">
            <h2>Film Making</h2>
            <Link to="/activities/2" className="btn btn-danger">
              Rules, Guidelines, How tos &amp; Contact
            </Link>
          </div>
          <div className="col-md-8 ">
            <h4 className="mb-4 font-md color-1">
              <br />Film making on OCOH Theme
            </h4>
            <p>
              {" "}
              The activity is to make a film on the theme of 'Our Cops Our
              Heroes'. <br />The selected films will be aired at the OCOH Forum
              Grand Finale.{" "}
            </p>
            <Link to="/studentdashboard/4" className="btn btn-primary">
              Signup &amp; Submit your Entry
            </Link>
          </div>
        </div>{" "}
        <hr className="mt-5 bg-6" />
        <div className="row">
          <div className="col-md-4 text-right">
            <h2>Book Cover Art Design</h2>
            <Link to="/activities/3" className="btn btn-danger">
              Rules, Guidelines, How tos &amp; Contact
            </Link>
          </div>
          <div className="col-md-8">
            <h4 className="mb-4 color-5 font-md">
              <br />Design a Cover Art
            </h4>
            <p>
              The activity is to design a art / illustration to feature in the
              cover of 'Book of Sagas' that will have the collection of all
              inspiring stories of our Cop Heroes. The covert design must
              reflect the essence of appreciating the police's inspiring and
              hardwork to our community.
            </p>
            <Link to="/studentdashboard/3" className="btn btn-primary">
              Signup &amp; Submit your Entry
            </Link>
          </div>
        </div>
      </div>
    </section>
  );
}
