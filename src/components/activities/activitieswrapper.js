import React from "react";
import { Tab, Tabs } from "react-bootstrap";

import CoverArtDesign from "./coverartdesign";
import FilmMaking from "./filmmaking";
import GoodCopMoment from "./goodcopmoment";

import LeftPanel from "../common/leftpanel";
import RightPanel from "../common/rightpanel";
import QuizHead from "../common/quizhead";

export default function(props) {
  return (
    <div className="container">
      {/* <LeftPanel />

      <RightPanel>
        <QuizHead /> */}
      <Tabs
        defaultActiveKey={
          props.match.params.id ? Number.parseInt(props.match.params.id) : 1
        }
        id="activities-dashboard"
      >
        <Tab eventKey={1} title="Good Cop Moment">
          <GoodCopMoment />
        </Tab>
        <Tab eventKey={2} title="Film Making">
          <FilmMaking />
        </Tab>
        <Tab eventKey={3} title="Cover Art Design">
          <CoverArtDesign />
        </Tab>
      </Tabs>
      {/* </RightPanel> */}
    </div>
  );
}
