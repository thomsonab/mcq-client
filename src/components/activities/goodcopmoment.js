import React from "react";
import { Link } from "react-router-dom";

export default function(props) {
  return (
    <div>
      <br />
      <br />
      <h4 className="mb-4 color-5 font-md">Share your Good Cop Moment</h4>
      <p>
        Our initiative “OUR COP OUR HEROES” isn't about politics, race, or
        religion. All of us have been positively impacted by law enforcement,
        and now is the time to show them our appreciation. Do you have a moment
        of good experience / inspiration / interaction, with our police? Share
        it with us
      </p>

      <div className="col-md-12 row">
        <div className="col-md-6">
          <h2>Rules &amp; Regulations</h2>
          <ul>
            <li>Only School and College students can participate</li>
            <li>Age Group - Open to all</li>
            <li>One entry per student</li>
            <li>
              The submission must be made online by clicking submit entry button
              at the bottom
            </li>
            <li>
              The entry must be approved by your institute's OCOH Coordinator
              before being acccepted officaily.
            </li>
            <li>
              Parents can proxy for students below the age of 10 years.
              (Requires approval from the respective Head of Institution)
            </li>

            <li>
              All selected entries will be published in the ‘Book of Sagas’
            </li>
            <li>
              Organising Committee hand picks ten inspiring stories for
              face-to-face session with their heroes. The decision of selecting
              those ten lies entirely with the organising committee and is
              final.
            </li>
            <br />
            <br />
          </ul>
        </div>
        <div className="col-md-6">
          <h2>Checklist before Entry Submission</h2>
          <ul>
            <li>Last date for submission is 20.03.2018</li>
            <li>View the entry and go through it fully before submitting</li>
            <li>
              Make sure there is no objectionable or immoral content exixts
            </li>
            <li>
              Make sure the submitted entry is of original work and not
              plagarised
            </li>
            <li>Ensure the entry is representing your institution</li>
            <li>
              Make sure you have pre-approval from your institution for this
              entry
            </li>
            <li>
              Ensure the entry is approved by your institution after submission
            </li>
            <li>
              The entry will be accepted and published post approval,
              verification and subjected to moral standards of the content.
            </li>
            <li>
              The decision to accept or reject an entry lies with the organising
              committee and is final
            </li>
            <br />
            <br />
          </ul>
        </div>
      </div>
      {/* <a
        href="https://goo.gl/forms/kp2EXx7pQ99aqqIh1"
        className="btn btn-small btn-danger"
      >
        Submit Your Entry
      </a> */}
      <Link to="/studentdashboard" className="btn btn-small btn-danger">
        Submit You Entry
      </Link>
      {"  "}

      <br />
      <br />
    </div>
  );
}
