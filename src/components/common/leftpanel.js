import React from "react";

export default function(props) {
  return (
    <div className="col-md-3 side-over side-menu">
      <br />
      <button type="submit" className="btn btn-sidemenu">
        OCOH Info Book
      </button>
      <br />
      <button type="submit" className="btn btn-sidemenu">
        Important Dates
      </button>
      <br />
      <button type="submit" className="btn btn-sidemenu">
        Activity Guidelines
      </button>
      <br />
      <button type="submit" className="btn btn-sidemenu">
        Student Safety Pocket Book
      </button>
      {props.children}
      <br />
      <br />
      <br />
    </div>
  );
}
