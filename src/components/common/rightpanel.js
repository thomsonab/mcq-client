import React from "react";

export default function(props) {
  return <div className="col-md-9 main-over">{props.children}</div>;
}
