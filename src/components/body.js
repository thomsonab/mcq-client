import React from "react";
import { Link } from "react-router-dom";

import JumboContainer from "./body/jumbocontainer";
import Why from "./body/why";
import Who from "./body/who";
import What from "./body/what";
import How from "./body/how";
import ActivitiesList from "./body/activitieslist";

import quizhead from "../static/images/quizhead.png";
export default function(props) {
  return (
    <div>
      <JumboContainer />
      <Why />
      <Who />
      <What />
      <How />
      <ActivitiesList />
    </div>
  );
}
