import React, { Component } from "react";
import { connect } from "react-redux";
import * as actions from "../../actions/admin";

import { formattedDate } from "../../utils/date";
import StudentExtraInfo from "./StudentExtraInfo";

import {
  BootstrapTable,
  TableHeaderColumn,
  ExportCSVButton
} from "react-bootstrap-table";

import "../../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css";

/* Below code is for expanding stuffs and
   * showing up additional details
   * 
   */
function expandComponent(row) {
  return (
    <StudentExtraInfo
      studentsubmissions={{
        goodcopmoment: row.goodcopmoment,
        coverart: row.coverart,
        film: row.filmmaking
      }}
    />
  );
}

class StudentList extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    return this.props.fetchStudentList();
  }
  /* Below code is for csv button
   * 
   */
  handleExportCSVButtonClick = onClick => {
    // Custom your onClick event here,
    // it's not necessary to implement this function if you have no any process before onClick
    // console.log('This is my custom function for ExportCSVButton click event');
    onClick();
  };
  createCustomExportCSVButton = onClick => {
    return (
      <ExportCSVButton
        btnText="Export Data"
        onClick={() => this.handleExportCSVButtonClick(onClick)}
      />
    );
  };

  isExpandableRow(row) {
    if (row.coverart && row.coverart.length > 0) {
      return true;
    }

    if (row.filmmaking && row.filmmaking.length > 0) {
      return true;
    }

    if (row.goodcopmoment && row.goodcopmoment.length > 0) {
      return true;
    }

    return false;
  }

  expandColumnComponent({ isExpandableRow, isExpanded }) {
    let content = "";

    if (isExpandableRow) {
      content = isExpanded ? "(-)" : "(+)";
    } else {
      content = " ";
    }
    return <div> {content} </div>;
  }

  render() {
    const options = {
      exportCSVBtn: this.createCustomExportCSVButton
    };
    return (
      <div>
        <h3> Registred Students </h3>
        <div>
          <BootstrapTable
            data={this.props.studentlist}
            search={true}
            multiColumnSearch={true}
            pagination
            options={options}
            exportCSV
            expandableRow={this.isExpandableRow}
            expandComponent={expandComponent}
            expandColumnOptions={{
              expandColumnVisible: true,
              expandColumnComponent: this.expandColumnComponent,
              columnWidth: 50
            }}
          >
            <TableHeaderColumn
              dataField="id"
              width="40"
              isKey
              searchable={false}
              export={false}
            >
              ID
            </TableHeaderColumn>
            <TableHeaderColumn dataField="name" width="280" dataSort={true}>
              Student Name
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField="institute"
              width="280"
              dataSort={true}
            >
              Institution
            </TableHeaderColumn>

            <TableHeaderColumn dataField="level" width="150" dataSort={true}>
              Class
            </TableHeaderColumn>
            <TableHeaderColumn dataField="email" width="200">
              Email
            </TableHeaderColumn>
            <TableHeaderColumn dataField="phone" width="120">
              Phone
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField="goodcopmomentyn"
              width="70"
              dataSort={true}
            >
              GoodCopMomentYN
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField="coverartyn"
              width="70"
              dataSort={true}
            >
              CoverartYN
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField="filmmakingyn"
              width="70"
              dataSort={true}
            >
              FilmMakingYN
            </TableHeaderColumn>
          </BootstrapTable>
        </div>
      </div>
    );

    //return <div> Check console.log </div>;
  }
}

function mapStateToProps(state) {
  return {
    studentlist: state.admin.studentlist
  };
}
export default connect(mapStateToProps, actions)(StudentList);
