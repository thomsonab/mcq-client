import React, { Component } from "react";
import { Tab, Tabs } from "react-bootstrap";
import Institutions from "./institutions";
import SelectedGoodCopMoments from "./goodcopmoments";
import SelectedCoverarts from "./coverarts";
import SelectedFilms from "./films";
import StudentList from "./studentlist";
import Reports from "./reports";

class AdminDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      key: 1
    };
  }

  handleSelect(key) {
    this.setState({ key });
  }

  render() {
    return (
      <div>
        <Tabs
          activeKey={this.state.key}
          onSelect={this.handleSelect.bind(this)}
          id="dashboard-tab"
        >
          <Tab eventKey={1} title="Registered Institutions">
            <Institutions />
          </Tab>

          <Tab eventKey={2} title="Registered Students">
            <StudentList />
          </Tab>

          <Tab eventKey={3} title="Selected GoodCopMoments">
            <SelectedGoodCopMoments />
          </Tab>

          <Tab eventKey={4} title="Selected Coverarts">
            <SelectedCoverarts />
          </Tab>

          <Tab eventKey={5} title="Selected Films">
            <SelectedFilms />
          </Tab>

          <Tab eventKey={6} title="Reports">
            <Reports />
          </Tab>
        </Tabs>
      </div>
    );
  }
}

export default AdminDashboard;
