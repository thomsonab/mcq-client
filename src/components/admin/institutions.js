import React, { Component } from "react";
import { connect } from "react-redux";
import * as actions from "../../actions/admin";

import {
  BootstrapTable,
  TableHeaderColumn,
  ExportCSVButton
} from "react-bootstrap-table";

import ExtraInfo from "./ExtraInfo";
import ValidateButton from "./validatebutton";

import "../../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css";

/* dateSort 
 *  params:
 *    a: row 1
 *    b: row 2
 *    order: ordering
 */
function dateSort(a, b, order) {
  if (order === "desc") {
    return a.created - b.created;
  } else {
    return b.created - a.created;
  }
}

/* Below code is for expanding stuffs and
   * showing up additional details
   * 
   */
function expandComponent(row) {
  return (
    <ExtraInfo
      data={{
        coordinator: row.coordinator,
        registeredevents: row.registeredevents,
        hod: row.hod
      }}
    />
  );
}

/* Below function is for approving stuffs.
   * Not the most optimized way. Works for now.
   */
function displayValidateButton(cell, row) {
  return (
    <ValidateButton
      data={{
        index: row.id - 1,
        id: row._id,
        validatedyn: row.validatedyn
      }}
    />
  );
}
/* dateSort
 * Description:
 *    Converts the date to Indian date string.
 */
const formattedDate = function(date) {
  return `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`;
};

class Institutions extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    return this.props.fetchInstitutions();
  }

  /* Below code is for csv button
   * 
   */
  handleExportCSVButtonClick = onClick => {
    // Custom your onClick event here,
    // it's not necessary to implement this function if you have no any process before onClick
    // console.log('This is my custom function for ExportCSVButton click event');
    onClick();
  };
  createCustomExportCSVButton = onClick => {
    return (
      <ExportCSVButton
        btnText="Export Data"
        onClick={() => this.handleExportCSVButtonClick(onClick)}
      />
    );
  };

  expandColumnComponent({ isExpandableRow, isExpanded }) {
    let content = "";

    if (isExpandableRow) {
      content = isExpanded ? "(-)" : "(+)";
    } else {
      content = " ";
    }
    return <div> {content} </div>;
  }

  render() {
    const options = {
      exportCSVBtn: this.createCustomExportCSVButton
    };

    return (
      <div>
        <h2>OCOH Admin</h2>

        <h3> Registred Institutions </h3>
        <div>
          <BootstrapTable
            data={this.props.institutions}
            search={true}
            multiColumnSearch={true}
            pagination
            options={options}
            exportCSV
            expandableRow={() => {
              return true;
            }}
            expandComponent={expandComponent}
            expandColumnOptions={{
              expandColumnVisible: true,
              expandColumnComponent: this.expandColumnComponent,
              columnWidth: 50
            }}
          >
            <TableHeaderColumn
              dataField="id"
              width="40"
              export={false}
              isKey
              searchable={false}
            >
              ID
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField="institution"
              width="280"
              dataSort={true}
            >
              Institution
            </TableHeaderColumn>
            <TableHeaderColumn dataField="board" width="150" dataSort={true}>
              Board
            </TableHeaderColumn>
            <TableHeaderColumn dataField="email" width="200">
              Institution Email
            </TableHeaderColumn>
            <TableHeaderColumn dataField="phone" width="120">
              Institution Phone
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="address"
              width="200"
              tdStyle={{ whiteSpace: "normal" }}
            >
              Address
            </TableHeaderColumn>

            <TableHeaderColumn dataField="coordinatorName" hidden export>
              Coordinator Name
            </TableHeaderColumn>
            <TableHeaderColumn dataField="coordinatorEmail" hidden export>
              Coordinator Email
            </TableHeaderColumn>
            <TableHeaderColumn dataField="coordinatorPhone" hidden export>
              Coordinator Phone
            </TableHeaderColumn>
            <TableHeaderColumn dataField="coordinatorDesignation" hidden export>
              Coordinator Designation
            </TableHeaderColumn>
            <TableHeaderColumn dataField="hoiName" hidden export>
              hod Name
            </TableHeaderColumn>
            <TableHeaderColumn dataField="hoiEmail" hidden export>
              hod Email
            </TableHeaderColumn>
            <TableHeaderColumn dataField="hoiPhone" hidden export>
              hod Phone
            </TableHeaderColumn>
            <TableHeaderColumn dataField="hoiDesignation" hidden export>
              hod Designation
            </TableHeaderColumn>

            <TableHeaderColumn
              dataField="validatedyn"
              width="90"
              dataSort={true}
            >
              Approved
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField="created"
              width="100"
              dataSort={true}
              dataFormat={formattedDate}
            >
              Created Date
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField="validate"
              dataFormat={displayValidateButton}
              export={false}
              width="100"
            >
              Validate
            </TableHeaderColumn>
          </BootstrapTable>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    institutions: state.admin.institutions
  };
}

export default connect(mapStateToProps, actions)(Institutions);
