import React, { Component } from "react";
import { TableHeaderColumn, BootstrapTable } from "react-bootstrap-table";

export default class ExtraInfo extends Component {
  render() {
    if (this.props.data) {
      const listItemshash = this.props.data.registeredevents;
      let listItems = [];
      for (const prop in listItemshash) {
        if (listItemshash[prop]) {
          listItems.push(<li key={prop}>{prop}</li>);
        }
      }

      return (
        <div>
          <div>
            <h3>Coordinator Info</h3>
            <BootstrapTable data={[this.props.data.coordinator]}>
              <TableHeaderColumn dataField="name" width="150" isKey>
                Name
              </TableHeaderColumn>
              <TableHeaderColumn dataField="email" width="150">
                Email
              </TableHeaderColumn>
              <TableHeaderColumn dataField="phone" width="100">
                Phone
              </TableHeaderColumn>
              <TableHeaderColumn dataField="level" width="150">
                Designation
              </TableHeaderColumn>
            </BootstrapTable>
          </div>

          <div>
            <h3>Head of The Department</h3>
            <BootstrapTable data={[this.props.data.hod]}>
              <TableHeaderColumn dataField="name" width="150" isKey>
                Name
              </TableHeaderColumn>
              <TableHeaderColumn dataField="email" width="150">
                Email
              </TableHeaderColumn>
              <TableHeaderColumn dataField="phone" width="100">
                Phone
              </TableHeaderColumn>
            </BootstrapTable>
          </div>

          <div>
            <h3>Registered Events</h3>
            <ul>{listItems}</ul>
          </div>
        </div>
      );
    } else {
      return <p>No Additional Data</p>;
    }
  }
}
