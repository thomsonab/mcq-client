import React, { Component } from "react";
import { Tab, Tabs } from "react-bootstrap";
import SelectedGoodCopMoments from "./police/goodcopmoment";
import SelectedCoverarts from "./police/coverarts";
import SelectedFilms from "./police/films";

class PoliceAdminDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      key: 1
    };
  }

  handleSelect(key) {
    this.setState({ key });
  }

  render() {
    return (
      <div>
        <Tabs
          activeKey={this.state.key}
          onSelect={this.handleSelect.bind(this)}
          id="dashboard-tab"
        >
          <Tab eventKey={1} title="Selected GoodCopMoments">
            <SelectedGoodCopMoments />
          </Tab>

          <Tab eventKey={2} title="Selected Coverarts">
            <SelectedCoverarts />
          </Tab>

          <Tab eventKey={3} title="Selected Films">
            <SelectedFilms />
          </Tab>
        </Tabs>
      </div>
    );
  }
}

export default PoliceAdminDashboard;
