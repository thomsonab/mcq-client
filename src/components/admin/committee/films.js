import React, { Component } from "react";
import { connect } from "react-redux";

import * as adminactions from "../../../actions/admin/committee";

import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import "../../../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import { formattedDate } from "../../../utils/date";
import DownloadButton from "../../student/downloadbutton";
import "../../css/goodcopmoment.css";
import VerifyButton from "./verifybutton";

function displayDownloadButton(cell, row) {
  return <DownloadButton dropboxid={row.dropboxid} />;
}

function displayVerifyButton(cell, row) {
  return (
    <VerifyButton
      section="film"
      data={{
        index: row.id - 1,
        id: row._id,
        institute: row.institute,
        selected: row.committeeselected,
        userid: row.userid,
        name: row.name,
        email: row.email,
        phone: row.phone
      }}
    />
  );
}

class Films extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    return this.props.getSelectedFilms();
  }
  render() {
    return (
      <div>
        <h3>Films </h3>
        <div>
          {this.props.selectedfilms ? (
            <BootstrapTable
              data={this.props.selectedfilms}
              search={true}
              pagination
            >
              <TableHeaderColumn
                dataField="id"
                width="40"
                isKey
                searchable={false}
              >
                ID
              </TableHeaderColumn>
              <TableHeaderColumn dataField="name" width="100" dataSort={true}>
                Name
              </TableHeaderColumn>
              <TableHeaderColumn
                dataField="institute"
                width="150"
                dataSort={true}
                tdStyle={{ whiteSpace: "normal" }}
              >
                Institute
              </TableHeaderColumn>
              <TableHeaderColumn
                dataField="email"
                width="120"
                dataSort={true}
                tdStyle={{ whiteSpace: "normal" }}
              >
                Email
              </TableHeaderColumn>
              <TableHeaderColumn dataField="phone" width="100" dataSort={true}>
                Phone
              </TableHeaderColumn>
              <TableHeaderColumn
                dataField="submissiondate"
                width="80"
                dataSort={true}
                dataFormat={formattedDate}
              >
                Submission Date
              </TableHeaderColumn>

              <TableHeaderColumn
                dataField="description"
                width="200"
                dataSort={true}
                tdStyle={{ whiteSpace: "normal" }}
              >
                Relation Description
              </TableHeaderColumn>

              <TableHeaderColumn
                dataField="url"
                width="100"
                dataSort={true}
                tdStyle={{ whiteSpace: "normal" }}
                dataFormat={url => {
                  return (
                    <a href={url} target="_blank">
                      Click to view
                    </a>
                  );
                }}
              >
                Film Url
              </TableHeaderColumn>
              <TableHeaderColumn
                dataField="selected"
                dataFormat={displayVerifyButton}
                export={false}
                width="150"
              >
                Verify
              </TableHeaderColumn>
            </BootstrapTable>
          ) : (
            <div> Loading </div>
          )}
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    selectedfilms: state.admin.films
  };
}

export default connect(mapStateToProps, adminactions)(Films);
