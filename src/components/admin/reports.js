import React, { Component } from "react";
import { connect } from "react-redux";

import * as adminactions from "../../actions/admin/index";

import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import "../../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import { formattedDate } from "../../utils/date";
import DownloadButton from "../student/downloadbutton";
import "../css/goodcopmoment.css";

function displayDownloadButton(cell, row) {
  return <DownloadButton dropboxid={row.dropboxid} />;
}

class Reports extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    return this.props.fetchReports();
  }
  render() {
    return (
      <div>
        <h3>Reports from Institute Coordinators </h3>
        <div>
          {this.props.selectedreports ? (
            <BootstrapTable
              data={this.props.selectedreports}
              search={true}
              pagination
            >
              <TableHeaderColumn
                dataField="id"
                width="40"
                isKey
                searchable={false}
              >
                ID
              </TableHeaderColumn>
              <TableHeaderColumn dataField="name" width="100" dataSort={true}>
                Name
              </TableHeaderColumn>
              <TableHeaderColumn
                dataField="institute"
                width="150"
                dataSort={true}
                tdStyle={{ whiteSpace: "normal" }}
              >
                Institute
              </TableHeaderColumn>
              <TableHeaderColumn
                dataField="email"
                width="120"
                dataSort={true}
                tdStyle={{ whiteSpace: "normal" }}
              >
                Email
              </TableHeaderColumn>
              <TableHeaderColumn dataField="phone" width="100" dataSort={true}>
                Phone
              </TableHeaderColumn>
              <TableHeaderColumn
                dataField="submissiondate"
                width="80"
                dataSort={true}
                dataFormat={formattedDate}
              >
                Submission Date
              </TableHeaderColumn>

              <TableHeaderColumn
                dataField="description"
                width="200"
                dataSort={true}
                tdStyle={{ whiteSpace: "normal" }}
              >
                Description
              </TableHeaderColumn>

              <TableHeaderColumn
                dataField="dropboxid"
                width="100"
                dataFormat={displayDownloadButton}
              >
                Pdf
              </TableHeaderColumn>
            </BootstrapTable>
          ) : (
            <div> Loading </div>
          )}
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    selectedreports: state.admin.selectedreports
  };
}

export default connect(mapStateToProps, adminactions)(Reports);
