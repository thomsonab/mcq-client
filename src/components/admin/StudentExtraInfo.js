import React, { Component } from "react";
import { TableHeaderColumn, BootstrapTable } from "react-bootstrap-table";

import DownloadButton from "../student/downloadbutton";

function displayDownloadButton(cell, row) {
  return <DownloadButton dropboxid={row.dropboxid} />;
}

export default class ExtraInfo extends Component {
  render() {
    return (
      <div>
        {this.props.studentsubmissions.goodcopmoment.length > 0 ? (
          <div>
            <h3>GoodCopMoment </h3>
            <BootstrapTable
              data={this.props.studentsubmissions.goodcopmoment}
              search={true}
              pagination
              sizePerPage={5}
            >
              <TableHeaderColumn
                dataField="_id"
                width="40"
                isKey
                hidden
                export={false}
                searchable={false}
              >
                ID
              </TableHeaderColumn>

              <TableHeaderColumn
                dataField="relatedtouser"
                width="50"
                dataSort={true}
              >
                Related To User
              </TableHeaderColumn>
              <TableHeaderColumn
                dataField="relationdescription"
                width="200"
                dataSort={true}
                tdStyle={{ whiteSpace: "normal" }}
              >
                Relation Description
              </TableHeaderColumn>

              <TableHeaderColumn
                dataField="dropboxid"
                width="100"
                dataFormat={displayDownloadButton}
              >
                Pdf
              </TableHeaderColumn>
            </BootstrapTable>
          </div>
        ) : (
          <div> </div>
        )}

        {this.props.studentsubmissions.coverart.length > 0 ? (
          <div>
            <h3>Cover Art </h3>

            <BootstrapTable
              data={this.props.studentsubmissions.coverart}
              search={true}
              pagination
              sizePerPage={5}
            >
              <TableHeaderColumn
                dataField="_id"
                width="40"
                isKey
                hidden
                export={false}
                searchable={false}
              >
                ID
              </TableHeaderColumn>

              <TableHeaderColumn
                dataField="description"
                width="200"
                dataSort={true}
                tdStyle={{ whiteSpace: "normal" }}
              >
                Description
              </TableHeaderColumn>

              <TableHeaderColumn
                dataField="dropboxid"
                width="100"
                dataFormat={displayDownloadButton}
              >
                Pdf
              </TableHeaderColumn>
            </BootstrapTable>
          </div>
        ) : (
          <div> </div>
        )}

        {this.props.studentsubmissions.film.length > 0 ? (
          <div>
            <h3>Short Film </h3>
            <BootstrapTable
              data={this.props.studentsubmissions.film}
              search={true}
              pagination
              sizePerPage={5}
            >
              <TableHeaderColumn
                dataField="_id"
                width="40"
                isKey
                hidden
                export={false}
                searchable={false}
              >
                ID
              </TableHeaderColumn>

              <TableHeaderColumn
                dataField="description"
                width="200"
                dataSort={true}
                tdStyle={{ whiteSpace: "normal" }}
              >
                Description
              </TableHeaderColumn>

              <TableHeaderColumn
                dataField="url"
                width="200"
                dataSort={true}
                tdStyle={{ whiteSpace: "normal" }}
              >
                Short Film Link
              </TableHeaderColumn>
            </BootstrapTable>
          </div>
        ) : (
          <div> </div>
        )}
      </div>
    );
  }
}
