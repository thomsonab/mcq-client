import React, { Component } from "react";
import { connect } from "react-redux";
import * as actions from "../../actions/admin";

import "./static/validatebutton.css";

class ValidateButton extends Component {
  constructor(props) {
    super(props);
    this.onClickButton = this.onClickButton.bind(this);
    this.state = {
      spin: false
    };
  }

  onClickButton() {
    // We will have to remove it as soon as people get used to it.
    if (
      window.confirm(
        "This action will approve the institute and send an email to the institute about it. Are you sure you want to continue?"
      )
    ) {
      this.setState({
        spin: true
      });
      this.props.updateValidateYN({
        index: this.props.data.index,
        id: this.props.data.id,
        validatedyn: "Y"
      });
    }
  }
  render() {
    if (this.props.data.validatedyn === "Y") {
      return <div />;
    } else {
      return (
        <button className="btn btn-info" onClick={this.onClickButton}>
          {this.state.spin ? (
            <span>
              <span className="glyphicon glyphicon-refresh spinning" />Processing...
            </span>
          ) : (
            "Approve"
          )}
        </button>
      );
    }
  }
}

export default connect(null, actions)(ValidateButton);
