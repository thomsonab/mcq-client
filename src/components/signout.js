import React, { Component } from "react";
import { connect } from "react-redux";
import * as actions from "../actions";
import * as coordinatoractions from "../actions/coordinator";

import Body from "../components/body";
class Signout extends Component {
  componentWillMount() {
    this.props.signoutUser();
    this.props.resetInstitute();
    this.props.resetCoordinator();
  }
  render() {
    const info =
      "Thanks for your time. Please sign in again to view the score and certificate.";
    const link = {
      anchor: "/signin",
      label: "Sign In"
    };

    return (
      <Body info={info} link={link} />
      // <section className="quiz">
      //   <div className="container">
      //     <div className="row">
      //       <div className="col-md-3 side-over">
      //         <h1>Take a Quiz on Chennai Police</h1>
      //         <span className="bold">
      //           Show them how much you appreciate them<br />
      //           <br />
      //           <br />
      //         </span>
      //         Thanks for your time. Please sign in again to view the score and
      //         certificate.<br />
      //         <br />
      //         <br />
      //         <button className="btn btn-danger">
      //           <Link to="/signin">Sign In</Link>
      //         </button>
      //         <br />
      //         <br />
      //       </div>
      //       <div className="col-md-9 main-over">
      //         <img src={quizhead} alt="" />
      //       </div>
      //     </div>
      //   </div>
      // </section>
    );
  }
}

export default connect(null, { ...actions, ...coordinatoractions })(Signout);
