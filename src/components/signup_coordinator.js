import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
import { Link } from "react-router-dom";
import * as actions from "../actions";
import { connect } from "react-redux";
import "react-select/dist/react-select.css";
import "react-virtualized/styles.css";
import "react-virtualized-select/styles.css";
import VirtualizedSelect from "react-virtualized-select";

class SignupCoordinator extends Component {
  constructor(props) {
    super(props);
    this.renderAutocompleteField = this.renderAutocompleteField.bind(this);
  }

  componentWillMount() {
    if (this.props.institutelist.length === 0) {
      this.props.getInstituteList();
    }
    this.props.removeAuthError();
  }

  handleFormSubmit(formProps) {
    this.props._showProgress();
    this.props.signupCoordinator(formProps);
  }

  renderAutocompleteField(field) {
    const { meta: { touched, error } } = field;
    const classNameforField = `form-group ${
      touched && error ? "has-error" : ""
    }`;
    return (
      <div className={classNameforField}>
        <label className="control-label col-md-4" htmlFor={field.name}>
          {field.label}
          {field.info ? (
            <span>
              <br />
              <small id="emailHelp" className="form-text text-muted">
                {field.info}
              </small>
            </span>
          ) : (
            <br />
          )}
        </label>
        <div className="col-md-8">
          <VirtualizedSelect
            options={this.props.institutelist}
            {...field.input}
            placeholder={field.placeholder}
            onBlur={() => {
              field.input.onBlur(field.input.value);
            }}
            labelKey="name"
            valueKey="name"
          />
          {touched && error && <div className="error">{error}</div>}
        </div>
      </div>
    );
  }

  renderFormField(field) {
    const { meta: { touched, error } } = field;

    const classNameforField = `form-group ${
      touched && error ? "has-error" : ""
    }`;
    return (
      <div className={classNameforField}>
        <label className="control-label col-md-4" htmlFor={field.name}>
          {field.label}
          {field.info ? (
            <span>
              <br />
              <small id="emailHelp" className="form-text text-muted">
                {field.info}
              </small>
            </span>
          ) : (
            <br />
          )}
        </label>
        {field.label === "Your facebook username" ? (
          <div className="col-md-2" style={{ paddingTop: 5 }}>
            www.facebook.com/
          </div>
        ) : (
          <div>{field.name}</div>
        )}
        <div
          className={
            field.label === "Your facebook username" ? "col-md-6" : "col-md-8"
          }
        >
          <input
            id={field.name}
            type={field.type}
            {...field.input}
            placeholder={field.placeholder}
            className="form-control"
          />
          {touched && error && <div className="error">{error}</div>}
        </div>
      </div>
    );
  }

  customInstitute() {
    if (this.props.form.values) {
      if (this.props.form.values.institute) {
        if (this.props.form.values.institute.name === "Others") {
          return (
            <Field
              name="institute_custom"
              component={this.renderFormField}
              type="text"
              placeholder="Enter your affiliated institution name"
            />
          );
        }
      }
      return <div />;
    }
  }

  renderAlert() {
    if (this.props.errorMessage) {
      this.props._hideProgress();
      return (
        <div className="alert alert-danger">
          <strong> Oops!</strong> {this.props.errorMessage}
        </div>
      );
    }
  }

  render() {
    const { handleSubmit, pristine, submitting } = this.props;

    return (
      <form
        className="form-horizontal"
        onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}
      >
        <Field
          name="name"
          component={this.renderFormField}
          label="Name"
          type="text"
          placeholder="Enter your full name"
          info="Please enter your full name"
        />

        {/* <Field
          name="institute"
          component={this.renderFormField}
          label="Institution/School"
          type="text"
          placeholder="Enter your affiliated institution name"
          info="Please enter affiliated institution name"
        /> */}

        <Field
          name="institute"
          component={this.renderAutocompleteField}
          label="Institution/School"
          type="autocomplete"
          placeholder="Select your affiliated institution name"
          info="If your institution doesnt show up, select Others"
        />

        {this.customInstitute()}

        <Field
          name="designation"
          component={this.renderFormField}
          label="Designation"
          type="text"
          placeholder="Enter your designation in the institution"
          info="Please enter your designation"
        />
        <Field
          name="facebookusername"
          component={this.renderFormField}
          label="Your facebook username"
          type="text"
          placeholder="Please enter facebookusername"
          info="We wont share the info with anyone"
        />

        <Field
          name="email"
          component={this.renderFormField}
          label="Email"
          type="email"
          placeholder="Enter your email id"
          info="Please enter correct email for verification"
        />

        <Field
          name="phone"
          component={this.renderFormField}
          label="Phone"
          type="text"
          placeholder="Enter your phone number"
          info="Please enter phone number for further communications"
        />

        <Field
          name="password"
          component={this.renderFormField}
          label="Password"
          type="password"
          placeholder="Enter password"
        />

        <Field
          name="confirmpassword"
          component={this.renderFormField}
          label="Confirm Password"
          type="password"
          placeholder="Enter the same password entered above"
        />
        {this.renderAlert()}
        <div className="col-md-4" />

        <button
          action="submit"
          className="btn btn-primary"
          disabled={submitting}
        >
          Sign up
        </button>
        <Link className="btn btn-danger" to="/">
          Cancel
        </Link>
      </form>
    );
  }
}

function validate(values) {
  const errors = {};
  if (!values.name) {
    errors.name = "Please Enter name";
  }
  if (!values.designation) {
    errors.designation = "Please Enter your designation";
  }
  if (!values.institute) {
    errors.institute = "Please Select an institution name";
  }

  if (
    values.institute &&
    values.institute.name === "Others" &&
    !values.institute_custom
  ) {
    errors.institute_custom = "Please Enter an institution name";
  }

  if (!values.email) {
    errors.email = "Please Enter a valid email address";
  }

  if (!values.phone) {
    errors.phone = "Please Enter your phone number";
  }

  if (values.phone && isNaN(Number(values.phone))) {
    errors.phone = "Phone number should be a number";
  }

  if (!values.password) {
    errors.password = "Please enter a password";
  }
  if (values.confirmpassword !== values.password) {
    errors.confirmpassword = "The password entered doesnt match";
  }

  return errors;
}

function mapStateToProps(state) {
  return {
    errorMessage: state.auth.error,
    institutelist: state.institute.institutelist,
    form: state.form.signupcoordinator
  };
}

export default reduxForm({
  form: "signupcoordinator",
  validate: validate
})(connect(mapStateToProps, actions)(SignupCoordinator));
