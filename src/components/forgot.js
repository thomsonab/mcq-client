import React, { Component } from "react";
import RightPanel from "./common/rightpanel";
import { Link } from "react-router-dom";
import { ProgressBar } from "react-bootstrap";
import QuizHead from "./common/quizhead";
import axios from "axios";
import { ROOT_URL } from "../actions/url";

import "../static/css/form.css";
import "../static/css/signin.css";

class Forgot extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      submitted: false
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({ email: event.target.value });
  }
  handleSubmit(event) {
    event.preventDefault();

    this.setState({ submitted: true });
    axios
      .post(`${ROOT_URL}/forgot`, {
        email: this.state.email
      })
      .then(response => {
        console.log(response.data);
      })
      .catch(err => {
        console.log(err.response.data);
      });
  }

  renderForm() {
    if (!this.state.submitted) {
      return (
        <form onSubmit={this.handleSubmit}>
          <fieldset className="form-group">
            <label>Email:</label>
            <input
              type="email"
              value={this.state.value}
              onChange={this.handleChange}
              className="form-control"
              placeholder="Enter email to get reset link"
            />
          </fieldset>
          <button action="submit" type="submit" className="btn btn-primary">
            Reset Password
          </button>
        </form>
      );
    } else {
      return <div>Password reset link has been sent to your email.</div>;
    }
  }

  render() {
    return (
      <section className="quiz">
        <div className="container">
          <div className="row">
            <div className="col-md-3 side-over">
              <h1>Not yet Registered?</h1>
              <span className="bold">
                Register as a student or as an Institute Co-ordinator<br />
                <br />
              </span>
              <button className="btn btn-danger">
                <Link to="/signup">Register</Link>
              </button>
              <br />
              <br />
            </div>
            <RightPanel>
              <QuizHead />
              <div className="formo col-md-8">
                <div className="panel panel-default">
                  <div className="panel-body">{this.renderForm()}</div>
                </div>
              </div>
            </RightPanel>
          </div>
        </div>
      </section>
    );
  }
}

export default Forgot;
