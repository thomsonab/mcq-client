import React from "react";

export default function(props) {
  return (
    <div className="container">
      <br />
      <br />
      <div className="col-md-12 row">
        <div className="col-md-4">
          <img />
        </div>
        <div className="col-md-8">
          <h4 className="mb-4 color-5 font-md">Education Matters</h4>
          <p>
            Our initiative “OUR COP OUR HEROES” isn't about politics, race, or
            religion. All of us have been positively impacted by law
            enforcement, and now is the time to show them our appreciation. Do
            you have a moment of good experience / inspiration / interaction,
            with our police? Share it with us
          </p>
        </div>
        <br />
        <br />
      </div>
      <div className="col-md-12 row">
        <div className="col-md-8">
          <h4 className="mb-4 color-5 font-md">
            Chennai Sahodhaya School Complex
          </h4>
          <p>
            Our initiative “OUR COP OUR HEROES” isn't about politics, race, or
            religion. All of us have been positively impacted by law
            enforcement, and now is the time to show them our appreciation. Do
            you have a moment of good experience / inspiration / interaction,
            with our police? Share it with us
          </p>
        </div>
        <div className="col-md-4">
          <img />
        </div>
        <br />
        <br />
      </div>
      <div className="col-md-12 row">
        <div className="col-md-4">
          <img />
        </div>
        <div className="col-md-8">
          <h4 className="mb-4 color-5 font-md">Greater Chennai Police</h4>
          <p>
            Our initiative “OUR COP OUR HEROES” isn't about politics, race, or
            religion. All of us have been positively impacted by law
            enforcement, and now is the time to show them our appreciation. Do
            you have a moment of good experience / inspiration / interaction,
            with our police? Share it with us
          </p>
        </div>
        <br />
        <br />
      </div>
      <div className="col-md-12 row">
        <div className="col-md-8">
          <h4 className="mb-4 color-5 font-md">Chennai City Home Gaurds</h4>
          <p>
            Our initiative “OUR COP OUR HEROES” isn't about politics, race, or
            religion. All of us have been positively impacted by law
            enforcement, and now is the time to show them our appreciation. Do
            you have a moment of good experience / inspiration / interaction,
            with our police? Share it with us
          </p>
        </div>
        <div className="col-md-4">
          <img />
        </div>
        <br />
        <br />
      </div>
      <div className="col-md-12 row">
        <div className="col-md-4">
          <img />
        </div>
        <div className="col-md-8">
          <h4 className="mb-4 color-5 font-md">Aggu Media</h4>
          <p>
            Our initiative “OUR COP OUR HEROES” isn't about politics, race, or
            religion. All of us have been positively impacted by law
            enforcement, and now is the time to show them our appreciation. Do
            you have a moment of good experience / inspiration / interaction,
            with our police? Share it with us
          </p>
        </div>
        <br />
        <br />
      </div>
      <div className="col-md-12 row">
        <div className="col-md-8">
          <h4 className="mb-4 color-5 font-md">VFS</h4>
          <p>
            Our initiative “OUR COP OUR HEROES” isn't about politics, race, or
            religion. All of us have been positively impacted by law
            enforcement, and now is the time to show them our appreciation. Do
            you have a moment of good experience / inspiration / interaction,
            with our police? Share it with us
          </p>
        </div>
        <div className="col-md-4">
          <img />
        </div>
        <br />
        <br />
      </div>
      <div className="col-md-12 row">
        <div className="col-md-6">
          <h2>Advisory Team</h2>
          <ul>
            <li>Only School and College students can participate</li>
            <li>Age Group - Open to all</li>
            <li>One entry per student</li>
            <li>
              The submission must be made online by clicking submit entry button
              at the bottom
            </li>
            <li>
              The entry must be approved by your institute's OCOH Coordinator
              before being acccepted officaily.
            </li>
            <li>
              Parents can proxy for students below the age of 10 years.
              (Requires approval from the respective Head of Institution)
            </li>

            <li>
              All selected entries will be published in the ‘Book of Sagas’
            </li>
            <li>
              Organising Committee hand picks ten inspiring stories for
              face-to-face session with their heroes. The decision of selecting
              those ten lies entirely with the organising committee and is
              final.
            </li>
            <br />
            <br />
          </ul>
        </div>
        <div className="col-md-6">
          <h2>Implementation Team</h2>
          <ul>
            <li>Last date for submission is 20.03.2018</li>
            <li>View the entry and go through it fully before submitting</li>
            <li>
              Make sure there is no objectionable or immoral content exixts
            </li>
            <li>
              Make sure the submitted entry is of original work and not
              plagarised
            </li>
            <li>Ensure the entry is representing your institution</li>
            <li>
              Mke sure you have pre-approval from your institution for this
              entry
            </li>
            <li>
              Ensure the entry is approved by your institution after submission
            </li>
            <li>
              The entry will be accepted and published post approval,
              verification and subjected to moral standards of the content.
            </li>
            <li>
              The decision to accept or reject an entry lies with the organising
              committee and is final
            </li>
            <br />
            <br />
          </ul>
        </div>
      </div>
    </div>
  );
}
