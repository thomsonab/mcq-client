import React, { Component } from "react";
import { Field } from "redux-form";

class CheckBoxesForm extends Component {
  render() {
    return (
      <div>
        <div className="col-md-4" />
        <div>
          <label className="control-label">
            Select the events you would like to participate.
          </label>
        </div>

        <div className="col-md-4" />
        <div>
          <Field
            name="gcm"
            id="gcm"
            component="input"
            type="checkbox"
            disabled={false}
          />
          <label htmlFor="gcm">
            Share your good cop moment you have had with a police officer
          </label>
        </div>

        <div className="col-md-4" />
        <div>
          <Field
            name="filmmaking"
            id="filmmaking"
            component="input"
            type="checkbox"
            disabled={false}
          />
          <label htmlFor="filmmaking">
            OCOH themed short filmmaking contest
          </label>
        </div>

        <div className="col-md-4" />
        <div>
          <Field
            name="coverart"
            id="coverart"
            component="input"
            type="checkbox"
            disabled={false}
          />
          <label htmlFor="coverart">Design Coverart for Book of Sagas</label>
        </div>
      </div>
    );
  }
}

export default CheckBoxesForm;
