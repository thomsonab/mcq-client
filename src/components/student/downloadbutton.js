import React, { Component } from "react";
import { Button, Modal } from "react-bootstrap";

import Dropbox from "dropbox";
import "../admin/static/validatebutton.css";

import { Document, Page } from "react-pdf/build/entry.webpack";

class DownloadButton extends Component {
  constructor(props) {
    super(props);
    this.onClickButton = this.onClickButton.bind(this);
    this.state = {
      spin: false,
      showModal: false,
      pdfUrl: "",
      pdfName: "",
      numPages: null,
      pageNumber: 1
    };
  }

  onDocumentLoad = ({ numPages }) => {
    this.setState({ numPages });
  };

  close() {
    this.setState({ showModal: false });
  }

  downloadFile() {
    let downloadButton = document.createElement("a");
    downloadButton.setAttribute("href", this.state.pdfUrl);
    downloadButton.setAttribute("download", this.state.pdfName);
    downloadButton.style.display = "none";
    downloadButton.click();
  }

  onClickButton() {
    if (this.state.pdfUrl) {
      this.setState({
        showModal: true
      });
    } else {
      this.setState({
        spin: true
      });
      const ACCESS_TOKEN = process.env.REACT_APP_DROPBOX_ACCESS_TOKEN;
      const dbx = new Dropbox({ accessToken: ACCESS_TOKEN });
      dbx
        .filesDownload({ path: this.props.dropboxid })
        .then(response => {
          const downloadUrl = URL.createObjectURL(response.fileBlob);
          // let downloadButton = document.createElement("a");
          // downloadButton.setAttribute("href", downloadUrl);
          // downloadButton.setAttribute("download", response.name);
          // downloadButton.style.display = "none";

          // the file is downloaded, set downloading to false.
          // The modal has access to the pdf. Which can be fed to the react-pdf
          // Show the modal to view the pdf.
          this.setState({
            spin: false,
            showModal: true,
            pdfUrl: downloadUrl,
            pdfName: response.name
          });

          //downloadButton.click();
        })
        .catch(err => {
          console.log(err);
          this.setState({
            spin: false
          });
        });
    }
  }

  render() {
    const { pdfUrl, pageNumber, numPages } = this.state;
    return (
      <div>
        <Button bsStyle="primary" bsSize="small" onClick={this.onClickButton}>
          {this.state.spin ? (
            <span>
              <span className="glyphicon glyphicon-refresh spinning" />Downloading...
            </span>
          ) : (
            "View Document"
          )}
        </Button>
        <Modal
          show={this.state.showModal}
          onHide={this.close.bind(this)}
          bsSize="large"
        >
          <Modal.Header closeButton>
            <Modal.Title>Good Cop Moment</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div>
              {pdfUrl ? (
                <div>
                  <Document file={pdfUrl} onLoadSuccess={this.onDocumentLoad}>
                    {/* <Page pageNumber={pageNumber} /> */}
                    {Array.from(new Array(numPages), (el, index) => (
                      <Page key={`page_${index + 1}`} pageNumber={index + 1} />
                    ))}
                  </Document>
                </div>
              ) : (
                <div> The document could not be loaded </div>
              )}
            </div>
          </Modal.Body>

          <Modal.Footer>
            {pdfUrl ? (
              <Button onClick={this.downloadFile.bind(this)}>
                Download Pdf
              </Button>
            ) : (
              <div />
            )}
            <Button onClick={this.close.bind(this)}>Close</Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default DownloadButton;
