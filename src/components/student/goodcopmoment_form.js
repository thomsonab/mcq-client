import React, { Component } from "react";
import { Field, FormSection, reduxForm } from "redux-form";
import { renderFormField, renderYNField, renderFileInput } from "./utils/util";

import { connect } from "react-redux";
import * as goodcopmoment from "../../actions/student/good_cop_moment";
import GuidelinesGCM from "./utils/guidelines_gcm";

class GoodcopmomentForm extends Component {
  constructor(props) {
    super(props);
  }

  handleFormSubmit(formProps) {
    this.props.uploadGoodCopMoment(formProps);
  }

  relationdescriptionfield() {
    if (!this.props.form) {
      return <div />;
    }
    if (this.props.form.values) {
      if (this.props.form.values.relatedtouser) {
        if (this.props.form.values.relatedtouser === "N") {
          return (
            <Field
              name="relationdescription"
              component={renderFormField}
              label="What is your relation to the person to whom the moment is related?"
              type="textarea"
              info="Please mention details about the relationship between you and the person with whom the moment is related."
            />
          );
        }
      }
      return <div />;
    }
  }

  renderAlert() {
    if (this.props.student && !this.props.student.error) {
      return <div />;
    } else {
      return (
        <div className="alert alert-danger">
          <strong> Oops!</strong> {this.props.student.error}
        </div>
      );
    }
  }

  render() {
    const { handleSubmit, pristine, submitting } = this.props;

    return (
      <div>
        <br />
        {/* <span className="text-center">
          <h1>Good Cop Moment</h1>
        </span> */}
        <GuidelinesGCM />
        <form
          className="form-horizontal"
          onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}
        >
          <Field
            name="relatedtouser"
            component={renderYNField}
            label="Moment directly related to you?"
            type="select"
            info="Is the Good Cop Moment directly related to you?"
          />
          {this.relationdescriptionfield()}
          {/* <Field
            name="relationdescription"
            component={renderFormField}
            label="How is the relation to you?"
            type="text"
            info="Please mention about how the moment is related to you?"
          /> */}

          <Field
            name="file"
            type="file"
            label="Upload good cop moment"
            component={renderFileInput}
            info="Please make sure that it is the pdf file version of the Good Cop Moment template and is less than 5Mb"
          />
          {this.renderAlert()}
          <div className="col-md-4" />
          <button
            action="submit"
            className="btn btn-primary"
            disabled={submitting}
          >
            {this.props.spin ? (
              <span>
                <span className="glyphicon glyphicon-refresh spinning" />Processing...
              </span>
            ) : (
              "Submit Good Cop Moment"
            )}
          </button>
        </form>
      </div>
    );
  }
}

function validate(values) {
  const errors = {};

  if (!values.relatedtouser) {
    errors.relatedtouser = "Please select relation details";
  }

  if (values.relatedtouser === "Y" && !values.relationdescription) {
    errors.relationdescription =
      "Please update info about the relattion to the good cop moment";
  }

  if (!values.file) {
    errors.file = "Please select a pdf file";
  }
  return errors;
}

function mapStateToProps(state) {
  return {
    student: state.student,
    form: state.form.goodcopmomentform,
    spin: state.student.spin
  };
}
export default reduxForm({
  form: "goodcopmomentform",
  validate: validate
})(connect(mapStateToProps, { ...goodcopmoment })(GoodcopmomentForm));
