import React from "react";
export function renderFormField(field) {
  const { meta: { touched, error } } = field;

  const classNameforField = `form-group ${touched && error ? "has-error" : ""}`;
  return (
    <div className={classNameforField}>
      <label className="control-label col-md-4" htmlFor={field.name}>
        {field.label}
        {field.info ? (
          <span>
            <br />
            <small id="emailHelp" className="form-text text-muted">
              {field.info}
            </small>
          </span>
        ) : (
          <br />
        )}
      </label>
      {field.label.match(/facebook/g) ? (
        <div className="col-md-2" style={{ paddingTop: 5 }}>
          www.facebook.com/
        </div>
      ) : (
        <div>{field.name}</div>
      )}
      <div className={field.label.match(/facebook/g) ? "col-md-6" : "col-md-8"}>
        {field.type === "textarea" ? (
          <textarea
            id={field.name}
            type={field.type}
            {...field.input}
            placeholder={field.placeholder}
            className="form-control"
          />
        ) : (
          <input
            id={field.name}
            type={field.type}
            {...field.input}
            placeholder={field.placeholder}
            className="form-control"
          />
        )}

        {touched && error && <div className="error">{error}</div>}
      </div>
    </div>
  );
}

export function renderYNField(field) {
  const { meta: { touched, error } } = field;

  // const classNameforField = `form-group ${
  //   touched && error ? "has-error" : ""
  // }`;
  return (
    <div className="form-group">
      <label className="control-label col-md-4" htmlFor={field.name}>
        {field.label}
        <br />
        <small className="form-text text-muted">{field.info}</small>
      </label>
      <div className="col-md-3">
        <select className="form-control" id="class" {...field.input}>
          <option selected="selected" />
          <option>Y</option>
          <option>N</option>
        </select>
        {touched && error && <div className="error">{error}</div>}
      </div>
    </div>
  );
}

export function renderFileInput(field) {
  const { meta: { touched, error } } = field;

  delete field.input.value;

  return (
    <div className="form-group">
      <label className="control-label col-md-4" htmlFor={field.name}>
        {field.label}
        <br />
        <small className="form-text text-muted">{field.info}</small>
      </label>
      <div className="col-md-6">
        <input
          id={field.name}
          type={field.type}
          {...field.input}
          placeholder={field.placeholder}
          className="form-control"
        />
        {touched && error && <div className="error">{error}</div>}
      </div>
    </div>
  );
}
