import React from "react";

export default function(props) {
  return (
    <div>
      <div className="alert alert-success" role="alert">
        <b>Important!</b>&ensp;&ensp; Use this template to write your Good Cop
        Moment &ensp;<a
          className="btn btn-success"
          target="_blank"
          href="https://aggumedia-my.sharepoint.com/personal/dev_aggu_media/_layouts/15/guestaccess.aspx?docid=0e0a0abfd15ee46419ce5728d41c3cd68&authkey=AVx1nWK27M3Gh7wZpX09zs8&e=ZOFEWh"
        >
          Download - Good Cop Moment Template
        </a>
      </div>{" "}
      <div className="alert alert-danger" role="alert">
        Please do read and follow the guidelines to write a Good Cop Momnet
        &ensp;<a
          className="btn btn-danger"
          target="_blank"
          href="https://aggumedia-my.sharepoint.com/personal/dev_aggu_media/_layouts/15/guestaccess.aspx?docid=1f72e987c0e19475aa2a3e432d2200ce0&authkey=ARfdO8NjKsO-88XCp8tHoBA&e=9si4fO"
        >
          View Guidelines
        </a>
      </div>
    </div>
  );
}
