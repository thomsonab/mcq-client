import React from "react";

export default function(props) {
  return (
    <div className="container">
      <h2> Not yet open for entry submissions</h2>
      <p>Updates will be mailed to you or contact ocoh@edmat.org</p>
    </div>
  );
}
