import React from "react";
import { Link } from "react-router-dom";

export default function(props) {
  return (
    <div className="row">
      <div className="col-md-6 side-over ">
        <div className="jumbotron text-center">
          <Link to="/questions" className="nav-link">
            <button className="btn btn-primary">Take the quiz now</button>
          </Link>
        </div>
      </div>
      <div className="col-md-6 side-over ">
        <div className="jumbotron text-center">
          <Link to="/scores" className="nav-link">
            <button className="btn btn-primary">See your scores</button>
          </Link>
        </div>
      </div>
    </div>
  );
}
