import React, { Component } from "react";
import { Field, FormSection, reduxForm } from "redux-form";
import { renderFormField, renderYNField, renderFileInput } from "./utils/util";

import { connect } from "react-redux";

import * as coverartactions from "../../actions/student/cover_art";

class CoverartForm extends Component {
  constructor(props) {
    super(props);
  }

  handleFormSubmit(formProps) {
    this.props.uploadCoverart(formProps);
  }

  renderAlert() {
    if (this.props.coverart && !this.props.coverart.error) {
      return <div />;
    } else {
      return (
        <div className="alert alert-danger">
          <strong> Oops!</strong> {this.props.coverart.error}
        </div>
      );
    }
  }

  render() {
    const { handleSubmit, pristine, submitting } = this.props;

    return (
      <div>
        <br />
        <form
          className="form-horizontal"
          onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}
        >
          <Field
            name="description"
            component={renderFormField}
            label="Cover art description"
            type="textarea"
            info="Please share some info about the cover art"
          />
          <Field
            name="file"
            type="file"
            label="Upload Cover Art"
            component={renderFileInput}
            info="Please make sure that it is the pdf file version of the Cover art template and is less than 5Mb"
          />
          {this.renderAlert()}
          <div className="col-md-4" />
          <button
            className="btn btn-primary"
            action="submit"
            disabled={submitting}
          >
            {this.props.spin ? (
              <span>
                <span className="glyphicon glyphicon-refresh spinning" />Processing...
              </span>
            ) : (
              "Submit Cover Art"
            )}
          </button>
        </form>
      </div>
    );
  }
}

function validate(values) {
  const errors = {};

  if (!values.file) {
    errors.file = "Please select a pdf file";
  }

  return errors;
}

function mapStateToProps(state) {
  return {
    coverart: state.coverart,
    student: state.student,
    spin: state.coverart.spin
  };
}

export default reduxForm({
  form: "coverartform",
  validate: validate
})(connect(mapStateToProps, { ...coverartactions })(CoverartForm));
