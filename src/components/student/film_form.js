import React, { Component } from "react";

import { Field, reduxForm } from "redux-form";
import { renderFormField } from "./utils/util";
import { connect } from "react-redux";
import * as filmactions from "../../actions/student/film";

class FilmForm extends Component {
  constructor(props) {
    super(props);
  }

  handleFormSubmit(formProps) {
    this.props.submitFilm(formProps);
  }

  renderAlert() {
    if (this.props.film && !this.props.film.error) {
      return <div />;
    } else {
      return (
        <div className="alert alert-danger">
          <strong> Oops!</strong> {this.props.film.error}
        </div>
      );
    }
  }

  render() {
    const { handleSubmit, pristine, submitting } = this.props;
    return (
      <div>
        <br />
        <form
          className="form-horizontal"
          onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}
        >
          <Field
            name="description"
            component={renderFormField}
            label="Short film description"
            type="textarea"
            info="Please share some info about the short film"
          />

          <Field
            name="url"
            component={renderFormField}
            label="Short film url"
            type="textarea"
            info="Please add the youtube url of the short film"
          />
          {this.renderAlert()}
          <div className="col-md-4" />
          <button
            className="btn btn-primary"
            action="submit"
            disabled={submitting}
          >
            {this.props.spin ? (
              <span>
                <span className="glyphicon glyphicon-refresh spinning" />Processing...
              </span>
            ) : (
              "Submit Short Film"
            )}
          </button>
        </form>
      </div>
    );
  }
}

function validate(values) {
  const errors = {};

  if (!values.url) {
    errors.url = "The short film url field should not be empty";
  }
  return errors;
}

function mapStateToProps(state) {
  return {
    film: state.film,
    student: state.student,
    spin: state.film.spin
  };
}

export default reduxForm({
  form: "filmform",
  validate: validate
})(connect(mapStateToProps, { ...filmactions })(FilmForm));
