import React, { Component } from "react";
import { connect } from "react-redux";
import { Tab, Tabs } from "react-bootstrap";
import { Link } from "react-router-dom";

import QuizTabContent from "./quiz";
import LeftPanel from "../common/leftpanel";
import RightPanel from "../common/rightpanel";
import QuizHead from "../common/quizhead";
import GoodCopMomentForm from "./goodcopmoment_form";
import UploadModal from "../activities/upload_modal";
import NotStarted from "./not_started";
import StudentSubmissions from "./student_submissions";
import CoverArtForm from "./coverart_form";
import FilmForm from "./film_form";

class StudentDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      key: 1
    };
  }

  handleSelect(key) {
    this.setState({ key });
  }

  // componentWillUpdate(nextProps) {
  //   if (this.props.tabselected !== nextProps.tabselected) {
  //     this.handleSelect(nextProps.tabselected);
  //   }
  // }

  componentWillMount() {
    if (this.props.match.params.tabselected) {
      this.setState({
        key: Number.parseInt(this.props.match.params.tabselected)
      });
    }
  }

  render() {
    return (
      <div className="container">
        <LeftPanel />

        <RightPanel>
          <QuizHead />

          <Tabs
            activeKey={this.state.key}
            onSelect={this.handleSelect.bind(this)}
            id="dashboard-tab"
          >
            <Tab eventKey={1} title="Good Cop Moment">
              <GoodCopMomentForm />
              {/* <UploadModal /> */}
              {/* <NotStarted /> */}
            </Tab>
            <Tab eventKey={2} title="Quiz">
              {/* <QuizTabContent /> */}
              <NotStarted />
            </Tab>
            <Tab eventKey={3} title="Cover Art">
              {/* <NotStarted /> */}
              <CoverArtForm />
            </Tab>
            <Tab eventKey={4} title="Filmmaking">
              {/* <NotStarted /> */}
              <FilmForm />
            </Tab>
            <Tab eventKey={5} title="Submissions">
              <StudentSubmissions />
            </Tab>
          </Tabs>
        </RightPanel>
      </div>
    );
  }
}

export default StudentDashboard;
