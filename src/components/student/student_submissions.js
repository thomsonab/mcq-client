import React, { Component } from "react";
import { connect } from "react-redux";

import * as studentactions from "../../actions/student/index";

import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import "../../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import { formattedDate } from "../../utils/date";
import DownloadButton from "./downloadbutton";

function displayDownloadButton(cell, row) {
  return <DownloadButton dropboxid={row.dropboxid} />;
}

function displayStatus(cell, row) {
  if (cell === "Y") {
    return <div>Approved</div>;
  } else if (cell === "N") {
    return <div>Not Approved</div>;
  } else {
    return <div> Approval Pending </div>;
  }
}
class StudentSubmissions extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    return this.props.getSubmissions();
  }

  render() {
    return (
      <div>
        <h3>GoodCopMoment </h3>
        <div>
          {this.props.studentsubmissions ? (
            <BootstrapTable
              data={this.props.studentsubmissions.goodcopmoment}
              search={true}
              pagination
            >
              <TableHeaderColumn
                dataField="id"
                width="40"
                isKey
                searchable={false}
              >
                ID
              </TableHeaderColumn>
              <TableHeaderColumn
                dataField="submissiondate"
                width="100"
                dataSort={true}
                dataFormat={formattedDate}
              >
                Submission Date
              </TableHeaderColumn>
              <TableHeaderColumn
                dataField="relatedtouser"
                width="50"
                dataSort={true}
              >
                Related To User
              </TableHeaderColumn>
              <TableHeaderColumn
                dataField="relationdescription"
                width="200"
                dataSort={true}
                tdStyle={{ whiteSpace: "normal" }}
              >
                Relation Description
              </TableHeaderColumn>

              <TableHeaderColumn
                dataField="dropboxid"
                width="100"
                dataFormat={displayDownloadButton}
              >
                Pdf
              </TableHeaderColumn>
              <TableHeaderColumn
                dataField="selected"
                width="100"
                dataFormat={displayStatus}
              >
                Status
              </TableHeaderColumn>
            </BootstrapTable>
          ) : (
            <div> Loading </div>
          )}
        </div>

        <h3>Cover Art </h3>
        <div>
          {this.props.studentsubmissions ? (
            <BootstrapTable
              data={this.props.studentsubmissions.coverart}
              search={true}
              pagination
            >
              <TableHeaderColumn
                dataField="id"
                width="40"
                isKey
                searchable={false}
              >
                ID
              </TableHeaderColumn>
              <TableHeaderColumn
                dataField="submissiondate"
                width="100"
                dataSort={true}
                dataFormat={formattedDate}
              >
                Submission Date
              </TableHeaderColumn>

              <TableHeaderColumn
                dataField="description"
                width="200"
                dataSort={true}
                tdStyle={{ whiteSpace: "normal" }}
              >
                Description
              </TableHeaderColumn>

              <TableHeaderColumn
                dataField="dropboxid"
                width="100"
                dataFormat={displayDownloadButton}
              >
                Pdf
              </TableHeaderColumn>
              <TableHeaderColumn
                dataField="selected"
                width="100"
                dataFormat={displayStatus}
              >
                Status
              </TableHeaderColumn>
            </BootstrapTable>
          ) : (
            <div> Loading </div>
          )}
        </div>

        <h3>Short Film </h3>
        <div>
          {this.props.studentsubmissions ? (
            <BootstrapTable
              data={this.props.studentsubmissions.film}
              search={true}
              pagination
            >
              <TableHeaderColumn
                dataField="id"
                width="40"
                isKey
                searchable={false}
              >
                ID
              </TableHeaderColumn>
              <TableHeaderColumn
                dataField="submissiondate"
                width="100"
                dataSort={true}
                dataFormat={formattedDate}
              >
                Submission Date
              </TableHeaderColumn>

              <TableHeaderColumn
                dataField="description"
                width="200"
                dataSort={true}
                tdStyle={{ whiteSpace: "normal" }}
              >
                Description
              </TableHeaderColumn>

              <TableHeaderColumn
                dataField="url"
                width="200"
                dataSort={true}
                tdStyle={{ whiteSpace: "normal" }}
              >
                Short Film Link
              </TableHeaderColumn>
              <TableHeaderColumn
                dataField="selected"
                width="100"
                dataFormat={displayStatus}
              >
                Status
              </TableHeaderColumn>
            </BootstrapTable>
          ) : (
            <div> Loading </div>
          )}
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    studentsubmissions: state.student.studentsubmissions
  };
}

export default connect(mapStateToProps, studentactions)(StudentSubmissions);
